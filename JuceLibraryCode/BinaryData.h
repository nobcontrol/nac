/* =========================================================================================

   This is an auto-generated file: Any edits you make may be overwritten!

*/

#pragma once

namespace BinaryData
{
    extern const char*   icon16_png;
    const int            icon16_pngSize = 934;

    extern const char*   icon32_png;
    const int            icon32_pngSize = 2631;

    extern const char*   icon321_png;
    const int            icon321_pngSize = 2631;

    extern const char*   icon64_png;
    const int            icon64_pngSize = 6455;

    extern const char*   icon128_png;
    const int            icon128_pngSize = 17600;

    extern const char*   icon256_png;
    const int            icon256_pngSize = 53820;

    extern const char*   icon2561_png;
    const int            icon2561_pngSize = 53820;

    extern const char*   icon512_png;
    const int            icon512_pngSize = 185566;

    extern const char*   icon5121_png;
    const int            icon5121_pngSize = 185566;

    extern const char*   icon1024_png;
    const int            icon1024_pngSize = 706315;

    // Points to the start of a list of resource names.
    extern const char* namedResourceList[];

    // Number of elements in the namedResourceList array.
    const int namedResourceListSize = 10;

    // If you provide the name of one of the binary resource variables above, this function will
    // return the corresponding data and its size (or a null pointer if the name isn't found).
    const char* getNamedResource (const char* resourceNameUTF8, int& dataSizeInBytes) throw();
}
