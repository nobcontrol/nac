/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 4.3.1

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library - "Jules' Utility Class Extensions"
  Copyright (c) 2015 - ROLI Ltd.

  ==============================================================================
*/

#ifndef __JUCE_HEADER_40E6BB562117AD1E__
#define __JUCE_HEADER_40E6BB562117AD1E__

//[Headers]     -- You can add your own extra header files here --
/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Assignment.h"
#include "SetShortcutButton.h"

#include "../JuceLibraryCode/JuceHeader.h"
using namespace juce;

class QuickAssignmentsPanel;
//[/Headers]



//==============================================================================
/**
                                                                    //[Comments]
    An auto-generated component, created by the Projucer.

    Describe your class and how it works here!
                                                                    //[/Comments]
*/
class QuickAssignmentShortcutEditor  : public juce::Component
{
public:
    //==============================================================================
    QuickAssignmentShortcutEditor (QuickAssignmentsPanel* Owner);
    ~QuickAssignmentShortcutEditor();

    //==============================================================================
    //[UserMethods]     -- You can add your own custom methods in this section.
	void removeButtonPressed();
	void resetShortcuts();
	void resetAssignShortcut();
	void resetTriggerShortcut();
	void shortcutSet(SetShortcutButton *button);

	void copyFromOther(const QuickAssignmentShortcutEditor& other);

	SetShortcutButton* getAssignButton() { return assignShortcutButton; };
	SetShortcutButton* getTriggerButton() { return triggerShortcutButton; };

	QuickAssignmentsPanel* getQuickAssignmentsPanel();
    void setAsDefault();

	void updateTooltips();
    
    ScopedPointer<Assignment> assignment;
    //[/UserMethods]

    void paint (Graphics& g) override;
    void resized() override;
    void mouseEnter (const MouseEvent& e) override;
    void mouseExit (const MouseEvent& e) override;



private:
    //[UserVariables]   -- You can add your own custom variables in this section.
	QuickAssignmentsPanel* owner;
    bool isDefaultEditor;
    //[/UserVariables]

    //==============================================================================
    ScopedPointer<SetShortcutButton> assignShortcutButton;
    ScopedPointer<SetShortcutButton> triggerShortcutButton;
    ScopedPointer<SetShortcutButton> removeButton;


    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (QuickAssignmentShortcutEditor)
};

//[EndFile] You can add extra defines here...
//[/EndFile]

#endif   // __JUCE_HEADER_40E6BB562117AD1E__
