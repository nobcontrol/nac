/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "nAcTrayIcon.h"

static juce::DocumentWindow* mainWindow;
static juce::DocumentWindow* updateFirmwareWindow;

nAcTrayIcon::nAcTrayIcon() : SystemTrayIconComponent()
{
	mainWindow = NULL;
	updateFirmwareWindow = NULL;

#if JUCE_WIN32
	defaultIconImage = juce::Image(juce::ImageFileFormat::loadFrom(juce::File::getCurrentWorkingDirectory().getChildFile("Data/nAcTrayIcon.png")));
	circularIconImage = juce::Image(juce::ImageFileFormat::loadFrom(juce::File::getCurrentWorkingDirectory().getChildFile("Data/nAcTrayIcon_Circular.png")));
	diagonalIconImage = juce::Image(juce::ImageFileFormat::loadFrom(juce::File::getCurrentWorkingDirectory().getChildFile("Data/nAcTrayIcon_Diagonal.png")));
#endif
    
#if JUCE_MAC
	defaultIconImage = juce::Image(juce::ImageFileFormat::loadFrom(juce::File::getSpecialLocation(juce::File::currentApplicationFile).getChildFile("Data/nAcTrayIcon.png")));
    circularIconImage = juce::Image(juce::ImageFileFormat::loadFrom(juce::File::getSpecialLocation(juce::File::currentApplicationFile).getChildFile("Data/nAcTrayIcon_Circular.png")));
	diagonalIconImage = juce::Image(juce::ImageFileFormat::loadFrom(juce::File::getSpecialLocation(juce::File::currentApplicationFile).getChildFile("Data/nAcTrayIcon_Diagonal.png")));
#endif

	lookAndFeel = new juce::LookAndFeel_V3();
    lookAndFeel->setColour(juce::PopupMenu::ColourIds::highlightedBackgroundColourId, juce::Colours::lightblue);
    lookAndFeel->setColour(juce::PopupMenu::ColourIds::highlightedTextColourId, juce::Colours::black);

	
	SystemTrayIconComponent::setIconImage(defaultIconImage);
    currentIconImage = defaultIcon;
}

void nAcTrayIcon::mouseDoubleClick(const juce::MouseEvent & event)
{
#if JUCE_WIN32
	if (event.mods.isLeftButtonDown())
	{
		/*
		if (mainWindow)
		{
			mainWindow->setVisible(true);
		}
		*/

		if (mainWindow)
		{
			//if (mainWindow->isVisible())
			//	mainWindow->closeButtonPressed();

			if (mainWindow->isVisible())
				mainWindow->setVisible(false);

			mainWindow->setVisible(true);
		}
	}
#endif
}

void nAcTrayIcon::mouseDown(const juce::MouseEvent &event)
{
#if JUCE_MAC
    // On OSX, there can be problems launching a menu when we're not the foreground
    // process, so just in case, we'll first make our process active, and then use a
    // timer to wait a moment before opening our menu, which gives the OS some time to
    // get its act together and bring our windows to the front.
    
    juce::Process::makeForegroundProcess();
    startTimer (menuTimer, 50);
#endif

#if JUCE_WIN32
	juce::ModifierKeys modkeys = event.mods.getCurrentModifiers();

	if (event.mouseWasClicked() && modkeys.testFlags(event.mods.popupMenuClickModifier))
	{
		juce::PopupMenu m;
		m.setLookAndFeel(lookAndFeel);
		m.addItem(50, "Settings...");
		m.addSeparator();
		m.addItem(51, "Update firmware...");
		m.addSeparator();
		m.addItem(1, "Quit nAc");

		const int result = m.show();
		if (result == 0)
		{
			// user dismissed the menu without picking anything
		}
		else
		{
			menuInvocationCallback(result, this);
		}
	}
#endif
}

void nAcTrayIcon::mouseUp(const juce::MouseEvent & event)
{
	/*
#if JUCE_WIN32
    juce::ModifierKeys modkeys = event.mods.getCurrentModifiers();

	if (event.mouseWasClicked() && modkeys.testFlags(event.mods.popupMenuClickModifier))
	{
		juce::PopupMenu m;
		m.setLookAndFeel(lookAndFeel);
		m.addItem(50, "Settings...");
		m.addSeparator();
		m.addItem(51, "Update firmware...");
		m.addSeparator();
		m.addItem(1, "Quit nAc");

		const int result = m.show();
		if (result == 0)
		{
			// user dismissed the menu without picking anything
		}
		else
		{
            menuInvocationCallback(result, this);
		}
	}
#endif
*/
}

void nAcTrayIcon::timerCallback(int timerID)
{
    stopTimer(timerID);
    
    if (timerID == menuTimer)
    {
        juce::PopupMenu m;
		m.setLookAndFeel(lookAndFeel);
        m.addItem (50, "Settings...");
		m.addSeparator();
		m.addItem(51, "Update firmware...");
		m.addSeparator();
        m.addItem (1, "Quit nAc");
    
        // It's always better to open menus asynchronously when possible.
        m.showMenuAsync (juce::PopupMenu::Options(), juce::ModalCallbackFunction::forComponent(menuInvocationCallback, this));
    }
}

// This is invoked when the menu is clicked or dismissed
void nAcTrayIcon::menuInvocationCallback (int chosenItemID, nAcTrayIcon*)
{
    if (chosenItemID == 1)
        juce::JUCEApplication::getInstance()->systemRequestedQuit();
    else if (chosenItemID == 50)
    {
          if (mainWindow)
          {
		//	if (mainWindow->isVisible())
		//		mainWindow->closeButtonPressed();

			if (mainWindow->isVisible())
				mainWindow->setVisible(false);

			mainWindow->setVisible(true);
          }
    }
	else if (chosenItemID == 51)
	{
		if (updateFirmwareWindow)
			updateFirmwareWindow->setVisible(true);
	}
}

void nAcTrayIcon::setMainWindow(juce::DocumentWindow * MainWindow)
{
	mainWindow = MainWindow;
}

void nAcTrayIcon::setUpdateFirmwareWindow(juce::DocumentWindow * FirmwareWindow)
{
	updateFirmwareWindow = FirmwareWindow;
}

void nAcTrayIcon::actionListenerCallback(const juce::String & message)
{
	if (message.contains("DefaultIcon"))
    {
        if (currentIconImage != defaultIcon)
        {
            setIconImage(defaultIconImage);
            currentIconImage = defaultIcon;
        }
    }
	else if (message.contains("CircularIcon"))
    {
        if (currentIconImage != circularIcon)
        {
            setIconImage(circularIconImage);
            currentIconImage = circularIcon;
        }
    }
	else if (message.contains("DiagonalIcon"))
    {
        if (currentIconImage != diagonalIcon)
        {
            setIconImage(diagonalIconImage);
            currentIconImage = diagonalIcon;
        }
    }

    
}
