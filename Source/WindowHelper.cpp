/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#if __APPLE__
    #import <AppKit/AppKit.h>
    #include <CoreFoundation/CoreFoundation.h>
    #include <ApplicationServices/ApplicationServices.h>
    #include <Carbon/Carbon.h>
#endif

#include "WindowHelper.h"


juce_ImplementSingleton_SingleThreaded(WindowHelper)

juce::StringArray WindowHelper::wildcards;
bool WindowHelper::matchesWildcard = false;
int WindowHelper::wildcardIndex = -1;

juce::Point<int> WindowHelper::getRelativeCoordinatesToFocusedWindow(juce::Point<int> screenPosition)
{
	BOOL rectRetrieved = false;
	juce::Point<int> windowPosition;

#if JUCE_WIN32
	HWND window = GetForegroundWindow();
	if (window)
	{
		RECT rect;
		rectRetrieved = GetWindowRect(window, &rect);
		windowPosition.x = rect.left;
		windowPosition.y = rect.top;
	}
#endif
    
#if JUCE_MAC
    AXUIElementRef* windowRef = getFrontMostWindow();
    if (windowRef)
    {
        AXError error;
        
        AXValueRef position;
        error = AXUIElementCopyAttributeValue(*windowRef, kAXPositionAttribute, (CFTypeRef *)&position);
        CGPoint windowPos;
        AXValueGetValue(position, (AXValueType)kAXValueCGPointType, &windowPos);
        if (error == kAXErrorSuccess)
        {
            
            CFRelease(position);
            rectRetrieved = true;
            windowPosition.x = (int)windowPos.x;
            windowPosition.y = (int)windowPos.y;
        }
        else
        {
            DBG("Unable to get front most window's position.");
        }
        
        CFRelease(*windowRef);
        delete windowRef;
    }
#endif

	if (rectRetrieved)
	{
		return juce::Point<int>(screenPosition.x - windowPosition.x, screenPosition.y - windowPosition.y);
	}
	else
	{
        DBG("Unable to get cursor's position related to active window.");
		return screenPosition;
	}
}

void* WindowHelper::getFocusedWindowInfo(juce::String &owner, juce::String &title)
{
#if JUCE_WIN32
	HWND window = GetForegroundWindow();
	owner = getWindowOwner(window);
	title = getWindowTitle(window);

	DBG("Focused window info> owner: " + owner + " title: " + title);
    return window;
#endif

#if JUCE_MAC
    AXUIElementRef* windowRefPtr = getFrontMostWindow();
    if (windowRefPtr)
    {
        //Get the window title
        CFStringRef titleCF;
        AXError error = AXUIElementCopyAttributeValue(*windowRefPtr, kAXTitleAttribute, (CFTypeRef *) &titleCF);
        
        if (error == kAXErrorSuccess)
        {
            NSString *titleNS = (NSString *)titleCF;
            std::string *titleSTD = new std::string([titleNS UTF8String]);
            title = juce::String(*titleSTD);
            delete titleSTD;
            CFRelease(titleCF);
        }
        
        //Get window owner
        pid_t PID;
        AXUIElementGetPid(*windowRefPtr, &PID);
        AXUIElementRef app = AXUIElementCreateApplication(PID);
        CFStringRef ownerCF;
        AXUIElementCopyAttributeValue(app, kAXTitleAttribute, (CFTypeRef*)&ownerCF);
        NSString *ownerNS = (NSString *)ownerCF;
        std::string *ownerSTD = new std::string([ownerNS UTF8String]);
        owner = juce::String(*ownerSTD);
        delete ownerSTD;
        CFRelease(ownerCF);
        CFRelease(app);
        
        DBG("Focused window info> owner: " + owner + " title: " + title);
        return windowRefPtr;
    }
    
    //Not getting reached anyway...
    if (windowRefPtr!=NULL)
    {
        CFRelease(*windowRefPtr);
        delete windowRefPtr;
    }
#endif
    
}

void* WindowHelper::getFocusedWindow()
{
#if JUCE_WIN32
	return (void*)GetForegroundWindow();
#endif
	return NULL;
}

bool WindowHelper::windowInfoMatch(juce::String owner, juce::String title, juce::String otherOwner, juce::String otherTitle)
{
	bool result = false;

	bool ownerMatch = (owner == otherOwner);

	bool wildcardMatch = false;
	bool wcIndex = -1;
	for (int i = 0; i < wildcards.size(); i++)
	{
		if (title.matchesWildcard(wildcards[i], true))
		{
			wildcardMatch = true;
			wcIndex = i;
			break;
		}
	}

	bool titleMatch = false;
	if (wildcardMatch)
		titleMatch = otherTitle.matchesWildcard(wildcards[wcIndex], true);
	else
		titleMatch = (title == otherTitle);

	if (titleMatch && ownerMatch)
		result = true;

	return result;
}

void WindowHelper::setWildcards(juce::StringArray& theWildcards)
{
	wildcards.clear();
	for (int i = 0; i < theWildcards.size(); i++)
	{
		wildcards.add(theWildcards[i]);
	}
}

void* WindowHelper::getWindowAtPosition(juce::Point<float> position, juce::String &owner, juce::String &title)
{
	void* nullPointer = NULL;

#if JUCE_WIN32
	POINT pos;
	pos.x = (LONG)position.x;
	pos.y = (LONG)position.y;

	HWND window = RealChildWindowFromPoint(GetDesktopWindow(), pos);

	if (window)
	{
		//Get window title
		title = getWindowTitle(window);

		//Get owner
		owner = getWindowOwner(window);

		return (void*)window;
	}
#endif

#if JUCE_MAC
    
    CGWindowID windowID = getWindowIdAtPosition(position);
    AXUIElementRef* windowRefPtr = getAXWindow(windowID);
    
    if (windowRefPtr)
    {
        //Get the window title
        CFStringRef titleCF;
        AXError error = AXUIElementCopyAttributeValue(*windowRefPtr, kAXTitleAttribute, (CFTypeRef *) &titleCF);
        
        if (error != kAXErrorSuccess)
        {
            DBG("Problem retrieving title attribure from AXUIElement.");
        }
        
        NSString *titleNS = (NSString *)titleCF;
        std::string *titleSTD = new std::string([titleNS UTF8String]);
        title = juce::String(*titleSTD);
        delete titleSTD;
        CFRelease(titleCF);
        
        //Get window owner
        pid_t PID;
        AXUIElementGetPid(*windowRefPtr, &PID);
        AXUIElementRef app = AXUIElementCreateApplication(PID);
        CFStringRef ownerCF;
        AXUIElementCopyAttributeValue(app, kAXTitleAttribute, (CFTypeRef*)&ownerCF);
        NSString *ownerNS = (NSString *)ownerCF;
        std::string *ownerSTD = new std::string([ownerNS UTF8String]);
        owner = juce::String(*ownerSTD);
        delete ownerSTD;
        CFRelease(ownerCF);
        CFRelease(app);
    }
    else
    {
        DBG("Unable to retrieve accessibility window from window id: " + juce::String(windowID));
    }
    
    return windowRefPtr;

#endif

	return nullPointer;
}

void * WindowHelper::findWindow(juce::String owner, juce::String title)
{
	matchesWildcard = false;
	wildcardIndex = -1;

	for (int i = 0; i < wildcards.size(); i++)
	{
		if (title.matchesWildcard(wildcards[i], true))
		{
			matchesWildcard = true;
			wildcardIndex = i;
			break;
		}
	}


#if JUCE_WIN32
	ownerToCompareTo = owner;
	titleToCompareTo = title;

	if (ownerToCompareTo.isEmpty())
		return NULL;

	result = NULL;
	EnumWindows(&WindowHelper::EnumWindowsProc, (LPARAM)result);

	if (result)
		return (void*)result;
	else
	{
		DBG("Unable to find window with owner: " + owner + " and title: " + title);
	}
#endif
    
#if JUCE_MAC
    pid_t ownerPID = 0;
    ownerPID = getPID(owner);
    if (ownerPID)
    {
        AXUIElementRef* window = NULL;
        window = getAXWindow(ownerPID, title);
        if (window)
            return (void*)window;
        else
        {
            DBG("Unable to find window with owner: " + owner + " and title: " + title);
        }
    }
    
#endif

	return nullptr;
}

#if JUCE_WIN32
juce::String WindowHelper::getWindowTitle(HWND window)
{
	juce::String title;

	int titlelength = GetWindowTextLength(window)+1;
	char* titleprt = new char[titlelength];
	GetWindowText(window, titleprt, titlelength);
	title = juce::String(juce::CharPointer_UTF8(titleprt));
	delete titleprt;

	return title;
}

juce::String WindowHelper::getWindowOwner(HWND window)
{
	juce::String owner;

	DWORD processID;
	GetWindowThreadProcessId(window, &processID);
	HANDLE processHandle = OpenProcess(PROCESS_QUERY_INFORMATION, true, processID);
	char ownerprt[MAX_PATH + 1] = { 0 };
	GetModuleFileNameEx(processHandle, NULL, ownerprt, MAX_PATH);
	owner = juce::String(juce::CharPointer_UTF8(ownerprt)).fromLastOccurrenceOf("\\", false, true);
	CloseHandle(processHandle);
//	delete ownerprt;

	return owner;
}

BOOL CALLBACK WindowHelper::EnumWindowsProc(_In_ HWND hwnd, _In_ LPARAM lParam)
{
	juce::String title = WindowHelper::getInstance()->getWindowTitle(hwnd);

	bool titleMatch = false;
	if (matchesWildcard)
	{
		titleMatch = title.matchesWildcard(wildcards[wildcardIndex], true);
	}
	else
	{
		titleMatch = (WindowHelper::getInstance()->titleToCompareTo == title);
	}

	bool ownerMatch = false;
	juce::String owner = WindowHelper::getInstance()->getWindowOwner(hwnd);
	ownerMatch = (WindowHelper::getInstance()->ownerToCompareTo == owner);

	if (titleMatch && ownerMatch)
	{
		WindowHelper::getInstance()->result = hwnd;
		return false;
	}

	return true;
}

#endif

#if JUCE_MAC
AXUIElementRef* WindowHelper::getFrontMostWindow()
{
    AXUIElementRef* result = new AXUIElementRef;
    
    NSRunningApplication* application = [[NSWorkspace sharedWorkspace] frontmostApplication];
    pid_t PID = [application processIdentifier];
    AXUIElementRef axApp = AXUIElementCreateApplication(PID);
    
    bool found = false;

    AXUIElementRef windowRef;
    AXError error = AXUIElementCopyAttributeValue(axApp, kAXFocusedWindowAttribute, (CFTypeRef*)&windowRef);
    if (error == kAXErrorSuccess)
    {
        *result = windowRef;
        CFRetain(*result);
        found = true;
    }
    else
    {
        DBG("Unable to get focused window of active application.");
    }
       
    if (!found)
    {
        delete result;
        return NULL;
    }
    else
        return result;
}


CGWindowID WindowHelper::getWindowIdAtPosition(juce::Point<float> position)
{
    CGWindowID windowID = kCGNullWindowID;
    
    CFArrayRef windowList = CGWindowListCopyWindowInfo(kCGWindowListExcludeDesktopElements | kCGWindowListOptionOnScreenOnly, kCGNullWindowID);
    
    CFIndex windowsCount = CFArrayGetCount(windowList);
    
    for (CFIndex i=0; i < windowsCount; i++)
    {
        CFDictionaryRef window = (CFDictionaryRef)CFArrayGetValueAtIndex(windowList, i);
        
        int level;
        CFNumberGetValue((CFNumberRef)CFDictionaryGetValue(window, (void *)kCGWindowLayer), kCFNumberIntType, &level);
        if (level < 20)
        {
            CGRect windowBounds;
            CGRectMakeWithDictionaryRepresentation((CFDictionaryRef)CFDictionaryGetValue(window, kCGWindowBounds), &windowBounds);
            
            if (position.x>=windowBounds.origin.x &&
                position.x<=windowBounds.origin.x+windowBounds.size.width &&
                position.y>=windowBounds.origin.y &&
                position.y<=windowBounds.origin.y+windowBounds.size.height)
            {
                CFNumberGetValue((CFNumberRef)CFDictionaryGetValue(window, kCGWindowNumber),
                                 kCGWindowIDCFNumberType,
                                 &windowID);
                
                //Check if the window was the appropriate subrole
                AXUIElementRef* windowCantidate = getAXWindow(windowID);
                if (windowCantidate)
                {
                    CFStringRef subrole;
                    AXUIElementCopyAttributeValue(*windowCantidate, kAXSubroleAttribute, (CFTypeRef *)&subrole);
                    
                    CFComparisonResult result1;
                    result1 = CFStringCompare(subrole, CFSTR("AXStandardWindow"), kCFCompareCaseInsensitive);
                    CFComparisonResult result2;
                    result2 = CFStringCompare(subrole, CFSTR("AXFloatingWindow"), kCFCompareCaseInsensitive);
                    CFComparisonResult result3;
                    result3 = CFStringCompare(subrole, CFSTR("AXDialog"), kCFCompareCaseInsensitive);

                    if (result1 == kCFCompareEqualTo || result2 == kCFCompareEqualTo || result3 == kCFCompareEqualTo)
                    {
                        delete windowCantidate;
                        CFRelease(windowList);
                        return windowID;
                    }
                
                    delete windowCantidate;
                }
                
            //  CFRelease(windowList);
            //  return windowID;
            }
        }
    }
    
    if (windowID == kCGNullWindowID)
    {
        DBG("Window ID for window at cursor position could not get retrieved.");
    }
    
    CFRelease(windowList);
    return windowID;
}

AXUIElementRef* WindowHelper::getAXWindow(CGWindowID windowID)
{
    AXError error;
    AXUIElementRef *result = new AXUIElementRef;

    if (windowID == kCGNullWindowID)
    {
        DBG("Window ID is NULL. No accesibility window wo retrieve.");
        
        delete result;
        return NULL;
    }
    
    CGWindowID windowArray[1];
    windowArray[0] = windowID;
    CFArrayRef windowArrayRef = CFArrayCreate(kCFAllocatorDefault, (const void**)&windowArray, 1, NULL);
    
    if (CFArrayGetCount(windowArrayRef) != 1)
    {
        DBG("Unable to create array to store window ID.");
        
        delete result;
        return NULL;
    }
    
    CFArrayRef windowDescriptionArray = CGWindowListCreateDescriptionFromArray(windowArrayRef);
    
    //Get bounds
    CGRect bounds;
    CFDictionaryRef windowDescription = (CFDictionaryRef)CFArrayGetValueAtIndex(windowDescriptionArray, 0);
    CFDictionaryRef _bounds = (CFDictionaryRef)CFDictionaryGetValue(windowDescription, kCGWindowBounds);
    CGRectMakeWithDictionaryRepresentation(_bounds, &bounds);
    
    //Get PID
    pid_t PID;
    CFNumberGetValue((CFNumberRef)CFDictionaryGetValue(windowDescription, (void *)kCGWindowOwnerPID), kCFNumberIntType, &PID);
    
    //Get window name
    CFStringRef windowName = (CFStringRef)CFDictionaryGetValue(windowDescription, kCGWindowName);

    //Now get the AXUIElement with this PID, get all his windows and compare bounds and title to find out the right AXUIElement.
    AXUIElementRef application = AXUIElementCreateApplication(PID);
    
    CFIndex maxValues;
    error = AXUIElementGetAttributeValueCount(application, kAXWindowsAttribute, &maxValues);
    
    if (error != kAXErrorSuccess)
    {
        DBG("Error retrieving number of windows: " + juce::String(error));
    }
    
    CFArrayRef applicationWindows;
    AXUIElementCopyAttributeValues(application, kAXWindowsAttribute, 0, maxValues, &applicationWindows);
    
    CFIndex windowCandidateIndex = -1;
    for (CFIndex i=0; i<CFArrayGetCount(applicationWindows); i++)
    {
        AXUIElementRef applicationWindow = (AXUIElementRef)CFArrayGetValueAtIndex(applicationWindows, i);
        
        AXValueRef positionRef;
        AXUIElementCopyAttributeValue(applicationWindow, kAXPositionAttribute, (CFTypeRef *) &positionRef);
        CGPoint windowPosition;
        AXValueGetValue(positionRef, (AXValueType)kAXValueCGPointType, &windowPosition);
        CFRelease(positionRef);
        
        AXValueRef sizeRef;
        AXUIElementCopyAttributeValue(applicationWindow, kAXSizeAttribute, (CFTypeRef *) &sizeRef);
        CGSize windowSize;
        AXValueGetValue(sizeRef, (AXValueType)kAXValueCGSizeType, &windowSize);
        CFRelease(sizeRef);
        
        CFStringRef title;
        AXUIElementCopyAttributeValue(applicationWindow, kAXTitleAttribute, (CFTypeRef *) &title);
        
        //Do some comparing.
        if (title)
        {
            if (bounds.origin.x == windowPosition.x && bounds.origin.y == windowPosition.y &&
                bounds.size.width == windowSize.width && bounds.size.height == windowSize.height)
            {
                if (CFStringCompare(title, windowName, 0) == kCFCompareEqualTo)
                {
                    *result = applicationWindow;
                    CFRetain(*result);
                 
                    CFRelease(title);
                    CFRelease(windowDescriptionArray);
                    CFRelease(applicationWindows);
                    CFRelease(windowArrayRef);
                    
                    return result;
                }
                else
                    windowCandidateIndex = i;
            }
            
            CFRelease(title);
        }
        else
        {
            DBG("Window title of accesibility window is NULL");
        }
    }
        
    if (windowCandidateIndex!=-1)
    {
        AXUIElementRef applicationWindow = (AXUIElementRef)CFArrayGetValueAtIndex(applicationWindows, windowCandidateIndex);
        *result = applicationWindow;
        CFRetain(*result);
        
        CFRelease(windowDescriptionArray);
        CFRelease(applicationWindows);
        CFRelease(windowArrayRef);
        
        return result;
    }
    
    delete result;
    
    CFRelease(applicationWindows);
    CFRelease(windowDescriptionArray);
    CFRelease(windowArrayRef);
    
    return NULL;
}

bool WindowHelper::isAXWindowOnScreen(AXUIElementRef windowRef)
{
    AXValueRef positionRef;
    AXUIElementCopyAttributeValue(windowRef, kAXPositionAttribute, (CFTypeRef *) &positionRef);
    CGPoint windowPosition;
    AXValueGetValue(positionRef, (AXValueType)kAXValueCGPointType, &windowPosition);
    CFRelease(positionRef);
    
    AXValueRef sizeRef;
    AXUIElementCopyAttributeValue(windowRef, kAXSizeAttribute, (CFTypeRef *) &sizeRef);
    CGSize windowSize;
    AXValueGetValue(sizeRef, (AXValueType)kAXValueCGSizeType, &windowSize);
    CFRelease(sizeRef);
    
    pid_t PID;
    AXUIElementGetPid(windowRef, &PID);
    
    CFArrayRef windowList = CGWindowListCopyWindowInfo(kCGWindowListExcludeDesktopElements | kCGWindowListOptionOnScreenOnly, kCGNullWindowID);
    
    for (CFIndex i=0; i < CFArrayGetCount(windowList) ; i++)
    {
        CFDictionaryRef window = (CFDictionaryRef)CFArrayGetValueAtIndex(windowList, i);
        
        pid_t windowPID;
        CFNumberGetValue((CFNumberRef)CFDictionaryGetValue(window, (void *)kCGWindowOwnerPID), kCFNumberIntType, &windowPID);

        if (windowPID == PID)
        {
            CGRect windowBounds;
            CGRectMakeWithDictionaryRepresentation((CFDictionaryRef)CFDictionaryGetValue(window, kCGWindowBounds), &windowBounds);
            
            if (windowPosition.x == windowBounds.origin.x &&
                windowSize.width == windowBounds.size.width &&
                windowPosition.y == windowBounds.origin.y &&
                windowSize.height == windowBounds.size.height)
            {
                CFRelease(windowList);
                return true;
            }
        }
    }
    
    CFRelease(windowList);
    
    return false;
}

pid_t WindowHelper::getPID(juce::String owner)
{
    pid_t result = 0;
    
    NSArray<NSRunningApplication*> *applications = [[NSWorkspace sharedWorkspace] runningApplications];
    
    for (NSUInteger i=0; i<applications.count; i++)
    {
        std::string *ownerSTD = new std::string([applications[i].localizedName UTF8String]);
        
        bool found = false;
        if (owner == juce::String(*ownerSTD))
        {
            found = true;
            result = applications[i].processIdentifier;
        }
        
        delete ownerSTD;
        
        if (found)
            break;
    }
    
    return result;
}

AXUIElementRef* WindowHelper::getAXWindow(pid_t owner, juce::String title)
{
    AXUIElementRef *result = new AXUIElementRef;
    
    AXUIElementRef application = AXUIElementCreateApplication(owner);
    
    
    
    CFIndex maxValues;
    AXError error = AXUIElementGetAttributeValueCount(application, kAXWindowsAttribute, &maxValues);
    if (error != kAXErrorSuccess)
    {
        DBG("Unable to get application's windows.");
    }
    
    if (maxValues == 0)
    {
        //Activate application for the windows list to be available
        AXUIElementSetAttributeValue(application, kAXFrontmostAttribute, kCFBooleanTrue);
        juce::Thread::sleep(500);
        AXUIElementGetAttributeValueCount(application, kAXWindowsAttribute, &maxValues);
    }
    
    CFArrayRef applicationWindows;
    AXUIElementCopyAttributeValues(application, kAXWindowsAttribute, 0, maxValues, &applicationWindows);
    
    bool found = false;
    for (CFIndex i=0; i<CFArrayGetCount(applicationWindows); i++)
    {
        AXUIElementRef applicationWindow = (AXUIElementRef)CFArrayGetValueAtIndex(applicationWindows, i);
     
        CFStringRef titleCF;
        AXUIElementCopyAttributeValue(applicationWindow, kAXTitleAttribute, (CFTypeRef *) &titleCF);

        NSString *titleNS = (NSString *)titleCF;
        
        if (titleNS)
        {
            std::string *titleSTD = new std::string([titleNS UTF8String]);
            
            bool titleMatch = false;
            juce::String windowTitle = juce::String(*titleSTD);
            if (matchesWildcard)
            {
                titleMatch = windowTitle.matchesWildcard(wildcards[wildcardIndex], true);
            }
            else
            {
                titleMatch = (title == windowTitle);
            }
            
            if (titleMatch)
            {
                *result = applicationWindow;
                CFRetain(*result);
                found = true;
            }
            
            CFRelease(titleCF);
            delete titleSTD;
        }
        
        if (found)
            break;
    }
    
    CFRelease(applicationWindows);
    CFRelease(application);
    
    if (found)
        return result;
    else
        return NULL;
}


#endif
