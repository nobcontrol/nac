/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CONTROLLER_H_INCLUDED
#define CONTROLLER_H_INCLUDED

#include "Assignment.h"
#include "../JuceLibraryCode/JuceHeader.h"
//#include "ControllerManager.h"

enum dragState
{
	noDrag = 0,
	accumulatingClicks,
	dragArmed,
	beingDragged
};

enum rightToggleTouchstate
{
	noTouch = 0,
	beingTouched,
	flippedWhileTouched
};

enum timerIDs
{
	circularOrDiagonalArmedTimer = 0,
	assignmentActivityTimer
};

enum createAssignmentState
{
	nothing_Assigned = 0,
	A_is_Assigned,
	A_and_B_are_Assigned,
	assignment_Process_Started
};

enum triggerState
{
	noTrigger = 0,
	assignmentA_triggered,
	assignmentB_triggered,
	external_assignment_triggered
};

enum directionChangeStateForTriggeredAssignments
{
	noDirectionChange = 0,
	assignmentJustTriggered,
	directionJustSaved,
	directionChanged
};

class ControllerManager;
class Assignment;

class Controller : public juce::ActionListener, public juce::ActionBroadcaster
{
public:
	Controller();
	~Controller();

    virtual void actionListenerCallback(const juce::String &message);

	virtual void init();
	virtual void release();

	virtual int ID();
    
    virtual void setSensitivity(int sensitivity) = 0;
	virtual void setPrimaryMouseButton(int MouseButtonID);
	virtual void setScrollWheelMode(int ScrollWheelModeID);

	virtual Assignment* getCurrentAssignment();
	virtual int getCurrentDragState();
	virtual void setCurrentDragState(int theCurrentDragState);

	virtual void releaseCurrentAssignment();
	virtual void triggerAssignment(Assignment* assignment);
	virtual void assignToDefault();

	virtual void setControllerManager(ControllerManager* theControllerManager);

	virtual void setUp(bool isKnobBeingTouched, bool isLeftToggleBeingTouched, bool isLeftToggleUp, bool isRightToggleBeingTouched, bool isRightToggleUp);

private:
	int uid;

protected:
	ControllerManager* controllerManager;
	Assignment* currentAssignment;
	int currentDragState;

	bool knobTouch;
	bool leftToggleTouch;
	bool leftToggleUp;
	bool rightToggleTouch;
	bool rightToggleUp;
};


#endif
