/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic startup code for a Juce application.

  ==============================================================================
*/

#include "LowLevelKeyboardListener.h"
#include "SettingsComponent.h"
#include "SerialInterface.h"
#include "nAcTrayIcon.h"
#include "ControllerManager.h"
#include "UpdateFirmwareWindow.h"

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
class nAcApplication  : public juce::JUCEApplication
{
public:
    //==============================================================================
    nAcApplication() {}

    const juce::String getApplicationName() override       { return ProjectInfo::projectName; }
    const juce::String getApplicationVersion() override    { return ProjectInfo::versionString; }
    bool moreThanOneInstanceAllowed() override       { return true; }

    //==============================================================================
    void initialise (const juce::String& commandLine) override
    {
        DBG("Initialising...");
        
        // This method is where you should put your application's initialisation code..
        mainWindow = new MainWindow(juce::String("Settings"));
		mainWindow->setAlwaysOnTop(true);

		//Update firmware window
		updateFirmwareWindow = new UpdateFirmwareWindow();
		updateFirmwareWindow->setAlwaysOnTop(true);

		//Trayicon code
		trayIcon = new nAcTrayIcon();
		trayIcon->setMainWindow(mainWindow);
		trayIcon->setUpdateFirmwareWindow(updateFirmwareWindow);

        
		//Serial interface code
        DBG("Starting serial interface...");
		serialInterface = new SerialInterface();
		serialInterface->setTrayIcon(trayIcon);
		serialInterface->startThread();

		//set the serial interface to the update firmware window
		updateFirmwareWindow->setSerialInterface(serialInterface);

		//Control manager code
		controllerManager = new ControllerManager();
		controllerManager->setTrayIcon(trayIcon);
		controllerManager->setSerialInterface(serialInterface);
        controllerManager->setQuickAssignmentsPanel(((SettingsComponent*)mainWindow->getContentComponent())->getQuickAssignmentsPanel());
        
        //Set control manager to settings component
        ((SettingsComponent*)mainWindow->getContentComponent())->setControllerManager(controllerManager);
        
#if JUCE_MAC
        juce::Process::setDockIconVisible(false);
#endif

		//Starting keyboard listener
        DBG("Starting low-level keyboard listener...");
		LowLevelKeyboardListener::getInstance()->setControllerManager(controllerManager);
		LowLevelKeyboardListener::getInstance()->setSerialInterface(serialInterface);
#if JUCE_MAC
		LowLevelKeyboardListener::getInstance()->startThread();
#endif

#if JUCE_WIN32
		LowLevelKeyboardListener::getInstance()->setUpHook();
#endif

		//Load the settings
        DBG("Loading settings...");
		((SettingsComponent*)mainWindow->getContentComponent())->loadSettings();
    }

    void shutdown() override
    {
        // Add your application's shutdown code here..

		//Close window and save the settings
        mainWindow->closeButtonPressed();
        
        DBG("Stopping low-level keyboard listener...");
#if JUCE_MAC
        LowLevelKeyboardListener::getInstance()->stopRunLoop();
#endif
		LowLevelKeyboardListener::getInstance()->stopThread(1500);
        LowLevelKeyboardListener::deleteInstance();

        DBG("Stopping serial interface...");
		serialInterface->stopThread(500);
		serialInterface = nullptr;

		controllerManager = nullptr;

		trayIcon = nullptr;

        mainWindow = nullptr; // (deletes our window)
    }

    //==============================================================================
    void systemRequestedQuit() override
    {
        // This is called when the app is being asked to quit: you can ignore this
        // request and let the app carry on running, or call quit() to allow the app to close.
        quit();
    }

    void anotherInstanceStarted (const juce::String& commandLine) override
    {
        // When another instance of the app is launched while this one is running,
        // this method is invoked, and the commandLine parameter tells you what
        // the other instance's command-line arguments were.
    }

    //==============================================================================
    /*
        This class implements the desktop window that contains an instance of
        our MainContentComponent class.
    */
	class MainWindow : public juce::DocumentWindow, public KeyListener
    {
    public:
        MainWindow (juce::String name)  : juce::DocumentWindow (name,
                                                juce::Colour(245, 245, 245),
                                                juce::DocumentWindow::closeButton | juce::DocumentWindow::minimiseButton)
        {
#if JUCE_WIN32
            setUsingNativeTitleBar (false);
			setTitleBarHeight(20);
#endif
            
#if JUCE_MAC
            setUsingNativeTitleBar (true);
#endif
			lookAndFeel = new LookAndFeel_V3();
            lookAndFeel->setColour(juce::PopupMenu::ColourIds::highlightedBackgroundColourId, juce::Colours::lightblue);
			lookAndFeel->setColour(juce::PopupMenu::ColourIds::highlightedTextColourId, juce::Colours::black);
			lookAndFeel->setColour(juce::Slider::ColourIds::backgroundColourId, juce::Colours::lightgrey);
			lookAndFeel->setColour(juce::Slider::ColourIds::thumbColourId, juce::Colours::lightblue);
			lookAndFeel->setColour(juce::TextButton::ColourIds::buttonColourId, juce::Colours::lightgrey);

			setLookAndFeel(lookAndFeel);

			addKeyListener(this);

			setContentOwned(new SettingsComponent(), true);

            setSize(305, 400);
            setResizable(true, false);
            setResizeLimits(getWidth(), getHeight()+10, getWidth(), 1080);

            centreWithSize (getWidth(), getHeight());
            //setVisible (true);
        }

        void closeButtonPressed() override
        {
            // This is called when the user tries to close this window. Here, we'll just
            // ask the app to quit when this happens, but you can change this to do
            // whatever you need.
            
			//JUCEApplication::getInstance()->systemRequestedQuit();
			setVisible(false);
            
            //Save settings on window close
            DBG("Saving settings to disk...");
            ((SettingsComponent*)getContentComponent())->saveSettings();
        }

		bool keyPressed(const KeyPress &key, Component *originatingComponent) override
		{
			if (key.isKeyCode(key.escapeKey))
				closeButtonPressed();

			return true;
		}

        /* Note: Be careful if you override any DocumentWindow methods - the base
           class uses a lot of them, so by overriding you might break its functionality.
           It's best to do all your work in your content component instead, but if
           you really have to override any DocumentWindow methods, make sure your
           subclass also calls the superclass's method.
        */

    private:
        TooltipWindow tooltipWindow;
		juce::ScopedPointer<LookAndFeel_V3> lookAndFeel;
        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainWindow)
    };

private:
    juce::ScopedPointer<MainWindow> mainWindow;
	juce::ScopedPointer<nAcTrayIcon> trayIcon;
	juce::ScopedPointer<SerialInterface> serialInterface;
	juce::ScopedPointer<ControllerManager> controllerManager;
	juce::ScopedPointer<UpdateFirmwareWindow> updateFirmwareWindow;
};

//==============================================================================
// This macro generates the main() routine that launches the app.
START_JUCE_APPLICATION (nAcApplication)
