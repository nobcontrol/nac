/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CONTROLLERMANAGER_H_INCLUDED
#define CONTROLLERMANAGER_H_INCLUDED

#include "QuickAssignmentsPanel.h"
#include "Controller.h"
#include "DoubleTapChecker.h"
#include "SerialInterface.h"
#include "nAcTrayIcon.h"

class ControllerManager : public juce::ActionListener, public juce::MultiTimer
{
public:
	ControllerManager();
	~ControllerManager();

	void setSerialInterface(SerialInterface* theSerialInterface);
	void setTrayIcon(nAcTrayIcon* theTrayIcon);
    void setQuickAssignmentsPanel(QuickAssignmentsPanel* theQuickAssignmentsPanel);

	void initDefaultController();
	void changeDefaultController(int controllerIndex);
    
    void setSensitivity(int sensitivity);
	void setControlMode(int controlMode);
	void setPrimaryMouseButton(int MouseButtonID);
	void setScrollWheelBehaviour(int ScrollWheelMode);

	void actionListenerCallback(const juce::String &message) override;
	void timerCallback(int timerID) override;

	int getCurrentAssignmentState();

	bool isKnobBeingTouched();

private:
	juce::CriticalSection objectLock;
	juce::OwnedArray<Controller> controllers;
	juce::ScopedPointer<Controller> defaultController;

	void setUpCurrentController();

	SerialInterface* serialInterface;
	nAcTrayIcon* trayIcon;
    QuickAssignmentsPanel* quickAssignmentsPanel;

	Controller* currentController;

	//Assignment stuff
	void detectAssignments();
	void triggerAssignment(Assignment* assignment);
	void releaseCurrentAssignment();
	void assignToDefault();

	int selectedMode;
	Assignment assignmentA;
	Assignment assignmentB;
	Assignment temporaryAssignment;

	bool knobTouch;
	bool leftToggleTouch;
	bool rightToggleTouch;
	DoubleTapChecker leftToggleTapChecker;
	juce::int64 timeAtAssignmentStart;
	int currentAssignmentState;
	int lastValidAssignmentState;
	int currentTriggerState;
	int lastAssignementTriggered;
	juce::int64 assignmentTimethreshold;   //in milliseconds
	bool leftToggleFlippedDuringAssignmentProcess;
	bool newAssignmentsWereJustDefined;
	bool needspositionUpdating;

	//Members for making proper assignments while manipulating
	Assignment* assignmentPtr;
	bool assignmentNeedsPositionUpdatingAndTriggering;

	void getAssignmentsForActiveWindow(juce::Array<Assignment*> &windowAssignments);
};

#endif 
