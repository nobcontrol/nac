/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NACTRAYICON_H_INCLUDED
#define NACTRAYICON_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

class nAcTrayIcon : public juce::SystemTrayIconComponent, public juce::ActionListener, public juce::MultiTimer
{
public:
	nAcTrayIcon();

	void mouseDoubleClick(const juce::MouseEvent &event) override;
	void mouseUp(const juce::MouseEvent &event) override;
    void mouseDown(const juce::MouseEvent &event) override;
    
	static void setMainWindow(juce::DocumentWindow* MainWindow);
	static void setUpdateFirmwareWindow(juce::DocumentWindow* FirmwareWindow);

	virtual void actionListenerCallback(const juce::String &message) override;
    
    // This is invoked when the menu is clicked or dismissed
    static void menuInvocationCallback (int chosenItemID, nAcTrayIcon*);

private:
    
	juce::ScopedPointer<juce::LookAndFeel_V3> lookAndFeel;

    void timerCallback(int timerID) override;
    
	juce::Image defaultIconImage;
	juce::Image circularIconImage;
	juce::Image diagonalIconImage;
    
    enum IconImages
    {
        defaultIcon = 0,
        circularIcon,
        diagonalIcon
    };
    
    int currentIconImage;
    
    enum timerIDs
    {
        menuTimer = 0,
        changeIconTimer
    };
};

#endif
