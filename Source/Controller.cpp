/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Controller.h"

Controller::Controller()
{
	static int id = 0; 
	uid = id++;

	currentAssignment = NULL;
	controllerManager = NULL;

	knobTouch = false;
	leftToggleTouch = false;;
	leftToggleUp = false;
	rightToggleTouch = false;
	rightToggleUp = true;
}

Controller::~Controller()
{
}

void Controller::actionListenerCallback(const juce::String & message)
{
}

void Controller::init()
{
}

void Controller::release()
{
}

int Controller::ID()
{
	return uid;
}

void Controller::setPrimaryMouseButton(int MouseButtonID)
{

}

void Controller::setScrollWheelMode(int ScrollWheelModeID)
{

}

Assignment * Controller::getCurrentAssignment()
{
	return currentAssignment;
}

int Controller::getCurrentDragState()
{
	return currentDragState;
}

void Controller::setCurrentDragState(int theCurrentDragState)
{
	currentDragState = theCurrentDragState;
}

void Controller::releaseCurrentAssignment()
{
}

void Controller::triggerAssignment(Assignment * assignment)
{
}

void Controller::assignToDefault()
{

}

void Controller::setControllerManager(ControllerManager * theControllerManager)
{
	controllerManager = theControllerManager;
}

void Controller::setUp(bool isKnobBeingTouched, bool isLeftToggleBeingTouched, bool isLeftToggleUp, bool isRightToggleBeingTouched, bool isRightToggleUp)
{
	knobTouch = isKnobBeingTouched;
	leftToggleTouch = isLeftToggleBeingTouched;
	leftToggleUp = isLeftToggleUp;
	rightToggleTouch = isRightToggleBeingTouched;
	rightToggleUp = isRightToggleUp;
}
