/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CURSORCONTROLLER_H_INCLUDED
#define CURSORCONTROLLER_H_INCLUDED

#include "Controller.h"
#include "Assignment.h"
#include "DoubleTapChecker.h"
#include "ControllerManager.h"

#if JUCE_WIN32
	#include <Windows.h>
#endif

class CursorController : public Controller, public juce::MultiTimer
{
public:
	CursorController();
	~CursorController();

	void actionListenerCallback(const juce::String &message) override;

	void timerCallback(int timerID) override;

	void release() override;
    
    void setSensitivity(int sensitivity) override;
	void setPrimaryMouseButton(int MouseButtonID) override;

	void triggerAssignment(Assignment* assignment) override;
	void releaseCurrentAssignment() override;
	void assignToDefault() override;

	void setUp(bool isKnobBeingTouched, bool isLeftToggleBeingTouched, bool isLeftToggleUp, bool isRightToggleBeingTouched, bool isRightToggleUp) override;
 
private:
	Assignment defaultAssignment;

	juce::CriticalSection objectLock;

	int residueClicks;

	juce::Point<int> pointAtDragStart;
	juce::Point<int> pointAtDragEnd;

	//Undo stuff
	DoubleTapChecker knobTapChecker;

	//Circular / Diagonal stuff
	DoubleTapChecker rightToggleTapChecker;

	int currentDirectionChangeState;
	int savedDirectionStateOfAssignment;
	int savedDirectionState;

	void moveSmoothlyTo(juce::Point<float> point);

	int currentDirectionState;
	int currentRightToggleTouchState;
	int currentCircularOrDiagonalTriggerState;
	int currentOperationMode;
};


#endif // !CURSORCONTROLLER_H_INCLUDED
