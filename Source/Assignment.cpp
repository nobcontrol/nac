/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "WindowHelper.h"

#if JUCE_MAC
    #import <Cocoa/Cocoa.h>
    AXUIElementRef volumeIcon = NULL;
#endif

#include "Assignment.h"

Assignment::Assignment()
{
	point.x = 0;
	point.y = 0;

	controlMode = CursorControl;

	currentDirectionState = vertical;
	currentOperationMode = dragMode;

	radius = 40;
	phase = -juce::float_Pi / 2;
	radiusAndPhaseJustSet = false;
	radiusAndPhaseNeverSet = true;

	currentMovementIndex = 0;
	numberOfMovementsToUndo = 0;

	primaryMouseButton = leftMouseButton;
	mouseButtonIsBeingPressed = false;

	scrollWheelMode = ClassicMode;

	pointAtDragStart.x = 0;
	pointAtDragStart.y = 0;

	radiusHadToGetSetToOneForDirectDiagonalInteraction = false;
    
    assignmentPosition.nativeWindow = NULL;

#if JUCE_WIN32
	volumeOSD = NULL;
	lastOSDShown = NoOSD;
#endif
    
#if JUCE_MAC

#endif
};

Assignment::~Assignment()
{
#if JUCE_MAC
    if ((AXUIElementRef*)assignmentPosition.nativeWindow)
    {
//      CFRelease((AXUIElementRef)assignmentPosition.nativeWindow);
    }
#endif
}

Assignment& Assignment::operator=(const Assignment& other)
{
	controlMode = other.controlMode;

	assignmentPosition.nativeWindow = other.assignmentPosition.nativeWindow;
	assignmentPosition.relativePosition = other.assignmentPosition.relativePosition;

	point.x = other.point.x;
	point.y = other.point.y;

	pointAtDragStart.x = other.pointAtDragStart.x;
	pointAtDragStart.y = other.pointAtDragStart.y;

	currentDirectionState = other.currentDirectionState;
	currentOperationMode = other.currentOperationMode;

	radius = other.radius;
	phase = other.phase;
    
	clicksPerPixel = other.clicksPerPixel;
    
	primaryMouseButton = other.primaryMouseButton;
    scrollWheelMode = other.scrollWheelMode;
	
	for (int i = 0; i < 3; i++)
		movements[i] = other.movements[i];

	currentMovementIndex = other.currentMovementIndex;
	numberOfMovementsToUndo = other.numberOfMovementsToUndo;

	return *this;
}

void Assignment::updateAssignmentPosition()
{
	juce::Point<float> cursorPos = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();
	updateAssignmentPosition(cursorPos);
}

void Assignment::updateAssignmentPosition(juce::Point<float> cursorPos)
{
    DBG("Updating assignment's position...");
    
#if JUCE_WIN32
	juce::String title = assignmentPosition.title;
	juce::String owner = assignmentPosition.owner;

    if (!myWindowIsValid())
        assignmentPosition.nativeWindow = (void*)WindowHelper::getInstance()->getWindowAtPosition(cursorPos, owner, title);

	if ((HWND)assignmentPosition.nativeWindow)
	{
		HWND window = (HWND)assignmentPosition.nativeWindow;
		RECT rect;
		BOOL result = GetWindowRect(window, &rect);

		if (result)
		{
			assignmentPosition.relativePosition.x = (int)((LONG)cursorPos.x - rect.left);
			assignmentPosition.relativePosition.y = (int)((LONG)cursorPos.y - rect.top);

			assignmentPosition.owner = owner;
			assignmentPosition.title = title;

			DBG("Assignment's window info > owner: " + owner + " and title: " + title);
		}
		else
		{
			DBG("Unable to get window's position");

			assignmentPosition.relativePosition.x = (int)cursorPos.x;
			assignmentPosition.relativePosition.y = (int)cursorPos.y;
		}
	}
	else
	{
		DBG("No valid window for the assignment could be set.");

		assignmentPosition.relativePosition.x = (int)cursorPos.x;
		assignmentPosition.relativePosition.y = (int)cursorPos.y;
	}
#endif

#if JUCE_MAC
    juce::String title = assignmentPosition.title;
    juce::String owner = assignmentPosition.owner;
    
    if (!myWindowIsValid())
    {
    //  DBG("Assignment's window not valid. Trying to set another appropriate one...");
        assignmentPosition.nativeWindow = (AXUIElementRef*)WindowHelper::getInstance()->getWindowAtPosition(cursorPos, owner, title);
    }
	
    if ((AXUIElementRef*)assignmentPosition.nativeWindow)
	{
		AXError error;

		AXValueRef position;
		error = AXUIElementCopyAttributeValue(*(AXUIElementRef*)assignmentPosition.nativeWindow,
			kAXPositionAttribute,
			(CFTypeRef *)&position);

		if (error != kAXErrorSuccess)
		{
            DBG("Unable to get window's position");
            
			//  CFRelease(position);
			assignmentPosition.relativePosition.x = (int)cursorPos.x;
			assignmentPosition.relativePosition.y = (int)cursorPos.y;
			return;
		}

		CGPoint windowPosition;
		AXValueGetValue(position, (AXValueType)kAXValueCGPointType, &windowPosition);

		assignmentPosition.relativePosition.x = (int)(cursorPos.x - windowPosition.x);
		assignmentPosition.relativePosition.y = (int)(cursorPos.y - windowPosition.y);
        assignmentPosition.owner = owner;
        assignmentPosition.title = title;
        
		CFRelease(position);
	}
	else
	{
        DBG("No valid window for the assignment could be set.");
        
		assignmentPosition.relativePosition.x = (int)cursorPos.x;
		assignmentPosition.relativePosition.y = (int)cursorPos.y;
	}
#endif
}

juce::Point<int> Assignment::getGlobalPosition()
{
	juce::Point<int> result;
	result.x = 0;
	result.y = 0;

#if JUCE_WIN32
	HWND window = (HWND)assignmentPosition.nativeWindow;
	RECT rect;
	BOOL rectRetrieved = GetWindowRect(window, &rect);
	if (rectRetrieved)
	{
		result.x = assignmentPosition.relativePosition.x + (int)rect.left;
		result.y = assignmentPosition.relativePosition.y + (int)rect.top;
	}
	else
	{
		DBG("Unable to get window's position, in order to calculate target cursor position.");
	}
#endif
    
#if JUCE_MAC
    if (myWindowIsValid())
    {
        AXError error;
    
        AXValueRef position;
        error = AXUIElementCopyAttributeValue(*(AXUIElementRef*)assignmentPosition.nativeWindow,
                                              kAXPositionAttribute,
                                              (CFTypeRef *) &position);
        
        if (error != kAXErrorSuccess)
        {
            DBG("Unable to get window's position, in order to calculate target cursor position.");
            
            if (position!=NULL)
                CFRelease(position);
            
            return result;
        }
        
        CGPoint windowPosition;
        AXValueGetValue(position, (AXValueType)kAXValueCGPointType, &windowPosition);
        
        result.x = assignmentPosition.relativePosition.x + (int)windowPosition.x;
        result.y = assignmentPosition.relativePosition.y + (int)windowPosition.y;
        
        CFRelease(position);
    }
#endif

	return result;
}

bool Assignment::myWindowIsValid()
{
#if JUCE_WIN32
	HWND window = (HWND)assignmentPosition.nativeWindow;

	if (IsWindow(window))
	{
		bool result = true;

		if (!IsWindowVisible(window))
		{
			DWORD processID;
			GetWindowThreadProcessId(window, &processID);
			HWND parent = NULL;

			/*
			parent = GetAncestor(window, GA_ROOTOWNER);
			if (!IsIconic(parent))
			{
				result = false;
			}
			*/

			juce::Array<HWND> windows;
			do
			{
				parent = FindWindowEx(NULL, parent, NULL, NULL);
				DWORD dwPID = 0;
				GetWindowThreadProcessId(parent, &dwPID);
				if (dwPID == processID)
				{
					windows.add(parent);
				}
			} while (parent != NULL);

			bool parentIsIconic = false;
			for (int i = 0; i < windows.size(); i++)
			{
				if (IsIconic(windows[i]))
					parentIsIconic = true;
			}

			if (!parentIsIconic)
			{
				result = false;
			}
		}

		return result;
	}
#endif
    
#if JUCE_MAC
    CFStringRef role = NULL;
    AXError error;

    if (!(AXUIElementRef*)assignmentPosition.nativeWindow)
        return false;
    
    error = AXUIElementCopyAttributeValue(*(AXUIElementRef*)assignmentPosition.nativeWindow,
                                          kAXRoleAttribute,
                                          (CFTypeRef *) &role);
    if (error != kAXErrorSuccess)
    {
        DBG("Unable to get the role of the accesibility element.");
        
        if (role != NULL)
            CFRelease(role);
        
        return false;
    }
    else
    {
        if (CFStringCompare(role, kAXWindowRole, kCFCompareCaseInsensitive) == kCFCompareEqualTo)
            return true;
        else
        {
            DBG("Accesibility element's role is not that of a window.");
            return false;
        }
    }
#endif

	return false;
}

void Assignment::bringWindowToFront()
{
    DBG("Trying to bring window to front...");
    
#if JUCE_WIN32
	using namespace juce;
	HWND window = (HWND)assignmentPosition.nativeWindow;

	if (IsIconic(window))
	{
		//De-Minimize it
		ShowWindow(window, SW_RESTORE);
	}
	else if (!IsWindowVisible(window))
	{
		DWORD processID;
		GetWindowThreadProcessId(window, &processID);
		HWND parent = NULL;
		juce::Array<HWND> windows;
		do
		{
			parent = FindWindowEx(NULL, parent, NULL, NULL);
			DWORD dwPID = 0;
			GetWindowThreadProcessId(parent, &dwPID);
			if (dwPID == processID)
			{
				windows.add(parent);
			}
		} while (parent != NULL);

		bool parentIsIconic = false;
		for (int i = 0; i < windows.size(); i++)
		{
			if (IsIconic(windows[i]))
				ShowWindow(windows[i], SW_RESTORE);
		}

		/*
		HWND parent = GetAncestor(window, GA_ROOTOWNER);
		if (IsIconic(parent))
		{
			ShowWindow(parent, SW_RESTORE);
		}
		*/
	}

	juce::Point<int> globalPosition = getGlobalPosition();
	juce::Rectangle<int> bounds = juce::Desktop::getInstance().getDisplays().getTotalBounds(false);

	int minimumDimension = bounds.getWidth();
	if (bounds.getHeight() < minimumDimension)
		minimumDimension = bounds.getHeight();

	int xTranslation = 0;
	int yTranslation = 0;

	bool windowNeedsAdjusting = false;
	if (globalPosition.x > bounds.getWidth() + bounds.getX())
	{
		xTranslation = bounds.getWidth() + bounds.getX() - globalPosition.x - (int)(0.2f * (float)minimumDimension);
		windowNeedsAdjusting = true;
	}
	else if (globalPosition.x < bounds.getX())
	{
		xTranslation = bounds.getX() - globalPosition.x + (int)(0.2f * (float)minimumDimension);
		windowNeedsAdjusting = true;
	}

	if (globalPosition.y > bounds.getHeight() + bounds.getY())
	{
		yTranslation = bounds.getHeight() + bounds.getY() - globalPosition.y - (int)(0.2f * (float)minimumDimension);
		windowNeedsAdjusting = true;
	}
	else if (globalPosition.y < bounds.getY())
	{
		yTranslation = bounds.getY() -globalPosition.y + (int)(0.2f * (float)minimumDimension);
		windowNeedsAdjusting = true;
	}

	if (windowNeedsAdjusting)
	{
		DBG("Adjusting window's position...");

		RECT rect;
		BOOL rectRetrieved = GetWindowRect(window, &rect);
		if (rectRetrieved)
		{
			for (int i = 1; i <= 10; i++)
			{
				SetWindowPos(window, HWND_TOP, rect.left + (int)(((float)i/10.0f)*(float)xTranslation), rect.top + (int)(((float)i / 10.0f)*(float)yTranslation), rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW);
			}
		}
	}

	/* get the thread id that created the given window */
	DWORD thread_id = GetWindowThreadProcessId(window, NULL);
	/* get the desktop assigned to the specified thread id */
	HDESK desktop_handle = GetThreadDesktop(thread_id);

	SwitchDesktop(desktop_handle);
	SetForegroundWindow(window);
    
#endif
    
#if JUCE_MAC
    if (myWindowIsValid())
    {
        pid_t pid;
        AXUIElementGetPid(*(AXUIElementRef*)assignmentPosition.nativeWindow, &pid);
        
        CFBooleanRef isMinimized;
        AXUIElementCopyAttributeValue(*(AXUIElementRef*)assignmentPosition.nativeWindow, kAXMinimizedAttribute, (CFTypeRef*)&isMinimized);
        
        /*
        NSRunningApplication *app = [NSRunningApplication runningApplicationWithProcessIdentifier: pid];
        if (![app isActive])
        {
            DBG("Activating the application...");
            [app activateWithOptions: NSApplicationActivateIgnoringOtherApps];
            juce::Thread::sleep(50);
        }
        */
        
        DBG("Activating the application...");
        AXUIElementRef axApp = AXUIElementCreateApplication(pid);
        AXError error = AXUIElementSetAttributeValue(axApp, kAXFrontmostAttribute, kCFBooleanTrue);
        if (error != kAXErrorSuccess)
        {
            DBG("Failed to activate application");
        }
        
        if (!WindowHelper::getInstance()->isAXWindowOnScreen(*(AXUIElementRef*)assignmentPosition.nativeWindow))
        {
            DBG("Window not on screen..waiting...");
            juce::Thread::sleep(380);
            DBG("Trying to activate the application again...");
            error = AXUIElementSetAttributeValue(axApp, kAXFrontmostAttribute, kCFBooleanTrue);
        }
        
        
            
        AXUIElementSetAttributeValue(*(AXUIElementRef*)assignmentPosition.nativeWindow, kAXMainAttribute, kCFBooleanTrue);
        AXUIElementSetAttributeValue(*(AXUIElementRef*)assignmentPosition.nativeWindow, kAXFocusedWindowAttribute, kCFBooleanTrue);
        AXUIElementPerformAction(*(AXUIElementRef*)assignmentPosition.nativeWindow, kAXRaiseAction);
        
        AXValueRef position;
        error = AXUIElementCopyAttributeValue(*(AXUIElementRef*)assignmentPosition.nativeWindow,
                                              kAXPositionAttribute,
                                              (CFTypeRef *) &position);
        
        if (error != kAXErrorSuccess)
            return;
        
        CGPoint windowPosition;
        AXValueGetValue(position, (AXValueType)kAXValueCGPointType, &windowPosition);
        CFRelease(position);
        
        juce::Point<int> globalPosition = getGlobalPosition();
        juce::Rectangle<int> bounds = juce::Desktop::getInstance().getDisplays().getTotalBounds(false);
        
        int minimumDimension = bounds.getWidth();
        if (bounds.getHeight() < minimumDimension)
            minimumDimension = bounds.getHeight();
        
        int xTranslation = 0;
        int yTranslation = 0;
        
        bool windowNeedsAdjusting = false;
        if (globalPosition.x > bounds.getWidth() + bounds.getX())
        {
            xTranslation = bounds.getWidth() + bounds.getX() - globalPosition.x - (int)(0.2f * (float)minimumDimension);
            windowNeedsAdjusting = true;
        }
        else if (globalPosition.x < bounds.getX())
        {
            xTranslation = bounds.getX() - globalPosition.x + (int)(0.2f * (float)minimumDimension);
            windowNeedsAdjusting = true;
        }
        
        if (globalPosition.y > bounds.getHeight() + bounds.getY())
        {
            yTranslation = bounds.getHeight() + bounds.getY() - globalPosition.y - (int)(0.2f * (float)minimumDimension);
            windowNeedsAdjusting = true;
        }
        else if (globalPosition.y < bounds.getY())
        {
            yTranslation = bounds.getY() -globalPosition.y + (int)(0.2f * (float)minimumDimension);
            windowNeedsAdjusting = true;
        }
        
        if (windowNeedsAdjusting)
        {
            DBG("Adjusting window's position...");
            for (int i = 1; i <= 10; i++)
            {
                CGPoint position;
                position.x = windowPosition.x + ((float)i/10.0f)*(float)xTranslation;
                position.y = windowPosition.y + ((float)i/10.0f)*(float)yTranslation;
                
                CFTypeRef newPosition = NULL;
                newPosition = (CFTypeRef)AXValueCreate(kAXValueTypeCGPoint, (const void *)(&position));

                AXUIElementSetAttributeValue(*(AXUIElementRef*)assignmentPosition.nativeWindow,  kAXPositionAttribute, newPosition);
            }
        }
        
    }
    else
    {
        DBG("Window is not valid.");
    }
    
#endif
}

void Assignment::mouseClick()
{
#if JUCE_WIN32
	INPUT input[1];
	input[0].type = 0;
	input[0].mi.dx = 0;
	input[0].mi.dy = 0;
	if (primaryMouseButton == leftMouseButton)
		input[0].mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
	else
		input[0].mi.dwFlags = MOUSEEVENTF_RIGHTDOWN;
	input[0].mi.mouseData = 0;
	input[0].mi.time = 0;
	input[0].mi.dwExtraInfo = NULL;

	SendInput(1, input, sizeof(input[0]));
	BlockInput(true);
#endif // !JUCE_WIN32

#if JUCE_MAC
    juce::Point<float> cursorPos = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();
 /*
    CGEventRef clickevent;
    if (primaryMouseButton == leftMouseButton)
        clickevent = CGEventCreateMouseEvent(NULL, kCGEventLeftMouseDown, CGPointMake((int)cursorPos.x, (int)cursorPos.y), kCGMouseButtonLeft);
    else
        clickevent = CGEventCreateMouseEvent(NULL, kCGEventRightMouseDown, CGPointMake((int)cursorPos.x, (int)cursorPos.y), kCGMouseButtonRight);
    CGEventPost(kCGHIDEventTap, clickevent);
    CFRelease(clickevent);
 */
    if (primaryMouseButton == leftMouseButton)
        CGPostMouseEvent(CGPointMake((int)cursorPos.x, (int)cursorPos.y), false, 2, true, false);
    else
        CGPostMouseEvent(CGPointMake((int)cursorPos.x, (int)cursorPos.y), false, 2, false, true);

    
#endif // !JUCE_MAC

	mouseButtonIsBeingPressed = true;
}

void Assignment::mouseRelease()
{
	if (mouseButtonIsBeingPressed)
	{
#if JUCE_WIN32
		INPUT input[1];
		input[0].type = 0;
		input[0].mi.dx = 0;
		input[0].mi.dy = 0;
		if (primaryMouseButton == leftMouseButton)
			input[0].mi.dwFlags = MOUSEEVENTF_LEFTUP;
		else
			input[0].mi.dwFlags = MOUSEEVENTF_RIGHTUP;
		input[0].mi.mouseData = 0;
		input[0].mi.time = 0;
		input[0].mi.dwExtraInfo = NULL;

		SendInput(1, input, sizeof(input[0]));
		BlockInput(false);
#endif // !JUCE_WIN32

#if JUCE_MAC
        juce::Point<float> cursorPos = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();
     /*
        CGEventRef clickevent;
        if (primaryMouseButton == leftMouseButton)
            clickevent = CGEventCreateMouseEvent(NULL, kCGEventLeftMouseUp, CGPointMake((int)cursorPos.x, (int)cursorPos.y), kCGMouseButtonLeft);
        else
            clickevent = CGEventCreateMouseEvent(NULL, kCGEventRightMouseUp, CGPointMake((int)cursorPos.x, (int)cursorPos.y), kCGMouseButtonRight);
        CGEventPost(kCGHIDEventTap, clickevent);
        CFRelease(clickevent);
     */

        CGPostMouseEvent(CGPointMake((int)cursorPos.x, (int)cursorPos.y), false, 2, false, false);
#endif // !JUCE_MAC

		if (currentOperationMode == dragMode) //Wait a little bit for the mouse cursor to appear (for example with Waves plugins), and then save the position.
		{
			juce::Thread::sleep(10);
            juce::Point<float> cursorPos = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();
			point.x = (int)cursorPos.x;
			point.y = (int)cursorPos.y;
		}
	}

	waitForCursorToAppear();

	mouseButtonIsBeingPressed = false;
}

void Assignment::moveCursor(int x, int y)
{
    juce::Point<float> cursorpoint = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();
	int newX = (int)cursorpoint.x + x;
	int newY = (int)cursorpoint.y + y;
    
#if JUCE_MAC
//  CGEventRef dragevent;
    if (mouseButtonIsBeingPressed)
    {
        /*
        if (primaryMouseButton == leftMouseButton)
            dragevent = CGEventCreateMouseEvent(NULL, kCGEventLeftMouseDragged, CGPointMake(newX, newY), kCGMouseButtonLeft);
        else
            dragevent = CGEventCreateMouseEvent(NULL, kCGEventRightMouseDragged, CGPointMake(newX, newY), kCGMouseButtonRight);
    
        CGEventPost(kCGHIDEventTap, dragevent);
        CFRelease(dragevent);
        */
        
        if (primaryMouseButton == leftMouseButton)
            CGPostMouseEvent(CGPointMake(newX, newY), true, 2, true, false);
        else
            CGPostMouseEvent(CGPointMake(newX, newY), true, 2, false, true);
    }
	else
	{
		juce::Desktop::getInstance().setMousePosition(juce::Point<int>(newX, newY));
		return;
	}
#endif

#if JUCE_WIN32
	//  juce::Desktop::getInstance().setMousePosition(juce::Point<int>(newX, newY));

	INPUT input[1];
	input[0].type = 0;
	input[0].mi.dx = x;
	input[0].mi.dy = y;

	input[0].mi.mouseData = 0;

	input[0].mi.dwFlags = MOUSEEVENTF_MOVE;
	input[0].mi.time = 0;
	input[0].mi.dwExtraInfo = NULL;
	SendInput(1, input, sizeof(input[0]));
#endif

}

void Assignment::dragCursorInReverse(MovementRecorder& mr)
{
    //Move horizontally
    for (int i = 0; i < mr.maxMovement; i++)
    {
        for (int ii = 0; ii < abs(mr.horizontalMoveCounts[i]); ii++)
        {
            int dx;
            if (mr.horizontalMoveCounts[i]>0)
                dx = - (i + 1);
            else
                dx = + (i + 1);
            
			moveCursor(dx, 0);
            juce::Thread::sleep(2);
        }
    }
    
    //Move vertically
    for (int i = 0; i < mr.maxMovement; i++)
    {
        for (int ii = 0; ii < abs(mr.verticalMoveCounts[i]); ii++)
        {
            int dy;
            if (mr.verticalMoveCounts[i]>0)
                dy = - (i + 1);
            else
                dy = + (i + 1);
            
			moveCursor(0, dy);
            juce::Thread::sleep(2);
        }
    }
}

void Assignment::scrollInReverse(MovementRecorder & mr)
{
	//Scroll horizontally
	for (int i = 0; i < mr.maxMovement; i++)
	{
		for (int ii = 0; ii < abs(mr.horizontalMoveCounts[i]); ii++)
		{
			int dx;
			if (mr.horizontalMoveCounts[i]>0)
				dx = -(i + 1);
			else
				dx = +(i + 1);

			scroll(dx, 0);
			juce::Thread::sleep(2);
		}
	}

	//Scroll vertically
	for (int i = 0; i < mr.maxMovement; i++)
	{
		for (int ii = 0; ii < abs(mr.verticalMoveCounts[i]); ii++)
		{
			int dy;
			if (mr.verticalMoveCounts[i]>0)
				dy = -(i + 1);
			else
				dy = +(i + 1);

			scroll(0, dy);
			juce::Thread::sleep(2);
		}
	}
}

void Assignment::setCurrentVolume()
{
	currentVolume = juce::SystemAudioVolume::getGain();
}

int Assignment::masterVolumeChange(int change)
{
#if JUCE_WIN32
    int trueChange = 0;
	if (change != 0)
	{
	//	float gain = juce::SystemAudioVolume::getGain();
		float gain = currentVolume;

		//if the last OSD shown does not correspond to volume then press the right key to make it appear

		if (lastOSDShown != VolumeOSD)
		{
			INPUT ip;

			ip.type = INPUT_KEYBOARD;
			ip.ki.wScan = 0; // hardware scan code for key
			ip.ki.time = 0;
			ip.ki.dwExtraInfo = 0;

			if (change<0)
				ip.ki.wVk = VK_VOLUME_DOWN; // virtual-key code for the "a" key
			else
				ip.ki.wVk = VK_VOLUME_UP; // virtual-key code for the "a" key

			ip.ki.dwFlags = 0; // 0 for key press
			SendInput(1, &ip, sizeof(INPUT));

			ip.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
			SendInput(1, &ip, sizeof(INPUT));

			volumeOSD = getVolumeOSD();
			lastOSDShown = VolumeOSD;
		}

		if (juce::SystemAudioVolume::isMuted())
			juce::SystemAudioVolume::setMuted(false);

		float gainChange = juce::SystemAudioVolume::getGain() - gain;
		float scaledChange = (float)change / 256.0f; //Change purely based on knob rotation

		gain += scaledChange - gainChange;

		if (gain>1.0f)
			gain = 1.0f;
		else if (gain<0.0f)
			gain = 0.0f;

		trueChange = (int)(256.0f*(gain - currentVolume));
        
		juce::SystemAudioVolume::setGain(gain);
		currentVolume = gain;
	}

	showVolumeOSD();
    return trueChange;
#endif
    
#if JUCE_MAC
    if (change != 0)
    {
        float scaledChange = (float)change / 256.0f;
        
        float gain = currentVolume;
        
        gain += scaledChange;
        
        if (gain>1.0f)
            gain = 1.0f;
        else if (gain<0.0f)
            gain = 0.0f;
        
        int trueChange = (int)(256.0f*(gain - currentVolume));
        
        currentVolume = gain;
        
        if (juce::SystemAudioVolume::isMuted())
            juce::SystemAudioVolume::setMuted(false);
        
        juce::SystemAudioVolume::setGain(currentVolume);
        
        return trueChange;
    }
    return change;
#endif
}

void Assignment::undoMasterVolumeChange()
{
    setCurrentVolume();
    
	//Add all movements together
	MovementRecorder summedMovement;

	if (numberOfMovementsToUndo == 1)
	{
		summedMovement.addMovements(movements[currentMovementIndex]);
	}
	else if (numberOfMovementsToUndo == 2)
	{
		summedMovement.addMovements(movements[currentMovementIndex]);
		decrementCurrentMovementIndex();
		summedMovement.addMovements(movements[currentMovementIndex]);
	}
	else if (numberOfMovementsToUndo == 3)
	{
		summedMovement.addMovements(movements[0]);
		summedMovement.addMovements(movements[1]);
		summedMovement.addMovements(movements[2]);
	}

	masterVolumeChangeInReverse(summedMovement);

	clearMovements();

	//Add redo stuff
	summedMovement.reverseMovement();
	movements[0].addMovements(summedMovement);
	currentMovementIndex = 0;
	numberOfMovementsToUndo = 1;
	
#if JUCE_WIN32
	hideVolumeOSD();
#endif // JUCE_WIN32
    
#if JUCE_MAC
    hideVolumeIndication();
#endif

}

void Assignment::masterVolumeChangeInReverse(MovementRecorder & mr)
{
	//Brightness 
	for (int i = 0; i < mr.maxMovement; i++)
	{
		for (int ii = 0; ii < abs(mr.horizontalMoveCounts[i]); ii++)
		{
			int dx;
			if (mr.horizontalMoveCounts[i]>0)
				dx = -(i + 1);
			else
				dx = +(i + 1);

			masterVolumeChange(dx);
			juce::Thread::sleep(2);
		}
	}

	//Volume
	for (int i = 0; i < mr.maxMovement; i++)
	{
		for (int ii = 0; ii < abs(mr.verticalMoveCounts[i]); ii++)
		{
			int dy;
			if (mr.verticalMoveCounts[i]>0)
				dy = -(i + 1);
			else
				dy = +(i + 1);

			masterVolumeChange(dy);
			juce::Thread::sleep(2);
		}
	}
}

void Assignment::performUndo()
{
	//Add all movements together
	MovementRecorder summedMovement;

	if (numberOfMovementsToUndo == 1)
	{
		summedMovement.addMovements(movements[currentMovementIndex]);
	}
	else if (numberOfMovementsToUndo == 2)
	{
		summedMovement.addMovements(movements[currentMovementIndex]);
		decrementCurrentMovementIndex();
		summedMovement.addMovements(movements[currentMovementIndex]);
	}
	else if (numberOfMovementsToUndo == 3)
	{
		summedMovement.addMovements(movements[0]);
		summedMovement.addMovements(movements[1]);
		summedMovement.addMovements(movements[2]);
	}

	juce::Point<float> point = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();
	
	mouseClick();
    dragCursorInReverse(summedMovement);
	mouseRelease();
	if (currentOperationMode == stationaryMode)
	{
		pointAtDragStart.x = (int)point.x;
		pointAtDragStart.y = (int)point.y;
		juce::Desktop::getInstance().setMousePosition(pointAtDragStart);
	}
	else
	{
		updateAssignmentPosition();

        juce::Point<float> cursorPoint = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();
		point.x = (int)cursorPoint.x;
		point.y = (int)cursorPoint.y;
	}

	clearMovements();

	//Add redo stuff
	summedMovement.reverseMovement();
	movements[0].addMovements(summedMovement);
	currentMovementIndex = 0;
	numberOfMovementsToUndo = 1;
}

void Assignment::waitForCursorToAppear()
{
	juce::int64 startTime = juce::Time::currentTimeMillis();
	juce::int64 currentTime = startTime;
	juce::int64 timePassed = 0;
	juce::int64 timeOut = 400;
	bool cursorIsVisible = false;

	do
	{
		currentTime = juce::Time::currentTimeMillis();
		timePassed = currentTime - startTime;

#if JUCE_WIN32
		CURSORINFO cursorInfo;
		cursorInfo.cbSize = sizeof(cursorInfo);
		GetCursorInfo(&cursorInfo);
		cursorIsVisible = (cursorInfo.flags == CURSOR_SHOWING) ? true : false;
#endif
        
#if JUCE_MAC
        cursorIsVisible = CGCursorIsVisible();
#endif

	} while (timePassed<timeOut && !cursorIsVisible);
    
    juce::Thread::sleep(10);  //Just to be sure.
}

void Assignment::incrementCurrentMovementIndex()
{
	currentMovementIndex++;
	if (currentMovementIndex > 2)
		currentMovementIndex = 0;
}

void Assignment::decrementCurrentMovementIndex()
{
	currentMovementIndex--;
	if (currentMovementIndex < 0)
		currentMovementIndex = 2;
}

void Assignment::clearMovements()
{
	for (int i = 0; i < 3; i++)
	{
		movements[i].clear();
	}
}

void Assignment::setRadiusAndPhase()
{
	juce::Point<float> cursorpoint = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();
	int xdiff = (int)cursorpoint.x - pointAtDragStart.x;
	int ydiff = (int)cursorpoint.y - pointAtDragStart.y;
	radius = (int)(sqrtf((float)xdiff*(float)xdiff + (float)ydiff*(float)ydiff));

	if (xdiff == 0)
	{
		phase = (ydiff > 0) ? juce::float_Pi / 2 : -juce::float_Pi / 2;
	}
	else if (ydiff == 0)
	{
		phase = (xdiff > 0) ? 0 : -juce::float_Pi;
	}
	else
	{
		phase = atan((float)ydiff / (float)xdiff);
		if (xdiff < 0)
			phase += juce::float_Pi;
	}

	radiusAndPhaseNeverSet = false;

	//Logger::outputDebugString("Set radius and phase");
}

void Assignment::moveToCircle(bool recordMovement)
{
	float y = (float)radius * sin(phase);
	float x = (float)radius * cos(phase);
	float len = sqrt(x * x + y * y);
	float movedX = 0;
	float movedY = 0;

	long iterations = (long)(len / 2.0f);
	float lengthPerIteration = 2.0f;

	for (long i = 0; i < iterations; i++)
	{
		float xposition = i * lengthPerIteration * cos(phase);
		float yposition = i * lengthPerIteration * sin(phase);

		int xmovement = (int)(xposition - movedX);
		int ymovement = (int)(yposition - movedY);

		moveCursor(xmovement, ymovement);
		if (recordMovement)
			movements[currentMovementIndex].addMovement(xmovement, ymovement);
		juce::Thread::sleep(1);

		movedX += (float)xmovement;
		movedY += (float)ymovement;
	}


	moveCursor((int)(x - movedX), (int)(y - movedY));
	if (recordMovement)
		movements[currentMovementIndex].addMovement((int)(x - movedX), (int)(y - movedY));
	juce::Thread::sleep(1);
}

void Assignment::setClicksPerPixel(int cpp)
{
	clicksPerPixel = cpp;
}

void Assignment::scroll(int x, int y)
{
#if JUCE_WIN32
	if (x != 0)
	{
		INPUT input[1];
		input[0].type = 0;
		input[0].mi.dx = 0;
		input[0].mi.dy = 0;

		if (scrollWheelMode == ClassicMode)
			input[0].mi.mouseData = x;
		else if (scrollWheelMode == NaturalMode)
			input[0].mi.mouseData = -x;

		input[0].mi.dwFlags = MOUSEEVENTF_HWHEEL;
		input[0].mi.time = 0;
		input[0].mi.dwExtraInfo = NULL;
		SendInput(1, input, sizeof(input[0]));
	}

	if (y != 0)
	{
		INPUT input[1];
		input[0].type = 0;
		input[0].mi.dx = 0;
		input[0].mi.dy = 0;

		if (scrollWheelMode == ClassicMode)
			input[0].mi.mouseData = -y;
		else if (scrollWheelMode == NaturalMode)
			input[0].mi.mouseData = y;

		input[0].mi.dwFlags = MOUSEEVENTF_WHEEL;
		input[0].mi.time = 0;
		input[0].mi.dwExtraInfo = NULL;
		SendInput(1, input, sizeof(input[0]));
	}
#endif // !JUCE_WIN32
    
#if JUCE_MAC
   // juce::Point<float> cursorPos = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();
    if (x != 0)
    {
        bool isNegative = false;
        if (x<0)
            isNegative = true;
        
        x = x/4;
        x = x*x;
        
        if (isNegative)
            x *=-1;
        
        CGEventRef scrollevent;
        
        if (scrollWheelMode == NaturalMode)
            scrollevent = CGEventCreateScrollWheelEvent(NULL, kCGScrollEventUnitPixel, 2, 0, x);
        else
            scrollevent = CGEventCreateScrollWheelEvent(NULL, kCGScrollEventUnitPixel, 2, 0, -x);

        CGEventPost(kCGHIDEventTap, scrollevent);
        CFRelease(scrollevent);
    }
    
    if (y != 0)
    {
        bool isNegative = false;
        if (y<0)
            isNegative = true;
        
        y = y/4;
        y = y*y;
        
        if (isNegative)
            y *=-1;
        
        CGEventRef scrollevent;
        
        if (scrollWheelMode == NaturalMode)
            scrollevent = CGEventCreateScrollWheelEvent(NULL, kCGScrollEventUnitPixel, 1, y);
        else
            scrollevent = CGEventCreateScrollWheelEvent(NULL, kCGScrollEventUnitPixel, 1, -y);
        
        CGEventPost(kCGHIDEventTap, scrollevent);
        CFRelease(scrollevent);
    }
#endif
    
}

void Assignment::undoScroll()
{
	//Add all movements together
	MovementRecorder summedMovement;

	if (numberOfMovementsToUndo == 1)
	{
		summedMovement.addMovements(movements[currentMovementIndex]);
	}
	else if (numberOfMovementsToUndo == 2)
	{
		summedMovement.addMovements(movements[currentMovementIndex]);
		decrementCurrentMovementIndex();
		summedMovement.addMovements(movements[currentMovementIndex]);
	}
	else if (numberOfMovementsToUndo == 3)
	{
		summedMovement.addMovements(movements[0]);
		summedMovement.addMovements(movements[1]);
		summedMovement.addMovements(movements[2]);
	}

	scrollInReverse(summedMovement);

	clearMovements();

	//Add redo stuff
	summedMovement.reverseMovement();
	movements[0].addMovements(summedMovement);
	currentMovementIndex = 0;
	numberOfMovementsToUndo = 1;
}

void Assignment::updatePosition()
{
	juce::Point<float> cursor = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();
	point.x = (int)cursor.x;
	point.y = (int)cursor.y;
}

MovementRecorder::MovementRecorder()
{
	clear();
}

MovementRecorder::~MovementRecorder()
{
}

MovementRecorder & MovementRecorder::operator=(const MovementRecorder & other)
{
	for (int i = 0; i < maxMovement; i++)
	{
		horizontalMoveCounts[i] = other.horizontalMoveCounts[i];
		verticalMoveCounts[i] = other.verticalMoveCounts[i];
	}

	return *this;
}

void MovementRecorder::clear()
{
	for (int i = 0; i < maxMovement; i++)
	{
		verticalMoveCounts[i] = 0;
		horizontalMoveCounts[i] = 0;
	}
}

void MovementRecorder::addMovement(int x, int y)
{
	if (abs(x) > maxMovement) //In case there are big input vectors during circular movements
	{
		int movecounts = x / maxMovement;
		horizontalMoveCounts[maxMovement - 1] += movecounts;

		x = x - movecounts*maxMovement;
	}

	if (abs(y) > maxMovement) //In case there are big input vectors during circular movements
	{
		int movecounts = y / maxMovement;
		verticalMoveCounts[maxMovement - 1] += movecounts;

		y = y - movecounts*maxMovement;
	}

	if (x > 0)
		horizontalMoveCounts[x - 1]++;
	else if (x < 0)
		horizontalMoveCounts[-x - 1]--;

	if (y > 0)
		verticalMoveCounts[y - 1]++;
	else if (y < 0)
		verticalMoveCounts[-y - 1]--;
}

void MovementRecorder::addMovements(MovementRecorder & movementRecorder)
{
	for (int i = 0; i<maxMovement; i++)
	{
		horizontalMoveCounts[i] += movementRecorder.horizontalMoveCounts[i];
		verticalMoveCounts[i] += movementRecorder.verticalMoveCounts[i];
	}
}

void MovementRecorder::moveCursorInReverse()
{
	//Move horizontally
	for (int i = 0; i < maxMovement; i++)
	{
		for (int ii = 0; ii < abs(horizontalMoveCounts[i]); ii++)
		{
			juce::Point<float> point = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();
			int newX;
			if (horizontalMoveCounts[i]>0)
				newX = (int)point.x - (i + 1);
			else
				newX = (int)point.x + (i + 1);

			int newY = (int)point.y;

			juce::Desktop::getInstance().setMousePosition(juce::Point<int>(newX, newY));
            juce::Thread::sleep(1);
		}
	}

	//Move vertically
	for (int i = 0; i < maxMovement; i++)
	{
		for (int ii = 0; ii < abs(verticalMoveCounts[i]); ii++)
		{
			juce::Point<float> point = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();
			int newX = (int)point.x;

			int newY;
			if (verticalMoveCounts[i]>0)
				newY = (int)point.y - (i + 1);
			else
				newY = (int)point.y + (i + 1);

			juce::Desktop::getInstance().setMousePosition(juce::Point<int>(newX, newY));
            juce::Thread::sleep(1);
		}
	}

}

void MovementRecorder::reverseMovement()
{
	for (int i = 0; i < maxMovement; i++)
	{
		verticalMoveCounts[i] *= -1;
		horizontalMoveCounts[i] *= -1;
	}
}

#if JUCE_WIN32

HWND Assignment::getVolumeOSD()
{
	HWND hwndRet = NULL;
	HWND hwndHost = NULL;

	int pairCount = 0;

	// search for all windows with class 'NativeHWNDHost'

	while ((hwndHost = FindWindowEx(NULL, hwndHost, "NativeHWNDHost", "")) != NULL)
	{
		// if this window has a child with class 'DirectUIHWND' it might be the volume OSD

		if (FindWindowEx(hwndHost, NULL, "DirectUIHWND", "") != NULL)
		{
			// if this is the only pair we are sure

			if (pairCount == 0)
			{
				hwndRet = hwndHost;
			}

			pairCount++;

			// if there are more pairs the criteria has failed...

			if (pairCount > 1)
			{
				return NULL;
			}
		}
	}

	return hwndRet;
}

void  Assignment::showVolumeOSD()
{
	if (volumeOSD == NULL)
	{
		volumeOSD = getVolumeOSD();
		SetWindowLongPtr(volumeOSD, GWL_EXSTYLE, GetWindowLongPtr(volumeOSD, GWL_EXSTYLE) ^ WS_EX_LAYERED);
	}

	SetLayeredWindowAttributes(volumeOSD, 0, 255, LWA_ALPHA);
	ShowWindow(volumeOSD, SW_SHOW);
}

void  Assignment::hideVolumeOSD()
{
	if (volumeOSD == NULL)
		volumeOSD = getVolumeOSD();

	ShowWindow(volumeOSD, SW_HIDE);
}

#endif

#if JUCE_MAC

void Assignment::showVolumeIndication()
{
    /*
    if (!volumeIcon)
        volumeIcon = (AXUIElementRef)getVolumeIndication();
    
    if (volumeIcon)
    {
        AXUIElementPerformAction(volumeIcon, kAXPressAction);
    }
    */

}

void Assignment::hideVolumeIndication()
{
    /*
    if (volumeIcon)
    {
        AXUIElementRef childrenRefs[1];
        CFArrayRef children;
        CFArrayCreate(kCFAllocatorDefault, (const void **)&childrenRefs, 1, 0);

        AXError error = AXUIElementCopyAttributeValues(volumeIcon, kAXChildrenAttribute, 0, 1, &children);
        
        if (error == kAXErrorSuccess)
        {
            AXUIElementRef child = (AXUIElementRef)CFArrayGetValueAtIndex(children, 0);
            AXUIElementPerformAction(child, kAXCancelAction);
            CFRelease(children);
        }
    }
     */
    
}

void* Assignment::getVolumeIndication()
{
    AXUIElementRef volumeIndication = NULL;
    
    pid_t resultPid = -1;
    NSArray *runningApplications = [[NSWorkspace sharedWorkspace] runningApplications];
    for (NSRunningApplication *app in runningApplications)
    {
        pid_t pid = [app processIdentifier];
        if (pid != ((pid_t)-1))
        {
            AXUIElementRef appl = AXUIElementCreateApplication(pid);
            CFTypeRef result = NULL;
            if(AXUIElementCopyAttributeValue(appl, kAXTitleAttribute, (CFTypeRef *)&result) == kAXErrorSuccess)
            {
                if([((NSString*)result) isEqualToString:@"SystemUIServer"])
                {
                    resultPid = pid;
                    break;
                }
            }
        }
    }
    
    if (resultPid != -1)
    {
        AXError error;
        AXUIElementRef uiSystem = AXUIElementCreateApplication(resultPid);
        AXUIElementRef menuBar = NULL;
        error = AXUIElementCopyAttributeValue(uiSystem, kAXMenuBarAttribute, (CFTypeRef *)&menuBar);
        CFRelease(uiSystem);
        if (error == kAXErrorSuccess)
        {
            CFArrayRef children;
            error = AXUIElementCopyAttributeValue(menuBar, kAXChildrenAttribute, (CFTypeRef *)&children);
            CFRelease(menuBar);
            if (error == kAXErrorSuccess)
            {
                for (CFIndex i = 0; i < CFArrayGetCount(children); i++)
                {
                    AXUIElementRef anElement = (AXUIElementRef)CFArrayGetValueAtIndex(children, i);
                    CFStringRef label;
                    error = AXUIElementCopyAttributeValue(anElement, kAXDescriptionAttribute, (CFTypeRef *)&label);
                    if (error == kAXErrorSuccess)
                    {
                        CFIndex length = CFStringGetLength(label);
                        CFIndex maxSize = CFStringGetMaximumSizeForEncoding(length, kCFStringEncodingUTF8) + 1;
                        char *buffer = (char *)malloc(maxSize);
                        if (CFStringGetCString(label, buffer, maxSize, kCFStringEncodingUTF8))
                        {
                            juce::String theLabel = juce::String(juce::CharPointer_UTF8(buffer));
                            if (theLabel.containsIgnoreCase(juce::String("System")))
                            {
                                volumeIndication = anElement;
                                CFRetain(volumeIndication);
                            }
                        }
                        
                        free(buffer);
                        CFRelease(label);
                    }
                }
                CFRelease(children);
            }
        }
    }
    
    return (void*)volumeIndication;
}



#endif

