/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LOWLEVELKEYBOARDLISTENER_H_INCLUDED
#define LOWLEVELKEYBOARDLISTENER_H_INCLUDED

#if __APPLE__
#include <ApplicationServices/ApplicationServices.h>
#include <Carbon/Carbon.h>
#endif

#include "QuickAssignmentsPanel.h"
#include "ControllerManager.h"

#include "../JuceLibraryCode/JuceHeader.h"
using namespace juce;

#if JUCE_WIN32
#include <Windows.h>
#endif

class LowLevelKeyboardListener : public Thread, public ActionBroadcaster
{
private:
	bool hookSet;
	static bool isHookActive;

	static QuickAssignmentsPanel* quickAssignmentsPanel;
	static ControllerManager* controllerManager;
	static ActionBroadcaster* broadcaster;
	static SerialInterface* serialInterface;

public:
	LowLevelKeyboardListener() : Thread("Keyboard Thread")
	{
		hookSet = false;
		isHookActive = true;
		quickAssignmentsPanel = nullptr;
		controllerManager = nullptr;
		serialInterface = nullptr;
	}

	~LowLevelKeyboardListener()
	{
#if JUCE_WIN32
		if (hookSet)
			UnhookWindowsHookEx(lowlevelhook);
#endif
        
#if JUCE_MAC
        if (hookSet)
        {
     
        }
#endif

		if (broadcaster)
			delete broadcaster;

		clearSingletonInstance();
	}

	void run()
	{
#if JUCE_WIN32
        while (!threadShouldExit())
        {
			/*
            if (!hookSet)
            {
                lowlevelhook = SetWindowsHookEx(WH_KEYBOARD_LL, LowLevelKeyboardProc, NULL, 0);
                hookSet = true;
            }
			*/
            juce::Thread::sleep(2);
        }
#endif
        
#if JUCE_MAC
        while (!threadShouldExit())
        {
            if (!hookSet)
            {
                // Create an event tap. We are interested in key presses.
                DBG("Creating mask for receiving key down events...");
                eventMask = CGEventMaskBit(kCGEventKeyDown);
            
                DBG("Creating event tap for key events...");
                eventTap = CGEventTapCreate(kCGHIDEventTap, kCGHeadInsertEventTap, kCGEventTapOptionDefault, eventMask, LowLevelKeyboardListener::myCGEventCallback, NULL);
                
                
                if (eventTap == NULL)
                {
                    DBG("Event tap could not be created. Exiting thread...");
                    return;
                }
                
                // Create a run loop source.
                DBG("Creating the run loop source...");
                runLoopSource = CFMachPortCreateRunLoopSource(kCFAllocatorDefault, eventTap, 0);
            
                // Add to the current run loop.
                DBG("Adding the run loop source to thread's run loop...");
                runLoop = CFRunLoopGetCurrent();
                CFRunLoopAddSource(runLoop, runLoopSource, kCFRunLoopCommonModes);
            
                // Enable the event tap.
                DBG("Enabling the event tap...");
                CGEventTapEnable(eventTap, true);
            
                hookSet = true;
                
                DBG("Starting the run loop...");
                CFRunLoopRun();
            }
        }
#endif
	}

	void setActive(bool shouldBeActive)
	{
		isHookActive = shouldBeActive;
	}
	void setQuickAssignmentsPanel(QuickAssignmentsPanel* theQuickAssignmentsPanel)
	{
		quickAssignmentsPanel = theQuickAssignmentsPanel;
	}
	void setControllerManager(ControllerManager* theControllerManager)
	{
		controllerManager = theControllerManager;
		broadcaster = new ActionBroadcaster();
		broadcaster->addActionListener(controllerManager);
	}

	void setSerialInterface(SerialInterface* theSerialInterface)
	{
		serialInterface = theSerialInterface;
	}

	static bool executeShortcut(KeypressExtended &shortcut)
	{
		bool shortcutExists = false;
    
		if (quickAssignmentsPanel != nullptr)
		{
			if (shortcut.knobModifier && shortcut.keyPress == KeyPress::leftKey)
			{
				if (broadcaster)
                {
                    DBG("Trying to activate the assignment on the left...");
					broadcaster->sendActionMessage(String("triggerleft"));
                }

				shortcutExists = true;
			}
			else if (shortcut.knobModifier && shortcut.keyPress == KeyPress::rightKey)
			{
				if (broadcaster)
                {
                    DBG("Trying to activate the assignment on the right...");
					broadcaster->sendActionMessage(String("triggerright"));
                }

				shortcutExists = true;
			}
			else if (shortcut.knobModifier && shortcut.keyPress == KeyPress::upKey)
			{
				if (broadcaster)
                {
                    DBG("Trying to activate the assignment above current...");
					broadcaster->sendActionMessage(String("triggerup"));
                }

				shortcutExists = true;
			}
			else if (shortcut.knobModifier && shortcut.keyPress == KeyPress::downKey)
			{
				if (broadcaster)
                {
                    DBG("Trying to activate the assignment below current...");
					broadcaster->sendActionMessage(String("triggerdown"));
                }

				shortcutExists = true;
			}

			if (!shortcutExists)
			{
				for (int i = 0; i < quickAssignmentsPanel->shortcutEditors.size(); i++)
				{
					bool equal = false;
					if (shortcut.knobModifier)
						equal = shortcut.compareIgnoringModifier(quickAssignmentsPanel->shortcutEditors[i]->getAssignButton()->shortcut);
					else
						equal = (quickAssignmentsPanel->shortcutEditors[i]->getAssignButton()->shortcut == shortcut);

					if (equal)
					{
						if (broadcaster)
                        {
                            DBG("Trying to assign to quick assignment #" + String(i));
							broadcaster->sendActionMessage(String("assign ") + String(i));
                        }
						shortcutExists = true;
						break;
					}
					else
					{
						if (shortcut.knobModifier)
							equal = shortcut.compareIgnoringModifier(quickAssignmentsPanel->shortcutEditors[i]->getTriggerButton()->shortcut);
						else
							equal = (quickAssignmentsPanel->shortcutEditors[i]->getTriggerButton()->shortcut == shortcut);

						if (equal)
						{
							if (broadcaster)
                            {
                                DBG("Trying to activate quick assignment #" + String(i));
								broadcaster->sendActionMessage(String("trigger ") + String(i));
                            }
							shortcutExists = true;
							break;
						}
					}
				}
			}
		}
        
		return shortcutExists;
	}
	
#if JUCE_WIN32
	static int keyMods;
	static HHOOK lowlevelhook;

	void setUpHook()
	{
		if (!hookSet)
		{
			lowlevelhook = SetWindowsHookEx(WH_KEYBOARD_LL, LowLevelKeyboardProc, NULL, 0);
			hookSet = true;
		}
	}

	static bool isKeyDown(const int key) noexcept { return (GetAsyncKeyState(key) & 0x8000) != 0; }
	static void updateKeyModifiers() noexcept
	{
		keyMods = 0;
		if (isKeyDown(VK_SHIFT))   keyMods |= ModifierKeys::shiftModifier;
		if (isKeyDown(VK_CONTROL)) keyMods |= ModifierKeys::ctrlModifier;
		if (isKeyDown(VK_MENU))    keyMods |= ModifierKeys::altModifier;

		// workaround: Windows maps AltGr to left-Ctrl + right-Alt.
		if (isKeyDown(VK_RMENU) && !isKeyDown(VK_RCONTROL))
		{
			keyMods = (keyMods & ~ModifierKeys::ctrlModifier) | ModifierKeys::altModifier;
		}
	}

	static int convertVKcode(DWORD vkCode)
	{
		int extendedKeyModifier = 0x10000;

		switch (vkCode)
		{
		case VK_DELETE:
			return VK_DELETE | extendedKeyModifier;
		case VK_INSERT:
			return VK_INSERT | extendedKeyModifier;
		case VK_TAB:
			return VK_TAB | extendedKeyModifier;
		case VK_LEFT:
			return VK_LEFT | extendedKeyModifier;
		case VK_RIGHT:
			return VK_RIGHT | extendedKeyModifier;
		case VK_UP:
			return VK_UP | extendedKeyModifier;
		case VK_DOWN:
			return VK_DOWN | extendedKeyModifier;
		case VK_HOME:
			return VK_HOME | extendedKeyModifier;
		case VK_END:
			return VK_END | extendedKeyModifier;
		case VK_PRIOR:
			return VK_PRIOR | extendedKeyModifier;
		case VK_NEXT:
			return VK_NEXT | extendedKeyModifier;
		case VK_F1:
			return VK_F1 | extendedKeyModifier;
		case VK_F2:
			return VK_F2 | extendedKeyModifier;
		case VK_F3:
			return VK_F3 | extendedKeyModifier;
		case VK_F4:
			return VK_F4 | extendedKeyModifier;
		case VK_F5:
			return VK_F5 | extendedKeyModifier;
		case VK_F6:
			return VK_F6 | extendedKeyModifier;
		case VK_F7:
			return VK_F7 | extendedKeyModifier;
		case VK_F8:
			return VK_F8 | extendedKeyModifier;
		case VK_F9:
			return VK_F9 | extendedKeyModifier;
		case VK_F10:
			return VK_F10 | extendedKeyModifier;
		case VK_F11:
			return VK_F11 | extendedKeyModifier;
		case VK_F12:
			return VK_F12 | extendedKeyModifier;
		case VK_F13:
			return VK_F13 | extendedKeyModifier;
		case VK_F14:
			return VK_F14 | extendedKeyModifier;
		case VK_F15:
			return VK_F15 | extendedKeyModifier;
		case VK_F16:
			return VK_F16 | extendedKeyModifier;
		case VK_NUMPAD0:
			return VK_NUMPAD0 | extendedKeyModifier;
		case VK_NUMPAD1:
			return VK_NUMPAD1 | extendedKeyModifier;
		case VK_NUMPAD2:
			return VK_NUMPAD2 | extendedKeyModifier;
		case VK_NUMPAD3:
			return VK_NUMPAD3 | extendedKeyModifier;
		case VK_NUMPAD4:
			return VK_NUMPAD4 | extendedKeyModifier;
		case VK_NUMPAD5:
			return VK_NUMPAD5 | extendedKeyModifier;
		case VK_NUMPAD6:
			return VK_NUMPAD6 | extendedKeyModifier;
		case VK_NUMPAD7:
			return VK_NUMPAD7 | extendedKeyModifier;
		case VK_NUMPAD8:
			return VK_NUMPAD8 | extendedKeyModifier;
		case VK_NUMPAD9:
			return VK_NUMPAD9 | extendedKeyModifier;
		case VK_ADD:
			return VK_ADD | extendedKeyModifier;
		case VK_SUBTRACT:
			return VK_SUBTRACT | extendedKeyModifier;
		case VK_MULTIPLY:
			return VK_MULTIPLY | extendedKeyModifier;
		case VK_DIVIDE:
			return VK_DIVIDE | extendedKeyModifier;
		case VK_SEPARATOR:
			return VK_SEPARATOR | extendedKeyModifier;
		case VK_DECIMAL:
			return VK_DECIMAL | extendedKeyModifier;
		case 0x92:
			return 0x92 | extendedKeyModifier;
		default:
			return vkCode;
		}
	}

	static LRESULT CALLBACK LowLevelKeyboardProc(_In_ int nCode, _In_ WPARAM wParam, _In_ LPARAM lParam)
	{
		HHOOK hook;

		if (serialInterface)
		{
			if (!serialInterface->isConnected())
				return CallNextHookEx(hook, nCode, wParam, lParam);
		}

		updateKeyModifiers();
		ModifierKeys currentModifiers = ModifierKeys(keyMods);

 		if ((wParam == WM_KEYDOWN || wParam == WM_SYSKEYDOWN) && isHookActive)
		{
  			KBDLLHOOKSTRUCT* hookStruct = (KBDLLHOOKSTRUCT*)lParam;

			KeypressExtended shortcut;
			shortcut.keyPress = KeyPress(convertVKcode(hookStruct->vkCode), currentModifiers, 0);

			if (controllerManager != nullptr)
				shortcut.knobModifier = controllerManager->isKnobBeingTouched();

			bool shortcutExists = executeShortcut(shortcut);
			
			if (shortcutExists)
			{
				//Send the right action and consume the key by returning a non-zero value
				return 1;
			}
		}


		if (nCode < 0)
		{
			return CallNextHookEx(hook, nCode, wParam, lParam);
		}

		return CallNextHookEx(hook, nCode, wParam, lParam);
	}
#endif
    
#if JUCE_MAC
    static CFMachPortRef eventTap;
    static CGEventMask eventMask;
    static CFRunLoopRef runLoop;
    static CFRunLoopSourceRef runLoopSource;
    
    static void stopRunLoop();
    static int getKeyCodeFromEvent (void* nsevent);
    static CGEventRef myCGEventCallback(CGEventTapProxy proxy, CGEventType type, CGEventRef event, void *refcon);
#endif

	juce_DeclareSingleton_SingleThreaded_Minimal(LowLevelKeyboardListener)
};



#endif 
