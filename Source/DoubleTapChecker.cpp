/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "DoubleTapChecker.h"

DoubleTapChecker::DoubleTapChecker()
{
	millisThreshold = 300;
	eventCounter = 0;
	eventStart = 0;
}

DoubleTapChecker::~DoubleTapChecker()
{
}

void DoubleTapChecker::justTouched()
{
	if (eventCounter == 0) //fresh start
	{
		eventStart = juce::Time::currentTimeMillis();
	}
	else if (eventCounter == 2) //first tap has already been done
	{
		juce::int64 currentTime = juce::Time::currentTimeMillis();
		if (currentTime - eventStart > millisThreshold) //Tap was too slow
		{
			eventCounter = 1; //Set the counter like its starting a new tap.
			eventStart = currentTime;
			return;
		}
	}

	eventCounter++;
}

bool DoubleTapChecker::justUntouched()
{
	if (eventCounter == 3) //Double tap completed
	{
		juce::int64 currentTime = juce::Time::currentTimeMillis();
		if (currentTime - eventStart > millisThreshold) //Taps were too slow
		{
			eventCounter = 0; //Reset the counter
			return false;
		}
		else //This was a successful double tap!
		{
			eventCounter = 0; //Reset the counter
			return true;
		}
	}

	if (eventCounter!=0)
		eventCounter++;

	return false;
}

void DoubleTapChecker::reset()
{
	eventCounter = 0;
}
