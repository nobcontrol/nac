/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 4.3.1

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library - "Jules' Utility Class Extensions"
  Copyright (c) 2015 - ROLI Ltd.

  ==============================================================================
*/

//[Headers] You can add your own extra header files here...
/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/
//[/Headers]

#include "QuickAssignmentsPanel.h"


//[MiscUserDefs] You can add your own user definitions and misc code here...
#include "ControllerManager.h"
//[/MiscUserDefs]

//==============================================================================
QuickAssignmentsPanel::QuickAssignmentsPanel ()
{
    //[Constructor_pre] You can add your own custom stuff here..
	controllerManager = nullptr;
    //[/Constructor_pre]

    addAndMakeVisible (defaultQuickShortcutEditor = new QuickAssignmentShortcutEditor (this));

    //[UserPreSize]
    //[/UserPreSize]

    setSize (600, 100);


    //[Constructor] You can add your own custom stuff here..
    defaultQuickShortcutEditor->setAsDefault();
    //[/Constructor]
}

QuickAssignmentsPanel::~QuickAssignmentsPanel()
{
    //[Destructor_pre]. You can add your own custom destruction code here..
    //[/Destructor_pre]

    defaultQuickShortcutEditor = nullptr;


    //[Destructor]. You can add your own custom destruction code here..
    //[/Destructor]
}

//==============================================================================
void QuickAssignmentsPanel::paint (Graphics& g)
{
    //[UserPrePaint] Add your own custom painting code here..
    //[/UserPrePaint]

    g.fillAll (Colour (0xffc4c4c4));

    //[UserPaint] Add your own custom painting code here..
    //[/UserPaint]
}

void QuickAssignmentsPanel::resized()
{
    //[UserPreResize] Add your own custom resize code here..
    //[/UserPreResize]

    defaultQuickShortcutEditor->setBounds (0, 0, 272, 25);
    //[UserResized] Add your own custom resize handling here..

	for (int i = 0; i < shortcutEditors.size(); i++)
	{
		shortcutEditors[i]->setBounds(0, 25+i*25, 272, 25);
	}

    //[/UserResized]
}



//[MiscUserCode] You can add your own definitions of your custom methods or any other code here...
void QuickAssignmentsPanel::addShortcutPair(QuickAssignmentShortcutEditor* editor)
{
	if (editor == defaultQuickShortcutEditor)
	{
		QuickAssignmentShortcutEditor* newEditor = new QuickAssignmentShortcutEditor(this);
		newEditor->copyFromOther(*defaultQuickShortcutEditor);
		defaultQuickShortcutEditor->resetShortcuts();

		shortcutEditors.insert(0, newEditor);
		addAndMakeVisible(newEditor);

		resize();
	}
}

void QuickAssignmentsPanel::removeShortcutPair(QuickAssignmentShortcutEditor* editor)
{
	if (editor != defaultQuickShortcutEditor)
	{
		for (int i = 0; i < shortcutEditors.size(); i++)
		{
			if (shortcutEditors[i] == editor)
			{
				shortcutEditors.remove(i);
				resize();
				return;
			}
		}
	}
}

void QuickAssignmentsPanel::shortcutSet(QuickAssignmentShortcutEditor* editor, SetShortcutButton* button)
{
	int indexToRemove = -1;

	for (int i = 0; i < shortcutEditors.size(); i++)
	{
		if (shortcutEditors[i] != editor)
		{
			SetShortcutButton* assignButton = shortcutEditors[i]->getAssignButton();
			SetShortcutButton* triggerButton = shortcutEditors[i]->getTriggerButton();

			if (assignButton->shortcut == button->shortcut)
				assignButton->resetShortcut();

			if (triggerButton->shortcut == button->shortcut)
				triggerButton->resetShortcut();

			if (!assignButton->shortcut.keyPress.isValid() && !triggerButton->shortcut.keyPress.isValid())
				indexToRemove = i;
		}
	}

	//clean up
	if (indexToRemove != -1)
	{
		removeShortcutPair(shortcutEditors[indexToRemove]);
		return;
	}

	if (editor != defaultQuickShortcutEditor)
	{
		SetShortcutButton* assignButton = defaultQuickShortcutEditor->getAssignButton();
		SetShortcutButton* triggerButton = defaultQuickShortcutEditor->getTriggerButton();

		if (assignButton->shortcut == button->shortcut)
			assignButton->resetShortcut();

		if (triggerButton->shortcut == button->shortcut)
			triggerButton->resetShortcut();
	}
}

void QuickAssignmentsPanel::resize()
{
	int editorHeight = defaultQuickShortcutEditor->getHeight();
	setSize(getWidth(), (1 + shortcutEditors.size())*editorHeight);
}

ControllerManager* QuickAssignmentsPanel::getControllerManager()
{
	return controllerManager;
}

void QuickAssignmentsPanel::setControllerManager(ControllerManager* theControllerManager)
{
	controllerManager = theControllerManager;
}

Assignment* QuickAssignmentsPanel::getAssignment(int index)
{
    if (index>=0 && index<shortcutEditors.size())
        return shortcutEditors[index]->assignment;
    else
        return NULL;
}

int QuickAssignmentsPanel::getNumberOfQuickAssignments()
{
	return shortcutEditors.size();
}

KeypressExtended QuickAssignmentsPanel::getAssignShortcut(int index)
{
	return shortcutEditors[index]->getAssignButton()->shortcut;
}

void QuickAssignmentsPanel::setAssignShortcut(int index, KeypressExtended shortcut)
{
	shortcutEditors[index]->getAssignButton()->setNewKey(shortcut);
}

KeypressExtended QuickAssignmentsPanel::getTriggerShortcut(int index)
{
	return shortcutEditors[index]->getTriggerButton()->shortcut;
}

void QuickAssignmentsPanel::setTriggerShortcut(int index, KeypressExtended shortcut)
{
	shortcutEditors[index]->getTriggerButton()->setNewKey(shortcut);
}

void QuickAssignmentsPanel::addEmptyShortcutEditor()
{
	QuickAssignmentShortcutEditor* editor = new QuickAssignmentShortcutEditor(this);
	shortcutEditors.add(editor);
	addAndMakeVisible(editor);
	resize();
}

void QuickAssignmentsPanel::updateTooltipsOfShortcutEditor(int index)
{
	shortcutEditors[index]->updateTooltips();
}


//[/MiscUserCode]


//==============================================================================
#if 0
/*  -- Projucer information section --

    This is where the Projucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="QuickAssignmentsPanel" componentName=""
                 parentClasses="public Component" constructorParams="" variableInitialisers=""
                 snapPixels="8" snapActive="1" snapShown="1" overlayOpacity="0.330"
                 fixedSize="0" initialWidth="600" initialHeight="100">
  <BACKGROUND backgroundColour="ffc4c4c4"/>
  <JUCERCOMP name="" id="9b5b39ec809b7198" memberName="defaultQuickShortcutEditor"
             virtualName="" explicitFocusOrder="0" pos="0 0 272 25" sourceFile="QuickAssignmentShortcutEditor.cpp"
             constructorParams="this"/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif


//[EndFile] You can add extra defines here...
//[/EndFile]
