/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef Assignment_H_INCLUDED
#define Assignment_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#if JUCE_WIN32
	#include <Windows.h>
#endif

enum ControlMode
{
	CursorControl = 0,
	ScrollWheelControl,
	SystemControl
};

enum CursorControllerOperationStates
{
	dragMode = 0,
	stationaryMode
};

enum DirectionStates
{
	vertical = 0,
	horizontal,
	circular,
	diagonal
};

enum CircularDiagonalTriggerStates
{
	noCircularOrDiagonalTrigger = 0,
	circularOrDiagonalArmed,
	circularOrDiagonalActive
};

enum MouseButtons
{
	leftMouseButton = 0,
	rightMouseButton
};

enum ScrollWheelModes
{
	ClassicMode = 0,
	NaturalMode
};

enum OSDs
{
	NoOSD = 0,
	VolumeOSD,
	BrightnessOSD
};

struct AssignmentPosition
{
	void* nativeWindow;
	juce::Point<int> relativePosition;
    juce::String owner;
    juce::String title;
};

class MovementRecorder
{
public:
	MovementRecorder();
	~MovementRecorder();

	MovementRecorder& operator=(const MovementRecorder& other);

	void clear();
	void addMovement(int x, int y);
	void addMovements(MovementRecorder& movementRecorder);

	void moveCursorInReverse();
	void reverseMovement();
    
    static const int maxMovement = 63;
    int verticalMoveCounts[maxMovement];
    int horizontalMoveCounts[maxMovement];
};

class Assignment
{
public:
	Assignment();
	~Assignment();

	Assignment& operator=(const Assignment& other);

	AssignmentPosition assignmentPosition;
	void updateAssignmentPosition();
	void updateAssignmentPosition(juce::Point<float> cursorPos);
	juce::Point<int> getGlobalPosition();
	bool myWindowIsValid();
	void bringWindowToFront();

	juce::Point<int> point; //Cursor position at the time of assignment

	int controlMode;

	int currentDirectionState;
	int currentOperationMode;
	int radius;
	float phase;
	bool radiusAndPhaseJustSet;
	bool radiusAndPhaseNeverSet;

	bool radiusHadToGetSetToOneForDirectDiagonalInteraction;

	int clicksPerPixel;

	int primaryMouseButton;
	bool mouseButtonIsBeingPressed;

	int scrollWheelMode;

    juce::Point<int> pointAtDragStart;

	MovementRecorder movements[3];
	int currentMovementIndex;
	int numberOfMovementsToUndo;

	void mouseClick();
	void mouseRelease();
	void moveCursor(int x, int y);
	void performUndo();
	void waitForCursorToAppear();


	void incrementCurrentMovementIndex();
	void decrementCurrentMovementIndex();
	void clearMovements();
	void setRadiusAndPhase();
	void moveToCircle(bool recordMovement);
	void setClicksPerPixel(int cpp);
    void dragCursorInReverse(MovementRecorder& mr);

	void scroll(int x, int y);
	void undoScroll();
	void scrollInReverse(MovementRecorder& mr);

	int masterVolumeChange(int change);
	void undoMasterVolumeChange();
	void masterVolumeChangeInReverse(MovementRecorder& mr);
   
	void updatePosition();
	void setCurrentVolume();
	float currentVolume;

#if JUCE_WIN32
	int lastOSDShown;
	HWND volumeOSD;
	void showVolumeOSD();
	void hideVolumeOSD();
	HWND getVolumeOSD();
#endif
    
#if JUCE_MAC
    void showVolumeIndication();
    void hideVolumeIndication();
    void* getVolumeIndication();
#endif
};



#endif
