/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 4.3.1

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library - "Jules' Utility Class Extensions"
  Copyright (c) 2015 - ROLI Ltd.

  ==============================================================================
*/

#ifndef __JUCE_HEADER_64193B7260114E76__
#define __JUCE_HEADER_64193B7260114E76__

//[Headers]     -- You can add your own extra header files here --
/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "QuickAssignmentShortcutEditor.h"

#include "../JuceLibraryCode/JuceHeader.h"
using namespace juce;

class ControllerManager;
class LowLevelKeyboardListener;
//[/Headers]



//==============================================================================
/**
                                                                    //[Comments]
    An auto-generated component, created by the Projucer.

    Describe your class and how it works here!
                                                                    //[/Comments]
*/
class QuickAssignmentsPanel  : public juce::Component
{
public:
    //==============================================================================
    QuickAssignmentsPanel ();
    ~QuickAssignmentsPanel();

    //==============================================================================
    //[UserMethods]     -- You can add your own custom methods in this section.
	friend class LowLevelKeyboardListener;
	friend class ControllerManager;

	void addShortcutPair(QuickAssignmentShortcutEditor* editor);
	void removeShortcutPair(QuickAssignmentShortcutEditor* editor);
	void resize();

	void shortcutSet(QuickAssignmentShortcutEditor* editor, SetShortcutButton* button);

	ControllerManager* getControllerManager();
	void setControllerManager(ControllerManager* theControllerManager);

	int getNumberOfQuickAssignments();

	Assignment* getAssignment(int index);

	KeypressExtended getAssignShortcut(int index);
	void setAssignShortcut(int index, KeypressExtended shortcut);

	KeypressExtended getTriggerShortcut(int index);
	void setTriggerShortcut(int index, KeypressExtended shortcut);

	void addEmptyShortcutEditor();

	void updateTooltipsOfShortcutEditor(int index);
    //[/UserMethods]

    void paint (Graphics& g) override;
    void resized() override;



private:
    //[UserVariables]   -- You can add your own custom variables in this section.
	OwnedArray<QuickAssignmentShortcutEditor> shortcutEditors;
	ControllerManager* controllerManager;
    //[/UserVariables]

    //==============================================================================
    ScopedPointer<QuickAssignmentShortcutEditor> defaultQuickShortcutEditor;


    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (QuickAssignmentsPanel)
};

//[EndFile] You can add extra defines here...
//[/EndFile]

#endif   // __JUCE_HEADER_64193B7260114E76__
