/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SerialInterface.h"

SerialInterface::SerialInterface() : Thread("SerialInterfaceThread")
{
	lastDiscoveryTime = 0;

	knobIsBeingTouched = false;
	leftToggleIsBeingTouched = false;
	rightToggleIsBeingTouched = false;
	leftToggleIsUp = false;
	rightToggleIsUp = false;

	HIDFunctionalityEnabled = true; //By default

	trayIcon = NULL;
	infoBubbleIsBeingShown = false;
	timeInfoBubbleWasShown = 0;

	interfaceReadable = false;
}


SerialInterface::~SerialInterface()
{

}


void SerialInterface::run()
{
	while (1)
	{
		//Try to discover a connected nOb...
		serial::PortInfo discoveredDevicePort;
		discoveredDevicePort.port = "NOTFOUND";
		discoveredDevicePort.hardware_id = "";
		discoveredDevicePort.description = "";

		//...if 1 seconds from last discovery try has passed
		juce::int64 currentTime = juce::Time::currentTimeMillis();
		juce::int64 timeDifference = currentTime - lastDiscoveryTime;
		if (timeDifference > (juce::int64)1000)
		{
            DBG("Trying to discover a connected nOb...");
			discoveredDevicePort = discoverDevice();
			lastDiscoveryTime = currentTime;
		}
		
		size_t stringPosition = discoveredDevicePort.port.find(std::string("NOTFOUND"));
		bool deviceSuccessfullyDiscovered = false;
		if (stringPosition == string::npos)
			deviceSuccessfullyDiscovered = true;

		if (deviceSuccessfullyDiscovered)
		{
            DBG("nOb discovered.");
            
			//Open the port
            DBG("Opening serial connection...");
			serial::Serial mySerial(discoveredDevicePort.port, 9600, serial::Timeout::simpleTimeout(2000));
			mySerial.setDTR(true);
			interfaceReadable = true;
			HIDFunctionalityEnabled = true;

			while (mySerial.isOpen() && interfaceReadable)
			{                
				//First disable nOb's HID functionality, if needed
				if (HIDFunctionalityEnabled)
				{
                    DBG("Disabling nOb's default HID functionality...");
                    
					unsigned char disableCommand = (unsigned char)0x31;
					size_t result = mySerial.write(&disableCommand, 1);

					if (result == 1)
					{
						HIDFunctionalityEnabled = false;
						if (trayIcon && !infoBubbleIsBeingShown)
						{
							trayIcon->showInfoBubble("nOb connected:", juce::String(discoveredDevicePort.port));
							trayIcon->setIconTooltip("nOb connected: " + juce::String(discoveredDevicePort.port));
							infoBubbleIsBeingShown = true;
							timeInfoBubbleWasShown = juce::Time::currentTimeMillis();
						}	
					}
						
				}

				string buffer;
				size_t size = mySerial.read(buffer, 2U); //get 2 bytes from the interface

				if (size == 0)
					interfaceReadable = false;

				if (size == 2)
				{
					interfaceReadable = true;

					//Find out the order of the bytes
					unsigned char firstByte = buffer.c_str()[0];
					unsigned char firstMaskedByte = firstByte & (unsigned char)0x80;
					bool firstByteIsState = true;
					if (firstMaskedByte != (unsigned char)0x00)
						firstByteIsState = false;

					unsigned char stateByte;
					unsigned char encoderByte;

					if (firstByteIsState)
					{
						stateByte = buffer.c_str()[0];
						encoderByte = buffer.c_str()[1];
					}
					else
					{
						stateByte = buffer.c_str()[1];
						encoderByte = buffer.c_str()[0];
					}

					//Analyse encoder byte
					bool clockwiseRotation = true;
					unsigned char rotationClicks = 0;

					rotationClicks = encoderByte & (unsigned char)0x3F;
					
					if ((encoderByte & (unsigned char)0x40) == (unsigned char)0x40)
						clockwiseRotation = false;

					if (rotationClicks != (unsigned char)0)
					{
						//rotation event
						sendActionMessage(juce::String("rotation ") + (clockwiseRotation ? juce::String() : juce::String("-")) + juce::String((unsigned int)rotationClicks));
					}	

					//Analyse state byte
					bool knobTouch = false;
					bool leftToggleTouch = false;
					bool rightToggleTouch = false;
					bool leftToggleUp = false;
					bool rightToggleUp = false;
					bool mostSignificantCheckBit = false;
					bool lessSignificantCheckBit = false;

					if ((stateByte & (unsigned char)0x40) == (unsigned char)0x40)
						knobTouch = true;

					if ((stateByte & (unsigned char)0x20) == (unsigned char)0x20)
						leftToggleTouch = true;

					if ((stateByte & (unsigned char)0x10) == (unsigned char)0x10)
						rightToggleTouch = true;

					if ((stateByte & (unsigned char)0x08) == (unsigned char)0x08)
						leftToggleUp = true;

					if ((stateByte & (unsigned char)0x04) == (unsigned char)0x04)
						rightToggleUp = true;

					if ((stateByte & (unsigned char)0x02) == (unsigned char)0x02)
						mostSignificantCheckBit = true;

					if ((stateByte & (unsigned char)0x01) == (unsigned char)0x01)
						lessSignificantCheckBit = true;

					if (knobTouch != knobIsBeingTouched)
					{
						if (knobTouch) // the knob has been touched, send action!
						{
							knobIsBeingTouched = true;
							sendActionMessage(juce::String("knobtouch ") + juce::String((unsigned int)(knobIsBeingTouched ? 1 : 0)));
						}
						else if (rotationClicks == 0) //the knob is not being touched anymore and the knob stopped turning. Now is the knob considered untouched.
						{
							knobIsBeingTouched = false;
							sendActionMessage(juce::String("knobtouch ") + juce::String((unsigned int)(knobIsBeingTouched ? 1 : 0)));
						}

						//knob touch event
						//knobIsBeingTouched = knobTouch;
						//sendActionMessage(String("knobtouch ") + String((unsigned int)(knobIsBeingTouched ? 1 : 0)));
					}

					if (leftToggleTouch != leftToggleIsBeingTouched)
					{
						//left toggle touch event
						leftToggleIsBeingTouched = leftToggleTouch;
						sendActionMessage(juce::String("lefttoggletouch ") + juce::String((unsigned int)(leftToggleIsBeingTouched ? 1 : 0)));
					}

					if (rightToggleTouch != rightToggleIsBeingTouched)
					{
						//right toggle touch event
						rightToggleIsBeingTouched = rightToggleTouch;
						sendActionMessage(juce::String("righttoggletouch ") + juce::String((unsigned int)(rightToggleIsBeingTouched ? 1 : 0)));
					}

					if (leftToggleUp != leftToggleIsUp)
					{
						//left toggle change event
						leftToggleIsUp = leftToggleUp;
						sendActionMessage(juce::String("lefttoggleup ") + juce::String((unsigned int)(leftToggleIsUp ? 1 : 0)));
					}

					if (rightToggleUp != rightToggleIsUp)
					{
						//right toggle change event
						rightToggleIsUp = rightToggleUp;
						sendActionMessage(juce::String("righttoggleup ") + juce::String((unsigned int)(rightToggleIsUp ? 1 : 0)));
					}
				}

				//hide info bubble after two seconds
				if (infoBubbleIsBeingShown)
				{
					juce::int64 curTime = juce::Time::currentTimeMillis();
					if (curTime - timeInfoBubbleWasShown > 2000)
					{
						trayIcon->hideInfoBubble();
						infoBubbleIsBeingShown = false;
					}
				}

				if (threadShouldExit()) //Threads need to exit, so finish up what you are doing.
				{
                    DBG("Serial connection thread should exit.");
					//Enable nOb's HID functionality, before closing the interface
					if (!HIDFunctionalityEnabled)
					{
                        DBG("Enabling nOb's default HID functionality...");

						unsigned char enableCommand = (unsigned char)0x30;
						size_t result = mySerial.write(&enableCommand, 1);

						if (result == 1)
							HIDFunctionalityEnabled = true;

						//Sleep before closing the serial, to allow some time for nOb to consume the command
						Thread::sleep(50);
					}

					mySerial.close();
				}
			}

		}
		else //Sleep for a little while and try again in next iteration
		{
			trayIcon->setIconTooltip("no nOb connected");
			Thread::sleep(100);
		}

		if (threadShouldExit()) //just get out of here
		{
            DBG("Exiting serial interface thread...");
			return;
		}
	}	
}

void SerialInterface::setTrayIcon(juce::SystemTrayIconComponent * aTrayIcon)
{
	trayIcon = aTrayIcon;
}

void SerialInterface::setUpController()
{
	sendActionMessage(juce::String("lefttoggleup ") + juce::String((unsigned int)(leftToggleIsUp ? 1 : 0)));
	sendActionMessage(juce::String("righttoggleup ") + juce::String((unsigned int)(rightToggleIsUp ? 1 : 0)));
	sendActionMessage(juce::String("lefttoggletouch ") + juce::String((unsigned int)(leftToggleIsBeingTouched ? 1 : 0)));
	sendActionMessage(juce::String("righttoggletouch ") + juce::String((unsigned int)(rightToggleIsBeingTouched ? 1 : 0)));
	sendActionMessage(juce::String("knobtouch ") + juce::String((unsigned int)(knobIsBeingTouched ? 1 : 0)));
}

bool SerialInterface::updateFirmware()
{
	//stop the serial interface
	bool stopped = stopThread(500);
	if (stopped)
		juce::Logger::outputDebugString("Serial connection closed.");
	else
		juce::Logger::outputDebugString("Serial connection killed.");

	//Discover nOb
	serial::PortInfo discoveredDevicePort;
	discoveredDevicePort.port = "NOTFOUND";
	discoveredDevicePort.hardware_id = "";
	discoveredDevicePort.description = "";

	discoveredDevicePort = discoverDevice();

	size_t stringPosition = discoveredDevicePort.port.find(std::string("NOTFOUND"));
	bool deviceSuccessfullyDiscovered = false;
	if (stringPosition == string::npos)
		deviceSuccessfullyDiscovered = true;

	if (deviceSuccessfullyDiscovered)
		juce::Logger::outputDebugString("nOb found.");
	else
		juce::Logger::outputDebugString("nOb not found.");

	//Reset device by opening and closing the interface at 1200bps, and run the avrdude command
	if (deviceSuccessfullyDiscovered)
	{
		juce::Logger::outputDebugString("Trying to open serial connection at 1200 baud.");
		serial::Serial mySerial(discoveredDevicePort.port, 1200, serial::Timeout::simpleTimeout(2000));
		if (mySerial.isOpen())
		{
			juce::Logger::outputDebugString("Connection at 1200 opened.");

			mySerial.close();
			juce::Logger::outputDebugString("Connection closed in order to enter bootloader mode.");

			juce::Thread::sleep(1500);

			//Discover bootloader
			serial::PortInfo discoveredBootloaderPort;
			discoveredBootloaderPort.port = "NOTFOUND";
			discoveredBootloaderPort.hardware_id = "";
			discoveredBootloaderPort.description = "";

			juce::Logger::outputDebugString("Trying to discover board in bootloader mode...");

			int attemptNumber = 1;
			bool bootloaderSuccessfullyDiscovered = false;

			while (attemptNumber <= 10 && bootloaderSuccessfullyDiscovered == false)
			{
				discoveredBootloaderPort.port = "NOTFOUND";
				discoveredBootloaderPort.hardware_id = "";
				discoveredBootloaderPort.description = "";

				juce::Logger::outputDebugString(juce::String("Discovery attempt: ") + juce::String(attemptNumber));

				discoveredBootloaderPort = discoverBootloader();

				size_t stringPosition = discoveredBootloaderPort.port.find(std::string("NOTFOUND"));
				if (stringPosition == string::npos)
					bootloaderSuccessfullyDiscovered = true;

				if (!bootloaderSuccessfullyDiscovered)
				{
					juce::Logger::outputDebugString("Device in bootloader mode not found.");
					juce::Logger::outputDebugString("Sleeping for a second and retrying...");
					juce::Thread::sleep(1000);
				}

				attemptNumber++;
			}

			if (!bootloaderSuccessfullyDiscovered)
				juce::Logger::outputDebugString("All discovery attempts failed. Device in bootloader mode not found.");

			if (bootloaderSuccessfullyDiscovered)
			{
				juce::Logger::outputDebugString("Executing update...");

				/*
				Load firmware with avrdude
				Example: avrdude -CC:\Program Files (x86)\Arduino\hardware\tools\avr/etc/avrdude.conf -v -patmega32u4 -cavr109 -PCOM5 -b57600 -D -Uflash:w:C:\Users\few\AppData\Local\Temp\buildf9a6e221e340224318f91654d5a6d701.tmp/sketch_jan23a.ino.hex:i
				
				avrdude.exe -C avrdude.conf -v -p atmega32u4 -c avr109 -P COM8 -b 57600 -D -U flash:w:nObFirmwareV01.hex:i
				
				*/
#ifdef JUCE_WIN32

				juce::String workingDirectory = juce::File::getCurrentWorkingDirectory().getFullPathName() + juce::String("\\Data\\Firmware");
				juce::String executablePath = juce::File::getCurrentWorkingDirectory().getChildFile("Data/Firmware/avrdude.exe").getFullPathName();

				juce::String configPath = juce::File::getCurrentWorkingDirectory().getChildFile("Data/Firmware/avrdude.conf").getFullPathName();
				juce::String hexPath = juce::File::getCurrentWorkingDirectory().getChildFile("Data/Firmware/nObFirmwareV01.hex").getFullPathName();

				juce::String parameters = juce::String(" -C ") + configPath.quoted() + juce::String(" -v -p atmega32u4 -cavr109 -P ") + juce::String(discoveredBootloaderPort.port)
					+ juce::String(" -b 57600 -D -U flash:w:") + hexPath.quoted() + juce::String(":i");

				juce::String commandLine = juce::String("avrdude.exe") + parameters;

				STARTUPINFO si;
				PROCESS_INFORMATION pi;

				ZeroMemory(&si, sizeof(si));
				si.cb = sizeof(si);
				ZeroMemory(&pi, sizeof(pi));

				DWORD error = 0;

				//ShellExecute(NULL, "open", executablePath.toUTF8(), parameters.toUTF8(), workingDirectory.toUTF8(), SHOW_OPENWINDOW);

				
				if (!CreateProcess(executablePath.toUTF8(),   // No module name (use command line)
					(LPSTR)commandLine.toRawUTF8(),        // Command line
					NULL,           // Process handle not inheritable
					NULL,           // Thread handle not inheritable
					FALSE,          // Set handle inheritance to FALSE
					0,              // No creation flags
					NULL,           // Use parent's environment block
					workingDirectory.toUTF8(),           // Use parent's starting directory 
					&si,            // Pointer to STARTUPINFO structure
					&pi)           // Pointer to PROCESS_INFORMATION structure
					)
				{
                    return false;
				}
				

				// Wait until child process exits.
				WaitForSingleObject(pi.hProcess, INFINITE);

				// Close process and thread handles. 
				CloseHandle(pi.hProcess);
				CloseHandle(pi.hThread);

				return true;

#endif // JUCE_WIN32
                
#ifdef JUCE_MAC
                /* Example
                 avrdude -C/Applications/Arduino.app/Contents/Java/hardware/tools/avr/etc/avrdude.conf -v -patmega32u4 -cavr109 -P/dev/cu.usbmodem1421 -b57600 -D -Uflash:w:/var/folders/gw/j_5s40x52rb_r4tvxsrtmgbw0000gp/T/arduino_build_117754/sketch_jan24a.ino.hex:i
                */
                
                
                juce::String executablePath = juce::File::getSpecialLocation(juce::File::currentApplicationFile).getChildFile("Data/Firmware/avrdude").getFullPathName();
                
                juce::String configPath = juce::File::getSpecialLocation(juce::File::currentApplicationFile).getChildFile("Data/Firmware/avrdude.conf").getFullPathName();
                
                juce::String hexPath = juce::File::getSpecialLocation(juce::File::currentApplicationFile).getChildFile("Data/Firmware/nObFirmwareV01.hex").getFullPathName();
                
                juce::String parameters = juce::String(" -C ") + configPath.quoted() + juce::String(" -v -p atmega32u4 -cavr109 -P ") + juce::String(discoveredBootloaderPort.port) + juce::String(" -b 57600 -D -U flash:w:") + hexPath.quoted() + juce::String(":i");
                
                juce::String commandLine = executablePath.quoted() + parameters;

                int result = system(commandLine.toUTF8());
                if (result == 0)
                    return true;
#endif // JUCE_MAC
			}
		}
	}

    return false;
}

bool SerialInterface::getKnobIsBeingTouched()
{
	return knobIsBeingTouched;
}

bool SerialInterface::getLeftToggleIsBeingTouched()
{
	return leftToggleIsBeingTouched;
}
	

bool SerialInterface::getRightToggleIsBeingTouched()
{
	return rightToggleIsBeingTouched;
}

bool SerialInterface::getLeftToggleIsUp()
{
	return leftToggleIsUp;
}

bool SerialInterface::getRightToggleIsUp()
{
	return rightToggleIsUp;
}

bool SerialInterface::isConnected()
{
	return interfaceReadable;
}

serial::PortInfo SerialInterface::discoverDevice()
{
	serial::PortInfo discoveredDevicePort;
	discoveredDevicePort.port = "NOTFOUND";
	discoveredDevicePort.hardware_id = "";
	discoveredDevicePort.description = "";

	vector<serial::PortInfo> devices_found = serial::list_ports();

	if (devices_found.size())
	{
		DBG(juce::String("Number of connected serial devices:") + juce::String(devices_found.size()));
	}
	else
	{
		DBG("No connected serial devices found.");
	}

	bool discovered = false;
	size_t portIndex = 0;
	while (!discovered && portIndex < devices_found.size())
	{
		bool portDescriptionRight = false;

		DBG(juce::String("Checking serial device:")
			+ juce::String(portIndex) + juce::String(" on port: ")
			+ juce::String(devices_found[portIndex].port)
			+ juce::String(" with description: ") + juce::String(devices_found[portIndex].description)
			+ juce::String(" and HW-ID: ") + devices_found[portIndex].hardware_id);

		//Checking hardware IDs
		if (devices_found[portIndex].hardware_id.find("2341") != string::npos && devices_found[portIndex].hardware_id.find("8036") != string::npos)
			portDescriptionRight = true;
		else if (devices_found[portIndex].hardware_id.find("2A03") != string::npos && devices_found[portIndex].hardware_id.find("0036") != string::npos)
			portDescriptionRight = true;
        else if (devices_found[portIndex].hardware_id.find("2a03") != string::npos && devices_found[portIndex].hardware_id.find("0036") != string::npos)
            portDescriptionRight = true;
		else if (devices_found[portIndex].hardware_id.find("1B4F") != string::npos && devices_found[portIndex].hardware_id.find("9206") != string::npos)
			portDescriptionRight = true;
        else if (devices_found[portIndex].hardware_id.find("1b4f") != string::npos && devices_found[portIndex].hardware_id.find("9206") != string::npos)
            portDescriptionRight = true;

		/*
		size_t found = devices_found[portIndex].description.find(string("Leonardo"));

		if (found == string::npos)
		{
			found = devices_found[portIndex].description.find(string("Arduino Micro"));
		}

		if (found == string::npos)
		{
			found = devices_found[portIndex].description.find(string("USB Serial Device"));
		}
		
		if (found != string::npos)
			portDescriptionRight = true;

		*/

		if (portDescriptionRight)
		{
			DBG("Description was valid. Connecting..");

			serial::Serial mySerial(devices_found[portIndex].port, 9600, serial::Timeout::simpleTimeout(500));
			mySerial.setDTR(true);
	
			if (mySerial.isOpen())
			{
				DBG("Connected. Reading data...");

				string buffer;
				bool thisIsAnOb = true;

				for (int i = 0; i < 5; i++)
				{
					size_t size = mySerial.read(buffer, 2U); //get 2 bytes from the interface
					if (size != 2)
					{
						thisIsAnOb = false;
						DBG("Unable to read data from serial device.");
					}
					else
					{
						//Check if first byte starts with 0, in order to ensure the right synchronisation
						unsigned char firstByte = buffer.c_str()[0];
						unsigned char firstMaskedByte = firstByte & (unsigned char)0x80;
						bool firstByteIsState = true;
						if (firstMaskedByte != (unsigned char)0x00)
							firstByteIsState = false;

						//Check if bytes correspond to nOb's behaviour
						unsigned char stateByte;
						unsigned char encoderByte;

						if (firstByteIsState)
						{
							stateByte = buffer.c_str()[0];
							encoderByte = buffer.c_str()[1];
						}
						else
						{
							stateByte = buffer.c_str()[1];
							encoderByte = buffer.c_str()[0];
						}

						unsigned char maskedStateBit = stateByte & (unsigned char)0x80;
						unsigned char maskedEncoderBit = encoderByte & (unsigned char)0x80;

						if (maskedStateBit != (unsigned char)0x00)
						{ 
							DBG("Unable to read state byte from serial device.");
							thisIsAnOb = false;
						}

						if (maskedEncoderBit != (unsigned char)0x80)
						{
							DBG("Unable to read encoder byte from serial device.");
							thisIsAnOb = false;
						}
					}
				}

				if (thisIsAnOb)
				{
					DBG("Data matches nOb's profile.");
					discoveredDevicePort = devices_found[portIndex];
				}

				//Close the port
				mySerial.close();
			}
		}
		else
		{
			DBG("Serial description not valid.");
		}

		portIndex++;
	}

	return discoveredDevicePort;
}

serial::PortInfo SerialInterface::discoverBootloader()
{
	serial::PortInfo discoveredDevicePort;
	discoveredDevicePort.port = "NOTFOUND";
	discoveredDevicePort.hardware_id = "";
	discoveredDevicePort.description = "";

	vector<serial::PortInfo> devices_found = serial::list_ports();

	if (devices_found.size())
	{
		DBG(juce::String("Number of connected serial devices: ") + juce::String(devices_found.size()));
	}
	else
	{
		DBG("No connected serial devices found.");
	}

	bool discovered = false;
	size_t portIndex = 0;
	while (!discovered && portIndex < devices_found.size())
	{
		bool portDescriptionRight = false;

		DBG(juce::String("Checking serial device: ")
			+ juce::String(portIndex) + juce::String(" on port: ")
			+ juce::String(devices_found[portIndex].port)
			+ juce::String(" with description: ") + juce::String(devices_found[portIndex].description)
			+ juce::String(" and HW-ID: ") + devices_found[portIndex].hardware_id);

		//Checking hardware IDs
		if (devices_found[portIndex].hardware_id.find("2341") != string::npos && devices_found[portIndex].hardware_id.find("0036") != string::npos)
			portDescriptionRight = true;
		else if (devices_found[portIndex].hardware_id.find("1B4F") != string::npos && devices_found[portIndex].hardware_id.find("9205") != string::npos)
			portDescriptionRight = true;
        else if (devices_found[portIndex].hardware_id.find("1b4f") != string::npos && devices_found[portIndex].hardware_id.find("9205") != string::npos)
            portDescriptionRight = true;
		else if (devices_found[portIndex].hardware_id.find("2A03") != string::npos && devices_found[portIndex].hardware_id.find("0036") != string::npos)
			portDescriptionRight = true;
        else if (devices_found[portIndex].hardware_id.find("2a03") != string::npos && devices_found[portIndex].hardware_id.find("0036") != string::npos)
            portDescriptionRight = true;
		
        
		/*
		//Trying to find one of the keywords
		size_t found = devices_found[portIndex].description.find(string("bootloader"));

		if (found == string::npos)
			found = devices_found[portIndex].description.find(string("Leonardo"));

		if (found == string::npos)
			found = devices_found[portIndex].description.find(string("USB Serial Device"));

		if (found == string::npos)
			found = devices_found[portIndex].description.find(string("SparkFun"));

		if (found == string::npos)
			found = devices_found[portIndex].description.find(string("Pro Micro"));

		if (found != string::npos)
			portDescriptionRight = true;
		*/

		if (portDescriptionRight)
		{
			juce::Logger::outputDebugString("Device in bootloader mode found.");
			return devices_found[portIndex];
		}
        
        portIndex++;
	}

	return discoveredDevicePort;
}
