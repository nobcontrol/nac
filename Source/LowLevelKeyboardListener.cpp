/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "LowLevelKeyboardListener.h"

juce_ImplementSingleton_SingleThreaded(LowLevelKeyboardListener)

#if JUCE_WIN32
int LowLevelKeyboardListener::keyMods = 0;
HHOOK LowLevelKeyboardListener::lowlevelhook = 0;
#endif

#if JUCE_MAC
#import <AppKit/AppKit.h>

CFMachPortRef LowLevelKeyboardListener::eventTap = 0;
CGEventMask LowLevelKeyboardListener::eventMask = 0;
CFRunLoopSourceRef LowLevelKeyboardListener::runLoopSource = 0;
CFRunLoopRef LowLevelKeyboardListener::runLoop = 0;

void LowLevelKeyboardListener::stopRunLoop()
{
    if (runLoop!=0)
    {
        CFRunLoopRemoveSource (runLoop, runLoopSource, kCFRunLoopCommonModes);
        CFRunLoopSourceInvalidate (runLoopSource);
        CFRelease (runLoopSource);
        CFRunLoopStop(runLoop);
    }
}

int LowLevelKeyboardListener::getKeyCodeFromEvent (void* nsevent)
{
    NSEvent* ev = (NSEvent*)nsevent;
    
    // Unfortunately, charactersIgnoringModifiers does not ignore the shift key.
    // Using [ev keyCode] is not a solution either as this will,
    // for example, return VK_KEY_Y if the key is pressed which
    // is typically located at the Y key position on a QWERTY
    // keyboard. However, on international keyboards this might not
    // be the key labeled Y (for example, on German keyboards this key
    // has a Z label). Therefore, we need to query the current keyboard
    // layout to figure out what character the key would have produced
    // if the shift key was not pressed
    String unmodified;
    
#if JUCE_SUPPORT_CARBON
    if (TISInputSourceRef currentKeyboard = TISCopyCurrentKeyboardInputSource())
    {
        CFDataRef layoutData = (CFDataRef) TISGetInputSourceProperty (currentKeyboard,
                                                                      kTISPropertyUnicodeKeyLayoutData);
        
        if (layoutData != nullptr)
        {
            if (const UCKeyboardLayout* layoutPtr = (const UCKeyboardLayout*) CFDataGetBytePtr (layoutData))
            {
                
                UInt32 keysDown = 0;
                UniChar buffer[4];
                UniCharCount actual;
                
                if (UCKeyTranslate (layoutPtr, [ev keyCode], kUCKeyActionDown, 0, LMGetKbdType(),
                                    kUCKeyTranslateNoDeadKeysBit, &keysDown, sizeof (buffer) / sizeof (UniChar),
                                    &actual, buffer) == 0)
                    unmodified = String (CharPointer_UTF16 (reinterpret_cast<CharPointer_UTF16::CharType*> (buffer)), 4);
            }
        }
        
        CFRelease (currentKeyboard);
    }
    
    // did the above layout conversion fail
    if (unmodified.isEmpty())
#endif
    {
        unmodified = CharPointer_UTF8 ([[ev charactersIgnoringModifiers] UTF8String]);
    //    unmodified = nsStringToJuce ([ev charactersIgnoringModifiers]);
    }
    
    int keyCode = unmodified[0];
    
    if (keyCode == 0x19) // (backwards-tab)
        keyCode = '\t';
    else if (keyCode == 0x03) // (enter)
        keyCode = '\r';
    else
        keyCode = (int) CharacterFunctions::toUpperCase ((juce_wchar) keyCode);
    
    if (([ev modifierFlags] & NSEventModifierFlagNumericPad) != 0)
    {
        const int numPadConversions[] = { '0', KeyPress::numberPad0, '1', KeyPress::numberPad1,
            '2', KeyPress::numberPad2, '3', KeyPress::numberPad3,
            '4', KeyPress::numberPad4, '5', KeyPress::numberPad5,
            '6', KeyPress::numberPad6, '7', KeyPress::numberPad7,
            '8', KeyPress::numberPad8, '9', KeyPress::numberPad9,
            '+', KeyPress::numberPadAdd, '-', KeyPress::numberPadSubtract,
            '*', KeyPress::numberPadMultiply, '/', KeyPress::numberPadDivide,
            '.', KeyPress::numberPadDecimalPoint,
            ',', KeyPress::numberPadDecimalPoint, // (to deal with non-english kbds)
            '=', KeyPress::numberPadEquals };
        
        for (int i = 0; i < numElementsInArray (numPadConversions); i += 2)
            if (keyCode == numPadConversions [i])
                keyCode = numPadConversions [i + 1];
    }
    
    return keyCode;
}

CGEventRef LowLevelKeyboardListener::myCGEventCallback(CGEventTapProxy proxy, CGEventType type, CGEventRef event, void *refcon)
{
    if (serialInterface)
    {
        if (!serialInterface->isConnected())
            return event;
    }
    
    if (!isHookActive)
        return event;
    
    
    // Paranoid sanity check.
    if (type != kCGEventKeyDown)
        return event;
    
    ModifierKeys modifiers = ModifierKeys::getCurrentModifiersRealtime();
    
    
    NSEvent* nsEvent = [NSEvent eventWithCGEvent:event];
    
    int convertedKeycode = getKeyCodeFromEvent(nsEvent);
    
    KeypressExtended shortcut;
    shortcut.keyPress = KeyPress(convertedKeycode, modifiers, 0);
    
    if (controllerManager != nullptr)
        shortcut.knobModifier = controllerManager->isKnobBeingTouched();
        
    bool shortcutExists = executeShortcut(shortcut);
    
    if (shortcutExists)
    {
        //Send the right action and consume the key by returning NULL
        return NULL;
    }
    
    // We must return the event for it to be useful.
    return event;
}
#endif

bool LowLevelKeyboardListener::isHookActive = true;
QuickAssignmentsPanel* LowLevelKeyboardListener::quickAssignmentsPanel = nullptr;
ControllerManager* LowLevelKeyboardListener::controllerManager = nullptr;
ActionBroadcaster* LowLevelKeyboardListener::broadcaster = nullptr;
SerialInterface* LowLevelKeyboardListener::serialInterface = nullptr;

