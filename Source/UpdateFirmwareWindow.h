/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UPDATEFIRMWAREWINDOW_H_INCLUDED
#define UPDATEFIRMWAREWINDOW_H_INCLUDED

#include "UpdateFirmwareComponent.h"
#include "SerialInterface.h"

class UpdateFirmwareWindow : public DocumentWindow
{
public:
	UpdateFirmwareWindow();
	~UpdateFirmwareWindow();

	void closeButtonPressed() override;

	void setSerialInterface(SerialInterface* theSerialInterface);

private:
	ScopedPointer<LookAndFeel_V3> lookAndFeel;
	ScopedPointer<UpdateFirmwareComponent> updateFirmwareComponent;
};

#endif
