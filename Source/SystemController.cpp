/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SystemController.h"

SystemController::SystemController() : Controller()
{
	residueClicks = 0;

	pointAtDragStart.x = 0;
	pointAtDragStart.y = 0;

	currentDragState = noDrag;
	currentDirectionState = vertical;
	currentRightToggleTouchState = noTouch;

	knobTouch = false;
	leftToggleTouch = false;
	leftToggleUp = false;
	rightToggleTouch = false;
	rightToggleUp = true;



	//Assignment stuff
	defaultAssignment.controlMode = SystemControl;
	currentAssignment = &defaultAssignment;
	currentAssignment->setClicksPerPixel(4);

	savedDirectionStateOfAssignment = currentDirectionState;
	savedDirectionState = currentDirectionState;

	currentDirectionChangeState = noDirectionChange;
}

SystemController::~SystemController()
{
}

void SystemController::setSensitivity(int sensitivity)
{
	switch (sensitivity)
	{
	case 1:
		defaultAssignment.clicksPerPixel = 20;
		break;
	case 2:
		defaultAssignment.clicksPerPixel = 12;
		break;
	case 3:
		defaultAssignment.clicksPerPixel = 8;
		break;
	case 4:
		defaultAssignment.clicksPerPixel = 7;
		break;
	case 5:
		defaultAssignment.clicksPerPixel = 5;
		break;
	case 6:
		defaultAssignment.clicksPerPixel = 4;
		break;
	case 7:
		defaultAssignment.clicksPerPixel = 3;
		break;
	case 8:
		defaultAssignment.clicksPerPixel = 2;
		break;
	case 9:
		defaultAssignment.clicksPerPixel = 1;
		break;
	default:
		defaultAssignment.clicksPerPixel = 4;
	}
}

void SystemController::setScrollWheelMode(int ScrollWheelModeID)
{
	defaultAssignment.scrollWheelMode = ScrollWheelModeID;
}

void SystemController::actionListenerCallback(const juce::String & message)
{
	const juce::ScopedLock myScopedLock(objectLock); // Do not allow more than one thread to manipulate the mouse cursor at a time.

	bool UndoNeeded = false;

	//Parse message and set parameters
	int rotation = 0;
	bool rotationEvent = false;

	if (message.contains("knobtouch"))
	{
		if (message.contains("1"))
		{
			knobTouch = true;
			knobTapChecker.justTouched();
		}
		else
		{
			knobTouch = false;

			//the knob has just been released, there may be a potential need for undo.
			bool wasDoubleTap = knobTapChecker.justUntouched();
			if (wasDoubleTap)
				UndoNeeded = true;
		}

	}
	else if (message.contains("lefttoggletouch"))
	{
		if (message.contains("1"))
		{
			leftToggleTouch = true;
		}
		else
		{
			leftToggleTouch = false;
		}
	}
	else if (message.contains("righttoggletouch"))
	{
		if (message.contains("1"))
		{
			rightToggleTouch = true;
			if (currentRightToggleTouchState == noTouch)
				currentRightToggleTouchState = beingTouched;
		}
		else
		{
			rightToggleTouch = false;

			currentRightToggleTouchState = noTouch;
		}

	}
	else if (message.contains("lefttoggleup"))
	{
		if (message.contains("1"))
		{
			leftToggleUp = true;
		}
		else
		{
			leftToggleUp = false;
		}
	}
	else if (message.contains("righttoggleup"))
	{
		if (message.contains("1"))
			rightToggleUp = true;
		else
			rightToggleUp = false;

		if (currentRightToggleTouchState == beingTouched)
			currentRightToggleTouchState = flippedWhileTouched;
	}
	else if (message.contains("rotation"))
	{
		rotation = message.substring(9).getIntValue();
		rotationEvent = true;
	}

	//Set direction state
	if (rightToggleUp && !rightToggleTouch)
	{
		currentDirectionState = vertical;
	}
	else if (rightToggleUp && rightToggleTouch)
	{
		if (currentRightToggleTouchState == flippedWhileTouched)
		{
			currentDirectionState = vertical;
		}
		else
		{
			currentDirectionState = horizontal;
		}
	}
	else if (!rightToggleUp && !rightToggleTouch)
	{
		currentDirectionState = horizontal;
	}
	else if (!rightToggleUp && rightToggleTouch)
	{
		if (currentRightToggleTouchState == flippedWhileTouched)
		{
			currentDirectionState = horizontal;
		}
		else
		{
			currentDirectionState = vertical;
		}
	}

	//save the selected direction on nOb
	if (currentDirectionChangeState == assignmentJustTriggered)
	{
		savedDirectionState = currentDirectionState;
		currentDirectionChangeState = directionJustSaved;
	}

	//check if the direction on nOb changed while an assignment is active
	if (currentDirectionChangeState == directionJustSaved && savedDirectionState != currentDirectionState)
	{
		currentDirectionChangeState = directionChanged;
	}

	//If the default assignment is active, we write the modes to it.
	if (currentAssignment == &defaultAssignment)
	{
		currentAssignment->currentDirectionState = currentDirectionState;
	}
	else //Some states may have to be overwritten based on the assignment and direction changes.
	{
		if (currentDirectionChangeState == directionChanged)
		{
			if (currentAssignment->currentDirectionState == vertical && currentDirectionState == horizontal)
				currentAssignment->currentDirectionState = horizontal;
			else if (currentAssignment->currentDirectionState == horizontal && currentDirectionState == vertical)
				currentAssignment->currentDirectionState = vertical;
		}
	}

	//Set drag state
	if (currentDragState == noDrag && knobTouch)
	{
		currentDragState = accumulatingClicks;
	}
	else if ((currentDragState == accumulatingClicks || currentDragState == dragArmed || currentDragState == beingDragged)
		&& !knobTouch)
	{


#if JUCE_WIN32
		currentAssignment->hideVolumeOSD();
#endif
        
#if JUCE_MAC
        currentAssignment->hideVolumeIndication();
#endif


		//Depending on wether there was a drag movement during the last tap, we decide on the number of movements that we have to get undone (between 1 and 3).
		//But not during the assignment process
		if (controllerManager->getCurrentAssignmentState() != assignment_Process_Started)
		{
			if (currentDragState == beingDragged)
				currentAssignment->numberOfMovementsToUndo++;
			else
				currentAssignment->numberOfMovementsToUndo--;

			if (currentAssignment->numberOfMovementsToUndo > 3)
				currentAssignment->numberOfMovementsToUndo = 3;
			if (currentAssignment->numberOfMovementsToUndo < 1)
				currentAssignment->numberOfMovementsToUndo = 1;
		}

		currentDragState = noDrag;
		residueClicks = 0;
	}

	//Knob rotation, while knob is being touched
	if (rotation != 0 && knobTouch)
	{
		int numberOfClicks = residueClicks + rotation;
		int movementInPixels = numberOfClicks / currentAssignment->clicksPerPixel;
		residueClicks = numberOfClicks - movementInPixels*currentAssignment->clicksPerPixel;

		if (currentDragState == accumulatingClicks && abs(movementInPixels) > 0)
			currentDragState = dragArmed;

		if (currentDragState == dragArmed) //Dragging has to start
		{
			currentDragState = beingDragged;

			currentAssignment->incrementCurrentMovementIndex(); //Move to next movement space 
			currentAssignment->movements[currentAssignment->currentMovementIndex].clear(); //Clear the movement space for recording of the movement

			if (controllerManager->getCurrentAssignmentState() != assignment_Process_Started) //Do not click during assignment process
			{
				//	currentAssignment->mouseClick();
			}
            
            currentAssignment->setCurrentVolume();

			juce::Point<float> point = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();
			pointAtDragStart.x = (int)point.x;
			pointAtDragStart.y = (int)point.y;

			//Save the point to current assignment:
			currentAssignment->pointAtDragStart = pointAtDragStart;
		}

		int trueChange = currentAssignment->masterVolumeChange(movementInPixels);
		currentAssignment->movements[currentAssignment->currentMovementIndex].addMovement(trueChange, 0);

		if (controllerManager->getCurrentAssignmentState() != assignment_Process_Started && controllerManager->getCurrentAssignmentState() != nothing_Assigned)
			currentAssignment->updatePosition();
	}

	//Perform undo if needed
	if (UndoNeeded)
	{
		currentAssignment->undoMasterVolumeChange();
	}
}

void SystemController::timerCallback(int timerID)
{
	
}

void SystemController::release() //Release the controller 
{
	knobTapChecker.reset();

#if JUCE_WIN32
	currentAssignment->hideVolumeOSD();
#endif

	releaseCurrentAssignment();
	assignToDefault();
}

void SystemController::triggerAssignment(Assignment* assignment)
{
	if (assignment == NULL)
		return;

	if (assignment == currentAssignment) //Already triggered.
		return;

/*
	// move the cursor to the right position
	juce::Point<float> point;

	point.x = (float)assignment->point.x;
	point.y = (float)assignment->point.y;

	//	juce::Desktop::getInstance().getMainMouseSource().setScreenPosition(point);
	moveSmoothlyTo(point);
 */

	//Set the currentAssignment to assignment and configure interaction
	currentAssignment = assignment;


	if (currentAssignment != &defaultAssignment)
	{
		currentDirectionChangeState = assignmentJustTriggered;
		savedDirectionStateOfAssignment = currentAssignment->currentDirectionState;
	}
}

void SystemController::moveSmoothlyTo(juce::Point<float> point)
{
	juce::Point<float> currentPos = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();

	juce::Point<float> movementVector = juce::Point<float>(point.x - currentPos.x, point.y - currentPos.y);

	if (movementVector.x == 0 && movementVector.y == 0)
		return;

	int stepNumber = 20;

	for (int i = 0; i < stepNumber; i++)
	{
		float factor = 0;

		float d = (float)stepNumber - 1;
		float t = (float)i / d;

		factor = t*t*t;

		juce::Point<int> newPosition = juce::Point<int>((int)(currentPos.x + factor*movementVector.x), (int)(currentPos.y + factor*movementVector.y));
		juce::Desktop::getInstance().setMousePosition(newPosition);
		juce::Thread::sleep(4);
	}
}

void SystemController::releaseCurrentAssignment()
{
	if (currentAssignment != &defaultAssignment)
	{
		currentAssignment->currentDirectionState = savedDirectionStateOfAssignment;
	}

	currentDirectionChangeState = noDirectionChange;
}

void SystemController::assignToDefault()
{
	if (currentAssignment != &defaultAssignment)
	{
		currentAssignment->currentDirectionState = savedDirectionStateOfAssignment;
	}

	currentDirectionChangeState = noDirectionChange;
	currentAssignment = &defaultAssignment;
}
