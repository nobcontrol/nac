/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SETSHORTCUTBUTTON_H_INCLUDED
#define SETSHORTCUTBUTTON_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
using namespace juce;

class QuickAssignmentShortcutEditor;
class KeypressExtended
{
public:
	bool operator== (const KeypressExtended& other) const noexcept
	{
		if (other.keyPress == keyPress && other.knobModifier == knobModifier)
			return true;
		else
			return false;
	}
    
    bool compareIgnoringModifier(const KeypressExtended& other)
    {
        return (other.keyPress == keyPress);
    }

	KeypressExtended& operator= (const KeypressExtended& other) noexcept
	{
		keyPress = other.keyPress;
		knobModifier = other.knobModifier;

		return *this;
	}
    
    String getDescription()
    {
        if (knobModifier)
            return String("knob + ")+keyPress.getTextDescription();
        else
            return keyPress.getTextDescription();
    }

	KeyPress keyPress;
	bool knobModifier;
};

class ShortcutEntryWindow : public AlertWindow
{
public:
	ShortcutEntryWindow(QuickAssignmentShortcutEditor* theOwner);
	bool keyPressed(const KeyPress& key) override;
	bool keyStateChanged(bool) override;

	KeypressExtended lastPress;
private:
	QuickAssignmentShortcutEditor* owner;
	ScopedPointer<LookAndFeel_V3> lookAndFeel;

	JUCE_DECLARE_NON_COPYABLE(ShortcutEntryWindow)
};


class SetShortcutButton : public Button
{
public:
	SetShortcutButton(const String& keyName, bool IsRemoveButton, QuickAssignmentShortcutEditor* Owner);

	void copyFromOther(SetShortcutButton& other);

	void paintButton(Graphics& g, bool /*isOver*/, bool /*isDown*/) override;
	void clicked() override;
	void fitToContent(const int h) noexcept;

	static void keyChosen(int result, SetShortcutButton* button);
	void setNewKey(const KeypressExtended& newKey);
	void assignNewKey();

	void resetShortcut();

    void setDefaultTooltip(String aToolTip);
    
	KeypressExtended shortcut;

private:
	String defaultName;
    String defaultTooltip;
	QuickAssignmentShortcutEditor* owner;
	bool isRemoveButton;
	ScopedPointer<ShortcutEntryWindow> currentShortcutEntryWindow;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(SetShortcutButton)
};

#endif
