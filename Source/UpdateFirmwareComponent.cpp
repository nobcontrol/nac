/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 4.3.0

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library - "Jules' Utility Class Extensions"
  Copyright (c) 2015 - ROLI Ltd.

  ==============================================================================
*/

//[Headers] You can add your own extra header files here...
/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/
//[/Headers]

#include "UpdateFirmwareComponent.h"


//[MiscUserDefs] You can add your own user definitions and misc code here...
//[/MiscUserDefs]

//==============================================================================
UpdateFirmwareComponent::UpdateFirmwareComponent ()
{
    //[Constructor_pre] You can add your own custom stuff here..
    //[/Constructor_pre]

    addAndMakeVisible (label = new Label ("new label",
                                          TRANS("Click on the button below to update nOb\'s firmware.")));
    label->setFont (Font (15.00f, Font::plain));
    label->setJustificationType (Justification::centred);
    label->setEditable (false, false, false);
    label->setColour (TextEditor::textColourId, Colours::black);
    label->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (updateButton = new TextButton ("update button"));
    updateButton->setButtonText (TRANS("Update..."));
    updateButton->addListener (this);


    //[UserPreSize]
	serialInterface = NULL;
    //[/UserPreSize]

    setSize (248, 112);


    //[Constructor] You can add your own custom stuff here..
    //[/Constructor]
}

UpdateFirmwareComponent::~UpdateFirmwareComponent()
{
    //[Destructor_pre]. You can add your own custom destruction code here..
    //[/Destructor_pre]

    label = nullptr;
    updateButton = nullptr;


    //[Destructor]. You can add your own custom destruction code here..
    //[/Destructor]
}

//==============================================================================
void UpdateFirmwareComponent::paint (Graphics& g)
{
    //[UserPrePaint] Add your own custom painting code here..
    //[/UserPrePaint]

    g.fillAll (Colour (0xffbdbdbd));

    //[UserPaint] Add your own custom painting code here..
    //[/UserPaint]
}

void UpdateFirmwareComponent::resized()
{
    //[UserPreResize] Add your own custom resize code here..
    //[/UserPreResize]

    label->setBounds (33, 17, 184, 32);
    updateButton->setBounds (41, 65, 168, 24);
    //[UserResized] Add your own custom resize handling here..
    //[/UserResized]
}

void UpdateFirmwareComponent::buttonClicked (Button* buttonThatWasClicked)
{
    //[UserbuttonClicked_Pre]
    //[/UserbuttonClicked_Pre]

    if (buttonThatWasClicked == updateButton)
    {
        //[UserButtonCode_updateButton] -- add your button handler code here..
		if (serialInterface)
		{
			bool result = serialInterface->updateFirmware();
            
            if (result)
            {
                AlertWindow::showMessageBox(AlertWindow::AlertIconType::InfoIcon, "Firmware update successful", "Your nOb's firmware has been successfully updated.");
                getParentComponent()->setVisible(false);       
            }
            else
            {
                AlertWindow::showMessageBox(AlertWindow::AlertIconType::WarningIcon, "Firmware update failed", "Your nOb's firmware failed to update. Reconnect your nOb and restart nAc to try again.");
                getParentComponent()->setVisible(false);
            }

			if (!serialInterface->isThreadRunning())
				serialInterface->startThread();

		}
        //[/UserButtonCode_updateButton]
    }

    //[UserbuttonClicked_Post]
    //[/UserbuttonClicked_Post]
}



//[MiscUserCode] You can add your own definitions of your custom methods or any other code here...
void UpdateFirmwareComponent::setSerialInterface(SerialInterface * theSerialInterface)
{
	serialInterface = theSerialInterface;
}
//[/MiscUserCode]


//==============================================================================
#if 0
/*  -- Projucer information section --

    This is where the Projucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="UpdateFirmwareComponent"
                 componentName="" parentClasses="public Component" constructorParams=""
                 variableInitialisers="" snapPixels="8" snapActive="1" snapShown="1"
                 overlayOpacity="0.330" fixedSize="1" initialWidth="248" initialHeight="112">
  <BACKGROUND backgroundColour="ffbdbdbd"/>
  <LABEL name="new label" id="9b64a36f9c9fc26e" memberName="label" virtualName=""
         explicitFocusOrder="0" pos="33 17 184 32" edTextCol="ff000000"
         edBkgCol="0" labelText="Click on the button below to update nOb's firmware."
         editableSingleClick="0" editableDoubleClick="0" focusDiscardsChanges="0"
         fontname="Default font" fontsize="15" bold="0" italic="0" justification="36"/>
  <TEXTBUTTON name="update button" id="d2dd6d54ffa0656a" memberName="updateButton"
              virtualName="" explicitFocusOrder="0" pos="41 65 168 24" buttonText="Update..."
              connectedEdges="0" needsCallback="1" radioGroupId="0"/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif


//[EndFile] You can add extra defines here...
//[/EndFile]
