/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "WindowHelper.h"
#include "ControllerManager.h"
#include "CursorController.h"
#include "ScrollWheelController.h"
#include "SystemController.h"

ControllerManager::ControllerManager()
{
	serialInterface = NULL;
	trayIcon = NULL;
    quickAssignmentsPanel = NULL;

	//Add default controller
	controllers.add(new CursorController());
	controllers.add(new ScrollWheelController());
	controllers.add(new SystemController());

	selectedMode = CursorControl;
	currentController = controllers[0];

	for (int i = 0; i < controllers.size(); i++)
	{
		controllers[i]->setControllerManager(this);
	}

	timeAtAssignmentStart = 0;
	currentAssignmentState = nothing_Assigned;
	lastValidAssignmentState = currentAssignmentState;
	currentTriggerState = noTrigger;
	lastAssignementTriggered = noTrigger;
	knobTouch = false;
	leftToggleTouch = false;
	rightToggleTouch = false;
	assignmentTimethreshold = 200;   //in milliseconds
	leftToggleFlippedDuringAssignmentProcess = false;
	newAssignmentsWereJustDefined = false;
	needspositionUpdating = false;

	assignmentNeedsPositionUpdatingAndTriggering = false;
	assignmentPtr = NULL;
}

ControllerManager::~ControllerManager()
{
}

void ControllerManager::setSerialInterface(SerialInterface* theSerialInterface)
{
	serialInterface = theSerialInterface;

	if (currentController)
	{
		serialInterface->addActionListener(currentController);
		setUpCurrentController();

		if (trayIcon)
		{
			//Set up cursor controller to be able to send actions to tray icon (because of diagonal/circular modes)
			if (controllers.size() >= 1)
				controllers[0]->addActionListener(trayIcon);
		}
	}

	serialInterface->addActionListener(this);
}

void ControllerManager::setTrayIcon(nAcTrayIcon * theTrayIcon)
{
	trayIcon = theTrayIcon;
}

void ControllerManager::setQuickAssignmentsPanel(QuickAssignmentsPanel* theQuickAssignmentsPanel)
{
    quickAssignmentsPanel = theQuickAssignmentsPanel;
}


void ControllerManager::initDefaultController()
{
	if (currentController)
	{
		currentController->release();
		serialInterface->removeActionListener(currentController);
	}

	if (controllers.size() > 0)
	{
		currentController = controllers[0]; //First controller is the default controller
		currentController->init();
		serialInterface->addActionListener(currentController);
		setUpCurrentController();
	}
}

void ControllerManager::changeDefaultController(int controllerIndex)
{
	if (controllerIndex >= 0 && controllerIndex < controllers.size())
	{
		if (currentController)
		{
			currentController->release();
			serialInterface->removeActionListener(currentController);
		}

		leftToggleTapChecker.reset();

		currentController = controllers[controllerIndex]; //First controller is the default controller
		currentController->init();
		serialInterface->addActionListener(currentController);
		setUpCurrentController();
	}
}

void ControllerManager::setSensitivity(int sensitivity)
{
    for (int i = 0; i<controllers.size(); i++)
    {
        controllers[i]->setSensitivity(sensitivity);
    }
}

void ControllerManager::setControlMode(int controlMode)
{
	if (controlMode == CursorControl)
	{
		changeDefaultController(0);
	}
	else if (controlMode == ScrollWheelControl)
	{
		changeDefaultController(1);
	}
	else if (controlMode == SystemControl)
	{
		changeDefaultController(2);
	}

	selectedMode = controlMode;
}

void ControllerManager::setPrimaryMouseButton(int MouseButtonID)
{
	controllers[0]->setPrimaryMouseButton(MouseButtonID);
}

void ControllerManager::setScrollWheelBehaviour(int ScrollWheelMode)
{
	if (controllers.size() >= 2)
		controllers[1]->setScrollWheelMode(ScrollWheelMode);
}

void ControllerManager::getAssignmentsForActiveWindow(juce::Array<Assignment*> &windowAssignments)
{
	juce::String owner;
	juce::String title;
	WindowHelper::getInstance()->getFocusedWindowInfo(owner, title);
    
    DBG("Active window > owner: " + owner + ", title: " + title);

	for (int i = 0; i < quickAssignmentsPanel->getNumberOfQuickAssignments(); i++)
	{
		Assignment* assignment = quickAssignmentsPanel->getAssignment(i);
		if (WindowHelper::getInstance()->windowInfoMatch(owner, title, assignment->assignmentPosition.owner, assignment->assignmentPosition.title))
			windowAssignments.add(assignment);
	}
    
    DBG("Active window > Number of quick assignments: " + juce::String(windowAssignments.size()));
}

void ControllerManager::actionListenerCallback(const juce::String & message)
{
	const juce::ScopedLock myScopedLock(objectLock); // Do not allow more than one thread to manipulate the controller manager

	if (message.contains("triggerleft"))
	{
		juce::Array<Assignment*> windowAssignments;
		getAssignmentsForActiveWindow(windowAssignments);
		
        juce::Point<int> cursorPosition;
        if (currentController->getCurrentAssignment()->controlMode == CursorControl && currentController->getCurrentAssignment()->currentOperationMode == stationaryMode && currentController->getCurrentAssignment()->mouseButtonIsBeingPressed)
            cursorPosition = currentController->getCurrentAssignment()->pointAtDragStart;
        else
            cursorPosition = juce::Desktop::getInstance().getMousePosition();
        
		juce::Point<int> relativePosition = WindowHelper::getInstance()->getRelativeCoordinatesToFocusedWindow(cursorPosition);

		int assignmentIndex = -1;
//		unsigned int mindistance = UINT_MAX;
		double mindistance = 10000000;

//		juce::Array<Assignment*> windowAssignmentsOnTheSameLine;
//		juce::Array<int> windowAssignmentsOnTheSameLineIndeces;

		for (int i = 0; i < windowAssignments.size(); i++)
		{
			if (windowAssignments[i]->assignmentPosition.relativePosition.x <= relativePosition.x)
			{
				/*
				unsigned int distance = relativePosition.x - windowAssignments[i]->assignmentPosition.relativePosition.x;
				if (distance < mindistance)
				{
					mindistance = distance;
					assignmentIndex = i;
				}
				*/
				if (currentController->getCurrentAssignment() != windowAssignments[i])
				{
					double distance = sqrt((relativePosition.x - windowAssignments[i]->assignmentPosition.relativePosition.x)*(relativePosition.x - windowAssignments[i]->assignmentPosition.relativePosition.x) +
						(relativePosition.y - windowAssignments[i]->assignmentPosition.relativePosition.y)*(relativePosition.y - windowAssignments[i]->assignmentPosition.relativePosition.y));

					juce::Point<double> currentPosition;
					currentPosition.x = (double)relativePosition.x;
					currentPosition.y = (double)relativePosition.y;

					juce::Point<double> assignmentPosition;
					assignmentPosition.x = windowAssignments[i]->assignmentPosition.relativePosition.x;
					assignmentPosition.y = windowAssignments[i]->assignmentPosition.relativePosition.y;

					double angleToAssignment = currentPosition.getAngleToPoint(assignmentPosition);
					double angleToLeftVector = -juce::double_Pi/2;

					double angleDifference = angleToAssignment - angleToLeftVector;
                    if (angleDifference<0)
                        angleDifference = angleDifference * (-1.0);
					double factor = angleDifference / (juce::double_Pi / 2);
					factor = 0.3 + factor*0.7;
			
					distance = distance*factor;

					if (distance < mindistance)
					{
						mindistance = distance;
						assignmentIndex = i;
					}
				}
			}
			/*
			else if (windowAssignments[i]->assignmentPosition.relativePosition.x == relativePosition.x && currentController->getCurrentAssignment() != windowAssignments[i])
			{
				windowAssignmentsOnTheSameLine.add(windowAssignments[i]);
				windowAssignmentsOnTheSameLineIndeces.add(i);
			}
			*/
		}

/*		if (windowAssignmentsOnTheSameLine.size() != 0)
		{
			//There are other assignments on the same vertical line, we find the one that is below.
			mindistance = UINT_MAX;
			for (int i = 0; i < windowAssignmentsOnTheSameLine.size(); i++)
			{
				if (windowAssignmentsOnTheSameLine[i]->assignmentPosition.relativePosition.y > relativePosition.y)
				{
					unsigned int distance = windowAssignmentsOnTheSameLine[i]->assignmentPosition.relativePosition.y - relativePosition.y;
					if (distance < mindistance)
					{
						mindistance = distance;
						assignmentIndex = windowAssignmentsOnTheSameLineIndeces[i];
					}
				}
			}
		} */

		if (assignmentIndex != -1)
		{
            DBG("Found appropriate assignment: " + juce::String(assignmentIndex));
            
			currentAssignmentState = lastValidAssignmentState;
#if JUCE_WIN32
			windowAssignments[assignmentIndex]->assignmentPosition.nativeWindow = (HWND)WindowHelper::getInstance()->getFocusedWindow();
#endif
#if JUCE_MAC
            windowAssignments[assignmentIndex]->assignmentPosition.nativeWindow = (void*)WindowHelper::getInstance()->getFrontMostWindow();
#endif
            
			triggerAssignment(windowAssignments[assignmentIndex]);
		}
	}
	else if (message.contains("triggerright"))
	{
		juce::Array<Assignment*> windowAssignments;
		getAssignmentsForActiveWindow(windowAssignments);

        juce::Point<int> cursorPosition;
        if (currentController->getCurrentAssignment()->controlMode == CursorControl && currentController->getCurrentAssignment()->currentOperationMode == stationaryMode && currentController->getCurrentAssignment()->mouseButtonIsBeingPressed)
            cursorPosition = currentController->getCurrentAssignment()->pointAtDragStart;
        else
            cursorPosition = juce::Desktop::getInstance().getMousePosition();
        
		juce::Point<int> relativePosition = WindowHelper::getInstance()->getRelativeCoordinatesToFocusedWindow(cursorPosition);

		int assignmentIndex = -1;
//		unsigned int mindistance = UINT_MAX;
		double mindistance = 1000000;


//		juce::Array<Assignment*> windowAssignmentsOnTheSameLine;
//		juce::Array<int> windowAssignmentsOnTheSameLineIndeces;

		for (int i = 0; i < windowAssignments.size(); i++)
		{
			if (windowAssignments[i]->assignmentPosition.relativePosition.x >= relativePosition.x)
			{
				/*
				unsigned int distance =  windowAssignments[i]->assignmentPosition.relativePosition.x - relativePosition.x;
				if (distance < mindistance)
				{
					mindistance = distance;
					assignmentIndex = i;
				}
				*/
				if (currentController->getCurrentAssignment() != windowAssignments[i])
				{
					double distance = sqrt((relativePosition.x - windowAssignments[i]->assignmentPosition.relativePosition.x)*(relativePosition.x - windowAssignments[i]->assignmentPosition.relativePosition.x) +
						(relativePosition.y - windowAssignments[i]->assignmentPosition.relativePosition.y)*(relativePosition.y - windowAssignments[i]->assignmentPosition.relativePosition.y));

					juce::Point<double> currentPosition;
					currentPosition.x = (double)relativePosition.x;
					currentPosition.y = (double)relativePosition.y;

					juce::Point<double> assignmentPosition;
					assignmentPosition.x = windowAssignments[i]->assignmentPosition.relativePosition.x;
					assignmentPosition.y = windowAssignments[i]->assignmentPosition.relativePosition.y;

					double angleToAssignment = currentPosition.getAngleToPoint(assignmentPosition);
					double angleToRightVector = juce::double_Pi/2;

					double angleDifference = angleToAssignment - angleToRightVector;
                    if (angleDifference<0)
                        angleDifference = angleDifference * (-1.0);
					double factor = angleDifference / (juce::double_Pi / 2);
					factor = 0.3 + factor*0.7;

					distance = distance*factor;

					if (distance < mindistance)
					{
						mindistance = distance;
						assignmentIndex = i;
					}
				}
			}
			/*
			else if (windowAssignments[i]->assignmentPosition.relativePosition.x == relativePosition.x && currentController->getCurrentAssignment() != windowAssignments[i])
			{
				windowAssignmentsOnTheSameLine.add(windowAssignments[i]);
				windowAssignmentsOnTheSameLineIndeces.add(i);
			}
			*/
		}

/*		if (windowAssignmentsOnTheSameLine.size() != 0)
		{
			//There are other assignments on the same vertical line, we find the one that is above.
			mindistance = UINT_MAX;
			for (int i = 0; i < windowAssignmentsOnTheSameLine.size(); i++)
			{
				if (windowAssignmentsOnTheSameLine[i]->assignmentPosition.relativePosition.y < relativePosition.y)
				{
					unsigned int distance = - windowAssignmentsOnTheSameLine[i]->assignmentPosition.relativePosition.y + relativePosition.y;
					if (distance < mindistance)
					{
						mindistance = distance;
						assignmentIndex = windowAssignmentsOnTheSameLineIndeces[i];
					}
				}
			}
		} */

		if (assignmentIndex != -1)
		{
            DBG("Found appropriate assignment: " + juce::String(assignmentIndex));
            
			currentAssignmentState = lastValidAssignmentState;
#if JUCE_WIN32
			windowAssignments[assignmentIndex]->assignmentPosition.nativeWindow = (HWND)WindowHelper::getInstance()->getFocusedWindow();
#endif
#if JUCE_MAC
            windowAssignments[assignmentIndex]->assignmentPosition.nativeWindow = (void*)WindowHelper::getInstance()->getFrontMostWindow();
#endif
            
			triggerAssignment(windowAssignments[assignmentIndex]);
		}
	}
	else if (message.contains("triggerup"))
	{
		juce::Array<Assignment*> windowAssignments;
		getAssignmentsForActiveWindow(windowAssignments);

        juce::Point<int> cursorPosition;
        if (currentController->getCurrentAssignment()->controlMode == CursorControl && currentController->getCurrentAssignment()->currentOperationMode == stationaryMode && currentController->getCurrentAssignment()->mouseButtonIsBeingPressed)
            cursorPosition = currentController->getCurrentAssignment()->pointAtDragStart;
        else
            cursorPosition = juce::Desktop::getInstance().getMousePosition();
        
        juce::Point<int> relativePosition = WindowHelper::getInstance()->getRelativeCoordinatesToFocusedWindow(cursorPosition);

		int assignmentIndex = -1;
//		unsigned int mindistance = UINT_MAX;
		double mindistance = 1000000;


//		juce::Array<Assignment*> windowAssignmentsOnTheSameLine;
//		juce::Array<int> windowAssignmentsOnTheSameLineIndeces;

		for (int i = 0; i < windowAssignments.size(); i++)
		{
			if (windowAssignments[i]->assignmentPosition.relativePosition.y <= relativePosition.y)
			{
				/*
				unsigned int distance = relativePosition.y - windowAssignments[i]->assignmentPosition.relativePosition.y;
				if (distance < mindistance)
				{
					mindistance = distance;
					assignmentIndex = i;
				}
				*/
				if (currentController->getCurrentAssignment() != windowAssignments[i])
				{
					double distance = sqrt((relativePosition.x - windowAssignments[i]->assignmentPosition.relativePosition.x)*(relativePosition.x - windowAssignments[i]->assignmentPosition.relativePosition.x) +
						(relativePosition.y - windowAssignments[i]->assignmentPosition.relativePosition.y)*(relativePosition.y - windowAssignments[i]->assignmentPosition.relativePosition.y));

					juce::Point<double> currentPosition;
					currentPosition.x = (double)relativePosition.x;
					currentPosition.y = (double)relativePosition.y;

					juce::Point<double> assignmentPosition;
					assignmentPosition.x = windowAssignments[i]->assignmentPosition.relativePosition.x;
					assignmentPosition.y = windowAssignments[i]->assignmentPosition.relativePosition.y;

					double angleToAssignment = 2 * juce::double_Pi + currentPosition.getAngleToPoint(assignmentPosition);
					double angleToUpVector = 0;

					double angleDifference = angleToAssignment - angleToUpVector;
                    if (angleDifference<0)
                        angleDifference = angleDifference * (-1.0);
					double factor = angleDifference / (juce::double_Pi / 2);
					factor = 0.3 + factor*0.7;

					distance = distance*factor;

					if (distance < mindistance)
					{
						mindistance = distance;
						assignmentIndex = i;
					}
				}
			}
			/*
			else if (windowAssignments[i]->assignmentPosition.relativePosition.y == relativePosition.y && currentController->getCurrentAssignment() != windowAssignments[i]) 
			{
				windowAssignmentsOnTheSameLine.add(windowAssignments[i]);
				windowAssignmentsOnTheSameLineIndeces.add(i);
			}
			*/
		}

/*		if (windowAssignmentsOnTheSameLine.size()!=0)
		{
			//There are other assignments on the same line, we find the one that is on the right.
			mindistance = UINT_MAX;
			for (int i = 0; i < windowAssignmentsOnTheSameLine.size(); i++)
			{
				if (windowAssignmentsOnTheSameLine[i]->assignmentPosition.relativePosition.x > relativePosition.x)
				{
					unsigned int distance = windowAssignmentsOnTheSameLine[i]->assignmentPosition.relativePosition.x - relativePosition.x;
					if (distance < mindistance)
					{
						mindistance = distance;
						assignmentIndex = windowAssignmentsOnTheSameLineIndeces[i];
					}
				}
			}
		} */

		if (assignmentIndex != -1)
		{
            DBG("Found appropriate assignment: " + juce::String(assignmentIndex));
            
			currentAssignmentState = lastValidAssignmentState;
#if JUCE_WIN32
			windowAssignments[assignmentIndex]->assignmentPosition.nativeWindow = (HWND)WindowHelper::getInstance()->getFocusedWindow();
#endif
#if JUCE_MAC
            windowAssignments[assignmentIndex]->assignmentPosition.nativeWindow = (void*)WindowHelper::getInstance()->getFrontMostWindow();
#endif
            
			triggerAssignment(windowAssignments[assignmentIndex]);
		}
	}
	else if (message.contains("triggerdown"))
	{
		juce::Array<Assignment*> windowAssignments;
		getAssignmentsForActiveWindow(windowAssignments);

        juce::Point<int> cursorPosition;
        if (currentController->getCurrentAssignment()->controlMode == CursorControl && currentController->getCurrentAssignment()->currentOperationMode == stationaryMode && currentController->getCurrentAssignment()->mouseButtonIsBeingPressed)
            cursorPosition = currentController->getCurrentAssignment()->pointAtDragStart;
        else
            cursorPosition = juce::Desktop::getInstance().getMousePosition();
        
        juce::Point<int> relativePosition = WindowHelper::getInstance()->getRelativeCoordinatesToFocusedWindow(cursorPosition);

		int assignmentIndex = -1;
	//	unsigned int mindistance = UINT_MAX;
		double mindistance = 1000000;


//		juce::Array<Assignment*> windowAssignmentsOnTheSameLine;
//		juce::Array<int> windowAssignmentsOnTheSameLineIndeces;

		for (int i = 0; i < windowAssignments.size(); i++)
		{
			if (windowAssignments[i]->assignmentPosition.relativePosition.y >= relativePosition.y)
			{
				/*
				unsigned int distance =  windowAssignments[i]->assignmentPosition.relativePosition.y - relativePosition.y;
				if (distance < mindistance)
				{
					mindistance = distance;
					assignmentIndex = i;
				}
				*/
				if (currentController->getCurrentAssignment() != windowAssignments[i])
				{
					double distance = sqrt((relativePosition.x - windowAssignments[i]->assignmentPosition.relativePosition.x)*(relativePosition.x - windowAssignments[i]->assignmentPosition.relativePosition.x) +
						(relativePosition.y - windowAssignments[i]->assignmentPosition.relativePosition.y)*(relativePosition.y - windowAssignments[i]->assignmentPosition.relativePosition.y));

					juce::Point<double> currentPosition;
					currentPosition.x = (double)relativePosition.x;
					currentPosition.y = (double)relativePosition.y;

					juce::Point<double> assignmentPosition;
					assignmentPosition.x = windowAssignments[i]->assignmentPosition.relativePosition.x;
					assignmentPosition.y = windowAssignments[i]->assignmentPosition.relativePosition.y;

					juce::Point<double> downVector;
					downVector.x = (double)relativePosition.x;
					downVector.y = (double)relativePosition.y+1;

					double angleToAssignment = currentPosition.getAngleToPoint(assignmentPosition);
					if (angleToAssignment < 0)
						angleToAssignment = -1 * angleToAssignment;

					double angleToDownVector = juce::double_Pi;

					double angleDifference = angleToAssignment - angleToDownVector;
                    if (angleDifference<0)
                        angleDifference = angleDifference * (-1.0);
					double factor = angleDifference / (juce::double_Pi / 2);
					factor = 0.3 + factor*0.7;

					distance = distance*factor;

					if (distance < mindistance)
					{
						mindistance = distance;
						assignmentIndex = i;
					}
				}
			}
			/*
			else if (windowAssignments[i]->assignmentPosition.relativePosition.y == relativePosition.y && currentController->getCurrentAssignment() != windowAssignments[i])
			{
				windowAssignmentsOnTheSameLine.add(windowAssignments[i]);
				windowAssignmentsOnTheSameLineIndeces.add(i);
			}
			*/
		}

		
/*		if (windowAssignmentsOnTheSameLine.size() != 0)
		{
			//There are other assignments on the same line, we find the one that is on the left.
			mindistance = UINT_MAX;
			for (int i = 0; i < windowAssignmentsOnTheSameLine.size(); i++)
			{
				if (windowAssignmentsOnTheSameLine[i]->assignmentPosition.relativePosition.x < relativePosition.x)
				{
					unsigned int distance = relativePosition.x - windowAssignmentsOnTheSameLine[i]->assignmentPosition.relativePosition.x ;
					if (distance < mindistance)
					{
						mindistance = distance;
						assignmentIndex = windowAssignmentsOnTheSameLineIndeces[i];
					}
				}
			}
		} */

		if (assignmentIndex != -1)
		{
            DBG("Found appropriate assignment: " + juce::String(assignmentIndex));
            
			currentAssignmentState = lastValidAssignmentState;
#if JUCE_WIN32
			windowAssignments[assignmentIndex]->assignmentPosition.nativeWindow = (HWND)WindowHelper::getInstance()->getFocusedWindow();
#endif
            
#if JUCE_MAC
            windowAssignments[assignmentIndex]->assignmentPosition.nativeWindow = (void*)WindowHelper::getInstance()->getFrontMostWindow();
#endif
            
			triggerAssignment(windowAssignments[assignmentIndex]);
		}
	}
    else if (message.contains("assign"))
    {
        int index = message.fromLastOccurrenceOf(" ", false, true).getIntValue();
        if (quickAssignmentsPanel)
        {
            Assignment* assignment = quickAssignmentsPanel->getAssignment(index);
            if (assignment)
            {
                currentAssignmentState = lastValidAssignmentState;
                
                DBG("Copying current settings to assignment #" + juce::String(index));
                *assignment = *(currentController->getCurrentAssignment());

				if (currentController->getCurrentDragState() == beingDragged)
				{
					assignmentNeedsPositionUpdatingAndTriggering = true;
					assignmentPtr = assignment;
				}
				else
				{
					assignment->updateAssignmentPosition();
					triggerAssignment(assignment);
				}

				quickAssignmentsPanel->updateTooltipsOfShortcutEditor(index);
            }
        }
    }
    else if (message.contains("trigger"))
    {
        int index = message.fromLastOccurrenceOf(" ", false, true).getIntValue();
        if (quickAssignmentsPanel)
        {
            Assignment* assignment = quickAssignmentsPanel->getAssignment(index);
            if (assignment)
            {
                currentAssignmentState = lastValidAssignmentState;
                triggerAssignment(assignment);
            }
        }
    }
	else if (message.contains("knobtouch"))
	{
		if (message.contains("1"))
		{
			knobTouch = true;
		}
		else
		{
			knobTouch = false;
		}
	}
	else if (message.contains("lefttoggletouch"))
	{
		if (message.contains("1"))
		{
			leftToggleTouch = true;

			leftToggleTapChecker.justTouched();

			if (currentAssignmentState != assignment_Process_Started)
			{
				//Assignment stuff 
				timeAtAssignmentStart = juce::Time::currentTimeMillis();
				currentAssignmentState = assignment_Process_Started;

				//notice the current assignment settings and mouse position
                DBG("Noticing current settings for super quick assignment...");
				temporaryAssignment = *currentController->getCurrentAssignment();

				temporaryAssignment.updateAssignmentPosition();
				juce::Point<float> cursor = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();
				temporaryAssignment.point.x = (int)cursor.x;
				temporaryAssignment.point.y = (int)cursor.y;
				if (currentController->getCurrentDragState() == noDrag)
					temporaryAssignment.pointAtDragStart = temporaryAssignment.point;
			}
		}
		else
		{
			leftToggleTouch = false;

			bool doubleTap = leftToggleTapChecker.justUntouched();
			if (doubleTap)
			{
				if (currentAssignmentState == assignment_Process_Started)
					currentAssignmentState = lastValidAssignmentState;

				if (currentAssignmentState == A_is_Assigned)
				{
					if (currentTriggerState == noTrigger)
					{
                        DBG("Trying to activate super quick assignment A...");
						triggerAssignment(&assignmentA);
					}
				}
				else if (currentAssignmentState == A_and_B_are_Assigned)
				{
					if (lastAssignementTriggered == assignmentA_triggered)
					{
                        DBG("Trying to activate super quick assignment B...");
						releaseCurrentAssignment();
						triggerAssignment(&assignmentB);
					}
					else if (lastAssignementTriggered == assignmentB_triggered)
					{
                        DBG("Trying to activate super quick assignment A...");
						releaseCurrentAssignment();
						triggerAssignment(&assignmentA);
					}
				}

			}

		}
	}
	else if (message.contains("righttoggletouch"))
	{
		if (message.contains("1"))
		{
			rightToggleTouch = true;
		}
		else
		{
			rightToggleTouch = false;
		}

	}
	else if (message.contains("lefttoggleup"))
	{
		if (currentAssignmentState == assignment_Process_Started)
			leftToggleFlippedDuringAssignmentProcess = true;
	}

	//Detect Assignments (left toggle quick assignments)
	detectAssignments();

	//Update and trigger if needed (for assignments that occur during manupulation)
	if (assignmentNeedsPositionUpdatingAndTriggering)
	{
		if (currentController->getCurrentDragState() == noDrag) //Interaction just stopped
		{
			if (assignmentPtr)
			{
				if (assignmentPtr->currentOperationMode == dragMode)
				{
					assignmentPtr->updateAssignmentPosition();					
				}
				else if (assignmentPtr->currentOperationMode == stationaryMode)
				{
					assignmentPtr->updateAssignmentPosition(juce::Point<float>((float)currentController->getCurrentAssignment()->pointAtDragStart.x, (float)currentController->getCurrentAssignment()->pointAtDragStart.y));
				}

				triggerAssignment(assignmentPtr);

				assignmentPtr = NULL;
			}

			assignmentNeedsPositionUpdatingAndTriggering = false;
		}
	}
}

void ControllerManager::timerCallback(int timerID)
{
	//Detect mouse activity during idle triggered assignement
	if (timerID == assignmentActivityTimer)
	{
		if (currentTriggerState != noTrigger) //Some kind of trigger is active
		{
			if (currentController->getCurrentDragState() == noDrag) //The user does not use the nOb
			{
				juce::Point<float> point = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();
				juce::Point<int> globalPoint = currentController->getCurrentAssignment()->getGlobalPosition();

				if (globalPoint.x != (int)point.x && globalPoint.y != (int)point.y)
				{
					if (currentController->getCurrentAssignment() == &assignmentA)
						lastAssignementTriggered = assignmentB_triggered; //So that it flips back to A, when there is a new trigger
					else if (currentController->getCurrentAssignment() == &assignmentB)
						lastAssignementTriggered = assignmentA_triggered; //So that it flips back to B, when there is a new trigger

					assignToDefault();
                    
                    DBG("Assignment deactived. Default settings restored.");
				}
			}
		}
	}
}

int ControllerManager::getCurrentAssignmentState()
{
	return currentAssignmentState;
}

bool ControllerManager::isKnobBeingTouched()
{
	return knobTouch;
}

void ControllerManager::setUpCurrentController()
{
	currentController->setUp(serialInterface->getKnobIsBeingTouched(),
		serialInterface->getLeftToggleIsBeingTouched(),
		serialInterface->getLeftToggleIsUp(),
		serialInterface->getRightToggleIsBeingTouched(),
		serialInterface->getRightToggleIsUp());
}

void ControllerManager::detectAssignments()
{
	//Set for potential assignments
	if (currentAssignmentState == assignment_Process_Started)
	{
		if (!leftToggleTouch) //the left toggle is released, perhaps there are assignments to make.
		{
			juce::int64 currentTime = juce::Time::currentTimeMillis();
			if (currentAssignmentState == assignment_Process_Started && currentTime - timeAtAssignmentStart < assignmentTimethreshold) //duration was too short
			{
                DBG("Touch duration of left toggle was too short for a super quick assignment.");
				currentAssignmentState = lastValidAssignmentState; //No assignment was defined.
			}
			else if (currentAssignmentState == assignment_Process_Started) //Duration was long enoung, decide on the number of assignemnts to make.
			{
				juce::Point<float> cursorPoint = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();

				if (temporaryAssignment.point.x == (int)cursorPoint.x && temporaryAssignment.point.y == (int)cursorPoint.y && !currentController->getCurrentAssignment()->mouseButtonIsBeingPressed) //There was no move, only A has to be assigned!
				{
					if (!leftToggleFlippedDuringAssignmentProcess) //the user did not just want to flip the switch (which would have caused touch and there fore possible intention to assign)
					{
						assignmentA = *currentController->getCurrentAssignment();

						if (assignmentA.currentDirectionState == diagonal)
							assignmentA.radiusHadToGetSetToOneForDirectDiagonalInteraction = true;

						assignmentA.updateAssignmentPosition();
						juce::Point<float> cursor = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();
						assignmentA.point.x = (int)cursor.x;
						assignmentA.point.y = (int)cursor.y;

						if (currentController->getCurrentDragState() == noDrag)
							assignmentA.pointAtDragStart = assignmentA.point;

						currentAssignmentState = A_is_Assigned;

						lastValidAssignmentState = currentAssignmentState;
						newAssignmentsWereJustDefined = true;
						//Logger::outputDebugString("Assigned A");
                        
                        DBG("Super quick assignment A set.");
					}
					else  //The user just wanted to flip the left toggle, no assignment!
					{
						leftToggleFlippedDuringAssignmentProcess = false;
						currentAssignmentState = lastValidAssignmentState;
					}

				}
			//	else if (currentController->getCurrentAssignment()->mouseButtonIsBeingPressed) //MAY BE BUGGY    The mouse has not been released yet == we are still manipulating the same parameter, therefore assign only A.
				else if (currentController->getCurrentDragState() == beingDragged)
				{
					assignmentA = *currentController->getCurrentAssignment();

					assignmentA.updateAssignmentPosition();
					juce::Point<float> cursor = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();
					assignmentA.point.x = (int)cursor.x;
					assignmentA.point.y = (int)cursor.y;

					if (currentController->getCurrentDragState() == noDrag)
						assignmentA.pointAtDragStart = assignmentA.point;

					currentAssignmentState = A_is_Assigned;

					lastValidAssignmentState = currentAssignmentState;
					newAssignmentsWereJustDefined = true;
					needspositionUpdating = true;
					//Logger::outputDebugString("Assigned A");
                    
                    DBG("Super quick assignment A set.");
				}
				else  // A and B have to be assigned
				{
					assignmentA = temporaryAssignment;

					if (assignmentA.currentDirectionState == diagonal)
						assignmentA.radiusHadToGetSetToOneForDirectDiagonalInteraction = true;

					assignmentB = *currentController->getCurrentAssignment();

					if (assignmentB.currentDirectionState == diagonal)
						assignmentB.radiusHadToGetSetToOneForDirectDiagonalInteraction = true;

					assignmentB.updateAssignmentPosition();
					juce::Point<float> cursor = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();
					assignmentB.point.x = (int)cursor.x;
					assignmentB.point.y = (int)cursor.y;

					if (currentController->getCurrentDragState() == noDrag)
						assignmentB.pointAtDragStart = assignmentB.point;

					currentAssignmentState = A_and_B_are_Assigned;

					lastValidAssignmentState = currentAssignmentState;
					newAssignmentsWereJustDefined = true;
					//Logger::outputDebugString("Assigned A + B");
                    
                    DBG("Super quick assignment A and B set.");
				}
			}
		}
	}

	//Trigger the last just created assignment, once the user has let go.
	if (newAssignmentsWereJustDefined && !knobTouch && !leftToggleTouch && !rightToggleTouch)
	{
		if (currentAssignmentState == A_is_Assigned)
		{
			if (needspositionUpdating)
			{
				if (assignmentA.currentOperationMode == dragMode)
				{
					assignmentA.updateAssignmentPosition();
				}
				else if (assignmentA.currentOperationMode == stationaryMode)
				{
					assignmentA.updateAssignmentPosition(juce::Point<float>((float)currentController->getCurrentAssignment()->pointAtDragStart.x, (float)currentController->getCurrentAssignment()->pointAtDragStart.y));
				}
				needspositionUpdating = false;
			}

			triggerAssignment(&assignmentA);
			newAssignmentsWereJustDefined = false;
		}
		else if (currentAssignmentState == A_and_B_are_Assigned)
		{
			triggerAssignment(&assignmentB);
			newAssignmentsWereJustDefined = false;
		}
	}
}

void ControllerManager::triggerAssignment(Assignment *assignment)
{
    //Check if the windows in focus meets the owner/window name requirements, an replace the window pointer with the windows in focus.
    if (assignment->controlMode != SystemControl)
    {
        juce::String owner;
        juce::String title;
        
#if JUCE_WIN32
        HWND window = (HWND)WindowHelper::getInstance()->getFocusedWindowInfo(owner, title);
		DBG("Active window > owner: " + owner + ", title: " + title);
		if (WindowHelper::getInstance()->windowInfoMatch(owner, title, assignment->assignmentPosition.owner, assignment->assignmentPosition.title))
		{
			DBG("Active window matches the window of the assignment to be triggered.");
			if (window!=(HWND)assignment->assignmentPosition.nativeWindow)
			{
				DBG("The assignment's window pointer has to be updated...");
				assignment->assignmentPosition.nativeWindow = (void*)window;
			}
		}
#endif
        
#if JUCE_MAC
        AXUIElementRef* windowRefPtr = (AXUIElementRef*)WindowHelper::getInstance()->getFocusedWindowInfo(owner, title);
        DBG("Active window > owner: " + owner + ", title: " + title);
        if (WindowHelper::getInstance()->windowInfoMatch(owner, title, assignment->assignmentPosition.owner, assignment->assignmentPosition.title))
        {
            DBG("Active window matches the window of the assignment to be triggered.");
            if (assignment->assignmentPosition.nativeWindow)
            {
                if (*((AXUIElementRef*)assignment->assignmentPosition.nativeWindow)!=*windowRefPtr && windowRefPtr)
                {
                    DBG("The assignment's window pointer has to be updated. Releasing old pointer...");
                    CFRelease(*((AXUIElementRef*)assignment->assignmentPosition.nativeWindow));
                    assignment->assignmentPosition.nativeWindow = (void*)windowRefPtr;
                }
            }
            else
            {
                DBG("Assignment to be triggered has no window pointer attached to it...");
            }
        }
        else
        {
            DBG("Active window does not match the window of the assignment to be triggered.");
        }
#endif
    }
    
    
	if (!assignment->myWindowIsValid() && assignment->controlMode != SystemControl)
	{
#if JUCE_WIN32
		HWND window = (HWND)WindowHelper::getInstance()->findWindow(assignment->assignmentPosition.owner, assignment->assignmentPosition.title);
#endif

#if JUCE_MAC
        AXUIElementRef* window = (AXUIElementRef*)WindowHelper::getInstance()->findWindow(assignment->assignmentPosition.owner, assignment->assignmentPosition.title);
#endif
        if (window)
        {
            DBG("Original assignment window not found. Successfully replaced with alternative.");
            assignment->assignmentPosition.nativeWindow = window;
        }
        else
        {
            DBG("No assignment related window was found. Activation canceled.");
            return;
        }
	}
	else if (assignment->controlMode == SystemControl) //If system assignment, then just update the assignment position to current mouse position. That way, the assignment stays active as long as we do not move the mouse.
	{
		assignment->updateAssignmentPosition();
	}


	if (currentController->getCurrentAssignment()->controlMode != assignment->controlMode)
	{
        DBG("Assignment's control mode varies from current mode. Switching...");
		changeDefaultController(assignment->controlMode);
	}

	currentController->release();
	currentController->triggerAssignment(assignment);

	//Set the right trigger state
	if (currentController->getCurrentAssignment() == &assignmentA)
	{
		currentTriggerState = assignmentA_triggered;
		lastAssignementTriggered = currentTriggerState;
	}
	else if (currentController->getCurrentAssignment() == &assignmentB)
	{
		currentTriggerState = assignmentB_triggered;
		lastAssignementTriggered = currentTriggerState;
	}
    else
    {
        currentTriggerState = external_assignment_triggered;
    }

	startTimer(assignmentActivityTimer, 10);  //Check every 10ms if there was a normal mouse movement (not coming from nOb)
}

void ControllerManager::releaseCurrentAssignment()
{
	currentController->releaseCurrentAssignment();
}

void ControllerManager::assignToDefault()
{
	currentController->assignToDefault();

	changeDefaultController(selectedMode);

	currentTriggerState = noTrigger;
	stopTimer(assignmentActivityTimer);
}


