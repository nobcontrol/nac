/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 4.3.1

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library - "Jules' Utility Class Extensions"
  Copyright (c) 2015 - ROLI Ltd.

  ==============================================================================
*/

//[Headers] You can add your own extra header files here...
/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/
//[/Headers]

#include "SharedAssignmentsEditor.h"


//[MiscUserDefs] You can add your own user definitions and misc code here...
//[/MiscUserDefs]

//==============================================================================
SharedAssignmentsEditor::SharedAssignmentsEditor ()
{
    //[Constructor_pre] You can add your own custom stuff here..
    //[/Constructor_pre]

    addAndMakeVisible (windowTitleEditor = new TextEditor ("window title editor"));
    windowTitleEditor->setMultiLine (true);
    windowTitleEditor->setReturnKeyStartsNewLine (true);
    windowTitleEditor->setReadOnly (false);
    windowTitleEditor->setScrollbarsShown (true);
    windowTitleEditor->setCaretVisible (true);
    windowTitleEditor->setPopupMenuEnabled (true);
    windowTitleEditor->setText (String());

    addAndMakeVisible (explanationLabel = new Label ("explanation label",
                                                     TRANS("If a certain window is not available, nAc will try to map its assignments to another window with a title that matches the appropriate wildcard expression from the following list:\n")));
    explanationLabel->setFont (Font (15.00f, Font::plain));
    explanationLabel->setJustificationType (Justification::centred);
    explanationLabel->setEditable (false, false, false);
    explanationLabel->setColour (TextEditor::textColourId, Colours::black);
    explanationLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));


    //[UserPreSize]
    //[/UserPreSize]

    setSize (600, 400);


    //[Constructor] You can add your own custom stuff here..
    //[/Constructor]
}

SharedAssignmentsEditor::~SharedAssignmentsEditor()
{
    //[Destructor_pre]. You can add your own custom destruction code here..
    //[/Destructor_pre]

    windowTitleEditor = nullptr;
    explanationLabel = nullptr;


    //[Destructor]. You can add your own custom destruction code here..
    //[/Destructor]
}

//==============================================================================
void SharedAssignmentsEditor::paint (Graphics& g)
{
    //[UserPrePaint] Add your own custom painting code here..
    //[/UserPrePaint]

    g.fillAll (Colour (0xffe2e2e2));

    //[UserPaint] Add your own custom painting code here..
    //[/UserPaint]
}

void SharedAssignmentsEditor::resized()
{
    //[UserPreResize] Add your own custom resize code here..
    //[/UserPreResize]

    windowTitleEditor->setBounds (11, 66, 386, 200);
    explanationLabel->setBounds (6, 7, 408, 56);
    //[UserResized] Add your own custom resize handling here..

	int extraSpaceBecauseOfNoneNativeWindow = 0;
    int widthCorrection = 0;
#if JUCE_WIN32
	extraSpaceBecauseOfNoneNativeWindow = 28;
#endif

#if JUCE_MAC
    widthCorrection = 8;
#endif

	int stringWidth = explanationLabel->getFont().getStringWidth(explanationLabel->getText());
	int labelWidth = getParentWidth() - 31 + widthCorrection;

	int numberOfLines = stringWidth / labelWidth + 1;
	int labelHeight = explanationLabel->getFont().getHeight() * numberOfLines +15;

	explanationLabel->setBounds(explanationLabel->getX(), explanationLabel->getY(), labelWidth, labelHeight);

	windowTitleEditor->setBounds(windowTitleEditor->getX(), explanationLabel->getY()+explanationLabel->getHeight() + 10, labelWidth, windowTitleEditor->getHeight());
	windowTitleEditor->setBounds(windowTitleEditor->getX(), windowTitleEditor->getY(), labelWidth, getParentHeight() - windowTitleEditor->getY() - 12 - extraSpaceBecauseOfNoneNativeWindow);

    //[/UserResized]
}



//[MiscUserCode] You can add your own definitions of your custom methods or any other code here...
String SharedAssignmentsEditor::getText()
{
	return windowTitleEditor->getText();
}

void SharedAssignmentsEditor::setText(String text)
{
	windowTitleEditor->setText(text);
}
//[/MiscUserCode]


//==============================================================================
#if 0
/*  -- Projucer information section --

    This is where the Projucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="SharedAssignmentsEditor"
                 componentName="" parentClasses="public Component" constructorParams=""
                 variableInitialisers="" snapPixels="8" snapActive="1" snapShown="1"
                 overlayOpacity="0.330" fixedSize="0" initialWidth="600" initialHeight="400">
  <BACKGROUND backgroundColour="ffe2e2e2"/>
  <TEXTEDITOR name="window title editor" id="1028e1ecabf463fa" memberName="windowTitleEditor"
              virtualName="" explicitFocusOrder="0" pos="11 66 386 200" initialText=""
              multiline="1" retKeyStartsLine="1" readonly="0" scrollbars="1"
              caret="1" popupmenu="1"/>
  <LABEL name="explanation label" id="4ff70250f3e20137" memberName="explanationLabel"
         virtualName="" explicitFocusOrder="0" pos="6 7 408 56" edTextCol="ff000000"
         edBkgCol="0" labelText="If a certain window is not available, nAc will try to map its assignments to another window with a title that matches the appropriate wildcard expression from the following list:&#10;"
         editableSingleClick="0" editableDoubleClick="0" focusDiscardsChanges="0"
         fontname="Default font" fontsize="15" bold="0" italic="0" justification="36"/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif


//[EndFile] You can add extra defines here...
//[/EndFile]
