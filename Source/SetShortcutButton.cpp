/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "LowLevelKeyboardListener.h"
#include "SetShortcutButton.h"
#include "QuickAssignmentShortcutEditor.h"
#include "QuickAssignmentsPanel.h"
#include "ControllerManager.h"

ShortcutEntryWindow::ShortcutEntryWindow(QuickAssignmentShortcutEditor* theOwner) : AlertWindow(TRANS("Set a new shortcut..."),
	TRANS("Please press a key combination"),
	AlertWindow::NoIcon)
{
	lookAndFeel = new LookAndFeel_V3();
	setLookAndFeel(lookAndFeel);

	owner = theOwner;
	setEscapeKeyCancels(true);

	// (avoid return + escape keys getting processed by the buttons..)
	for (int i = getNumChildComponents(); --i >= 0;)
	{
		getChildComponent(i)->setWantsKeyboardFocus(false);
	}

	setWantsKeyboardFocus(true);
	grabKeyboardFocus();
}

bool ShortcutEntryWindow::keyPressed(const KeyPress & key)
{
	AlertWindow::keyPressed(key);

	if (key == KeyPress::escapeKey)
	{
		return false;
	}
	else if (key == KeyPress::leftKey || key == KeyPress::rightKey || key == KeyPress::upKey || key == KeyPress::downKey)
	{
		return false;
	}

	lastPress.keyPress = key;
	lastPress.knobModifier = false;

	ControllerManager* controllerManager = owner->getQuickAssignmentsPanel()->getControllerManager();
	if (controllerManager)
		lastPress.knobModifier = owner->getQuickAssignmentsPanel()->getControllerManager()->isKnobBeingTouched();

	String message(TRANS("Key") + ": " + lastPress.keyPress.getTextDescription());
	setMessage(message);

	AlertWindow::keyPressed(KeyPress(KeyPress::escapeKey));

	return true;
}

bool ShortcutEntryWindow::keyStateChanged(bool)
{
	return true;
}

SetShortcutButton::SetShortcutButton(const String& keyName, bool IsRemoveButton, QuickAssignmentShortcutEditor* Owner) : Button(keyName)
{
	defaultName = keyName;
	owner = Owner;

	isRemoveButton = IsRemoveButton;

	setWantsKeyboardFocus(false);
	setTriggeredOnMouseDown(!IsRemoveButton);

	setTooltip(IsRemoveButton ? TRANS("Remove this pair of shortcuts.")
				: TRANS("Click to set a shortcut."));
}

void SetShortcutButton::copyFromOther(SetShortcutButton& other)
{
	setName(other.getName());
    setTooltip(other.getTooltip());
	shortcut = other.shortcut;
}


void SetShortcutButton::paintButton(Graphics & g, bool, bool)
{
	const Colour textColour(this->findColour(0x100ad01, true));
	int width = getWidth();
	int height = getHeight();

	if (!isRemoveButton)
	{
		if (this->isEnabled())
		{
			g.setColour(textColour.withAlpha(this->isDown() ? 0.4f : (this->isOver() ? 0.2f : 0.1f)));
			g.fillRoundedRectangle(this->getLocalBounds().toFloat(), 4.0f);
			g.drawRoundedRectangle(this->getLocalBounds().toFloat(), 4.0f, 1.0f);
		}

		g.setColour(textColour);
		g.setFont(height * 0.6f);
		g.drawFittedText(getName(), 4, 0, width - 8, height, Justification::centred, 1);
	}
	else
	{
		const float thickness = 7.0f;
		const float indent = 22.0f;

		Path p;
		p.addEllipse(0.0f, 0.0f, 100.0f, 100.0f);
		p.addRectangle(indent, 50.0f - thickness, 100.0f - indent * 2.0f, thickness * 2.0f);
		p.setUsingNonZeroWinding(false);

		g.setColour(textColour.brighter(0.5f).withAlpha(this->isDown() ? 0.7f : (this->isOver() ? 0.5f : 0.3f)));
		g.fillPath(p, p.getTransformToScaleToFit(2.0f, 2.0f, width - 4.0f, height - 4.0f, true));
	}

	if (this->hasKeyboardFocus(false))
	{
		g.setColour(textColour.withAlpha(0.4f));
		g.drawRect(0, 0, width, height);
	}
}

void SetShortcutButton::clicked()
{
	if (isRemoveButton)
	{
		owner->removeButtonPressed();
	}
	else
	{
		assignNewKey();
	}
}

void SetShortcutButton::fitToContent(const int h) noexcept
{
	if (isRemoveButton)
		setSize(h, h);
	else
		setSize(jlimit(h * 4, h * 8, 6 + Font(h * 0.6f).getStringWidth(getName())), h);
}

void SetShortcutButton::keyChosen(int result, SetShortcutButton * button)
{
	if (button != nullptr && button->currentShortcutEntryWindow != nullptr)
	{
		button->currentShortcutEntryWindow->setVisible(false);

		if (button->currentShortcutEntryWindow->lastPress.keyPress.isValid())
		{
            DBG(String("New shortcut: ") + ((button->currentShortcutEntryWindow->lastPress.knobModifier)?String("knob + "):String("")) + button->currentShortcutEntryWindow->lastPress.keyPress.getTextDescription());
        
			button->setNewKey(button->currentShortcutEntryWindow->lastPress);
		}

		button->currentShortcutEntryWindow = nullptr;
	}

	LowLevelKeyboardListener::getInstance()->setActive(true);
}


void SetShortcutButton::setNewKey(const KeypressExtended & newKey)
{
	shortcut = newKey;
	if (newKey.knobModifier)
    {
		setName(String("knob + ") + shortcut.keyPress.getTextDescription());
    }
	else
    {
		setName(shortcut.keyPress.getTextDescription());
    }
	owner->shortcutSet(this);
	repaint();
}

void SetShortcutButton::assignNewKey()
{
	LowLevelKeyboardListener::getInstance()->setActive(false);

	currentShortcutEntryWindow = new ShortcutEntryWindow(owner);
	currentShortcutEntryWindow->enterModalState(true, ModalCallbackFunction::forComponent(keyChosen, this));
}

void SetShortcutButton::resetShortcut()
{
	setName(defaultName);
    setTooltip(defaultTooltip);
	shortcut = KeypressExtended();
	repaint();
}

void SetShortcutButton::setDefaultTooltip(String aToolTip)
{
    defaultTooltip = aToolTip;
}

