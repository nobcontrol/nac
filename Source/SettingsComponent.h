/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 4.3.1

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library - "Jules' Utility Class Extensions"
  Copyright (c) 2015 - ROLI Ltd.

  ==============================================================================
*/

#ifndef __JUCE_HEADER_963CDBB75B5D1790__
#define __JUCE_HEADER_963CDBB75B5D1790__

//[Headers]     -- You can add your own extra header files here --
/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../JuceLibraryCode/JuceHeader.h"
using namespace juce;

#include "ControllerManager.h"
#include "QuickAssignmentsPanel.h"
#include "SharedAssignmentsEditor.h"

class AboutWindow;
class SharedAssignmentsWindow;
//[/Headers]



//==============================================================================
/**
                                                                    //[Comments]
    An auto-generated component, created by the Projucer.

    Describe your class and how it works here!
                                                                    //[/Comments]
*/
class SettingsComponent  : public juce::Component,
                           public SliderListener,
                           public ComboBoxListener,
                           public ButtonListener
{
public:
    //==============================================================================
    SettingsComponent ();
    ~SettingsComponent();

    //==============================================================================
    //[UserMethods]     -- You can add your own custom methods in this section.
	friend class AboutWindow;

    void setControllerManager(ControllerManager* theControllerManager);
    QuickAssignmentsPanel* getQuickAssignmentsPanel();

	void saveSettings();
	void loadSettings();
    //[/UserMethods]

    void paint (Graphics& g) override;
    void resized() override;
    void sliderValueChanged (Slider* sliderThatWasMoved) override;
    void comboBoxChanged (ComboBox* comboBoxThatHasChanged) override;
    void buttonClicked (juce::Button* buttonThatWasClicked) override;
    void mouseDoubleClick (const MouseEvent& e) override;

    // Binary resources:
    static const char* nAc_logo_png;
    static const int nAc_logo_pngSize;


private:
    //[UserVariables]   -- You can add your own custom variables in this section.
    ControllerManager* controllerManager;
	ScopedPointer<AboutWindow> aboutWindow;
	ScopedPointer<SharedAssignmentsWindow> sharedAssignmentsWindow;
    //[/UserVariables]

    //==============================================================================
    ScopedPointer<Slider> sensitivitySlider;
    ScopedPointer<Label> sensitivityLabel;
    ScopedPointer<Label> controlModeLabel;
    ScopedPointer<ComboBox> controlModeComboBox;
    ScopedPointer<Label> primaryMouseButtonLabel;
    ScopedPointer<ComboBox> primaryMouseButtonComboBox;
    ScopedPointer<Label> scrollwheelBehaviourLabel;
    ScopedPointer<ComboBox> scrollwheelBehaviourComboBox;
    ScopedPointer<Viewport> quickAssignmentsViewport;
    ScopedPointer<Label> quickAssignmentsLabel;
    ScopedPointer<TextButton> advancedButton;
    Image cachedImage_nAc_logo_png_1;


    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SettingsComponent)
};

//[EndFile] You can add extra defines here...
class AboutWindow : public AlertWindow
{
public:
	AboutWindow(SettingsComponent* theOwner) : AlertWindow("", "nOb Assignment center a.k.a. nAc", juce::AlertWindow::AlertIconType::NoIcon)
	{
		lookAndFeel = new LookAndFeel_V3();
		setLookAndFeel(lookAndFeel);

		owner = theOwner;
		setEscapeKeyCancels(true);
		setSize(350, 165);
	}

	bool keyPressed(const KeyPress& key) override
	{
		AlertWindow::keyPressed(KeyPress(KeyPress::escapeKey));
		setVisible(false);
		return true;
	}

	void paint(Graphics& g) override
	{
		AlertWindow::paint(g);

		g.drawImage(owner->cachedImage_nAc_logo_png_1,
			124, -12, 100, 100,
			0, 0, owner->cachedImage_nAc_logo_png_1.getWidth(), owner->cachedImage_nAc_logo_png_1.getHeight());
	}

	void mouseDown(const MouseEvent &event) override
	{
		AlertWindow::keyPressed(KeyPress(KeyPress::escapeKey));
		setVisible(false);
	}

private:
	SettingsComponent* owner;
	ScopedPointer<LookAndFeel_V3> lookAndFeel;

	JUCE_DECLARE_NON_COPYABLE(AboutWindow)
};

class SharedAssignmentsWindow : public juce::DocumentWindow, public juce::KeyListener
{
public:
	SharedAssignmentsWindow() : DocumentWindow("Share quick assignments among windows...", juce::Colours::lightgrey, juce::DocumentWindow::closeButton, true)
	{
		lookAndFeel = new LookAndFeel_V3();
		setLookAndFeel(lookAndFeel);

#if JUCE_WIN32
		setUsingNativeTitleBar(false);
		setTitleBarHeight(20);
#endif

#if JUCE_MAC
		setUsingNativeTitleBar(true);
#endif

		editorComponent = new SharedAssignmentsEditor();
		setContentOwned(editorComponent, true);

		setResizable(true, false);
		setSize(450, 306);
		centreAroundComponent(NULL, getWidth(), getHeight());

        setResizeLimits(300, 200, 1920, 1080);

        setVisible(false);
		addToDesktop();
		setAlwaysOnTop(true);

		addKeyListener(this);
	}

	bool keyPressed(const KeyPress &key, Component *originatingComponent) override
	{
		if (key == KeyPress::escapeKey)
		{
            exitModalState(0);
			setVisible(false);
			return true;
		}

		return false;
	}

	void closeButtonPressed() override
	{
        exitModalState(0);
        setVisible(false);

		updateWildcards();
	}

	String getEditorsText()
	{
		return editorComponent->getText();
	}

	void setEditorsText(String text)
	{
		editorComponent->setText(text);
	}

    void updateWildcards();
    /*
	{
		String sharedAssignmentsText = getEditorsText();
		StringArray lines;
		lines.addLines(sharedAssignmentsText);
		lines.removeEmptyStrings();

		String newText = lines.joinIntoString("\n");
		setEditorsText(newText);

		WindowHelper::getInstance()->setWildcards(lines);
	}*/

private:
	ScopedPointer<LookAndFeel_V3> lookAndFeel;

	SharedAssignmentsEditor* editorComponent;
};
//[/EndFile]

#endif   // __JUCE_HEADER_963CDBB75B5D1790__
