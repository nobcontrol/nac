/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CursorController.h"

CursorController::CursorController() : Controller()
{
	residueClicks = 0;

	pointAtDragStart.x = 0;
	pointAtDragStart.y = 0;

	currentDragState = noDrag;
	currentDirectionState = vertical;
	currentRightToggleTouchState = noTouch;
	currentOperationMode = dragMode;
	currentCircularOrDiagonalTriggerState = noCircularOrDiagonalTrigger;

	knobTouch = false;
	leftToggleTouch = false;
	leftToggleUp = false;
	rightToggleTouch = false;
	rightToggleUp = true;
	
	//Assignment stuff
	defaultAssignment.controlMode = CursorControl;
	currentAssignment = &defaultAssignment;
	currentAssignment->setClicksPerPixel(4);

	savedDirectionStateOfAssignment = currentDirectionState;
	savedDirectionState = currentDirectionState;

	currentDirectionChangeState = noDirectionChange;
}

CursorController::~CursorController()
{
}

void CursorController::setSensitivity(int sensitivity)
{
	switch (sensitivity)
	{
	case 1:
		defaultAssignment.clicksPerPixel = 20;
		break;
	case 2:
		defaultAssignment.clicksPerPixel = 12;
		break;
	case 3:
		defaultAssignment.clicksPerPixel = 8;
		break;
	case 4:
		defaultAssignment.clicksPerPixel = 7;
		break;
	case 5:
		defaultAssignment.clicksPerPixel = 5;
		break;
	case 6:
		defaultAssignment.clicksPerPixel = 4;
		break;
	case 7:
		defaultAssignment.clicksPerPixel = 3;
		break;
	case 8:
		defaultAssignment.clicksPerPixel = 2;
		break;
	case 9:
		defaultAssignment.clicksPerPixel = 1;
		break;
	default:
		defaultAssignment.clicksPerPixel = 4;
	}
}

void CursorController::setPrimaryMouseButton(int MouseButtonID)
{
	defaultAssignment.primaryMouseButton = MouseButtonID;
}

void CursorController::actionListenerCallback(const juce::String & message)
{
	const juce::ScopedLock myScopedLock(objectLock); // Do not allow more than one thread to manipulate the mouse cursor at a time.

	bool UndoNeeded = false;
	bool doubleTapOnRightToggle = false;

	//Parse message and set parameters
	int rotation = 0;
	bool rotationEvent = false;

	if (message.contains("knobtouch"))
	{
		if (message.contains("1"))
		{
			knobTouch = true;
			knobTapChecker.justTouched();
		}
		else
		{
			knobTouch = false;

			//the knob has just been released, there may be a potential need for undo.
			bool wasDoubleTap = knobTapChecker.justUntouched();
			if (wasDoubleTap)
				UndoNeeded = true;
		}	
	}
	else if (message.contains("lefttoggletouch"))
	{
		if (message.contains("1"))
		{
			leftToggleTouch = true;
		}
		else
		{
			leftToggleTouch = false;
		}
	}
	else if (message.contains("righttoggletouch"))
	{
		if (message.contains("1"))
		{
			rightToggleTouch = true;
			if (currentRightToggleTouchState == noTouch)
				currentRightToggleTouchState = beingTouched;
			
			rightToggleTapChecker.justTouched();
		}	
		else
		{
			rightToggleTouch = false;

			currentRightToggleTouchState = noTouch;

			doubleTapOnRightToggle = rightToggleTapChecker.justUntouched();
			if (doubleTapOnRightToggle && currentCircularOrDiagonalTriggerState == noCircularOrDiagonalTrigger && currentAssignment == &defaultAssignment)
			{
				currentCircularOrDiagonalTriggerState = circularOrDiagonalArmed;

				if (currentDragState == beingDragged) //It is time to reset the radius and phase, because the user just defined it with his drag action
				{
					currentCircularOrDiagonalTriggerState = circularOrDiagonalActive;
					currentAssignment->setRadiusAndPhase();
					currentAssignment->radiusAndPhaseJustSet = true;
				}
				else
				{
					//start a timer to disable the trigger if 2 seconds have passed.
					startTimer(circularOrDiagonalArmedTimer, 2000);
				}
			}
		}
			
	}
	else if (message.contains("lefttoggleup"))
	{
		if (message.contains("1"))
		{
			leftToggleUp = true;
		}
		else
		{
			leftToggleUp = false;
		}
	}
	else if (message.contains("righttoggleup"))
	{
		if (message.contains("1"))
			rightToggleUp = true;
		else
			rightToggleUp = false;

		if (currentRightToggleTouchState == beingTouched)
			currentRightToggleTouchState = flippedWhileTouched;
	}
	else if (message.contains("rotation"))
	{
		rotation = message.substring(9).getIntValue();
		rotationEvent = true;
	}

	//Set direction state
	if (rightToggleUp && !rightToggleTouch)
	{
		if (currentCircularOrDiagonalTriggerState == circularOrDiagonalArmed || currentCircularOrDiagonalTriggerState == circularOrDiagonalActive)
		{
			currentDirectionState = circular;
			sendActionMessage("CircularIcon");
			if (currentDragState == beingDragged && !currentAssignment->radiusAndPhaseJustSet)
			{
				currentAssignment->setRadiusAndPhase();
				currentAssignment->radiusAndPhaseJustSet = true;
			}		
		}
		else
			currentDirectionState = vertical;
	}
	else if (rightToggleUp && rightToggleTouch)
	{
		if (currentRightToggleTouchState == flippedWhileTouched)
		{
			if (currentCircularOrDiagonalTriggerState == circularOrDiagonalArmed || currentCircularOrDiagonalTriggerState == circularOrDiagonalActive)
			{
				currentDirectionState = circular;
				sendActionMessage("CircularIcon");
				if (currentDragState == beingDragged && !currentAssignment->radiusAndPhaseJustSet)
				{
					currentAssignment->setRadiusAndPhase();
					currentAssignment->radiusAndPhaseJustSet = true;
				}	
			}
			else
				currentDirectionState = vertical;
		}
		else
		{
			if (currentCircularOrDiagonalTriggerState == circularOrDiagonalArmed || currentCircularOrDiagonalTriggerState == circularOrDiagonalActive)
			{
				currentDirectionState = diagonal;
				sendActionMessage("DiagonalIcon");
			}	
			else
				currentDirectionState = horizontal;
		}
	}
	else if (!rightToggleUp && !rightToggleTouch)
	{
		if (currentCircularOrDiagonalTriggerState == circularOrDiagonalArmed || currentCircularOrDiagonalTriggerState == circularOrDiagonalActive)
		{
			currentDirectionState = diagonal;
			sendActionMessage("DiagonalIcon");
		}
		else
			currentDirectionState = horizontal;
	}
	else if (!rightToggleUp && rightToggleTouch)
	{
		if (currentRightToggleTouchState == flippedWhileTouched)
		{
			if (currentCircularOrDiagonalTriggerState == circularOrDiagonalArmed || currentCircularOrDiagonalTriggerState == circularOrDiagonalActive)
			{
				currentDirectionState = diagonal;
				sendActionMessage("DiagonalIcon");
			}
			else
				currentDirectionState = horizontal;
		}
		else
		{
			if (currentCircularOrDiagonalTriggerState == circularOrDiagonalArmed || currentCircularOrDiagonalTriggerState == circularOrDiagonalActive)
			{
				currentDirectionState = circular;
				sendActionMessage("CircularIcon");
				if (currentDragState == beingDragged && !currentAssignment->radiusAndPhaseJustSet)
				{
					currentAssignment->setRadiusAndPhase();
					currentAssignment->radiusAndPhaseJustSet = true;
				}	
			}
			else
				currentDirectionState = vertical;
		}
	}

	//Set operation state
	if (leftToggleUp)
		currentOperationMode = stationaryMode;
	else
		currentOperationMode = dragMode;

	//save the selected direction on nOb
	if (currentDirectionChangeState == assignmentJustTriggered)
	{
		savedDirectionState = currentDirectionState;
		currentDirectionChangeState = directionJustSaved;
	}

	//check if the direction on nOb changed while an assignment is active
	if (currentDirectionChangeState == directionJustSaved && savedDirectionState != currentDirectionState)
	{
		currentDirectionChangeState = directionChanged;
	}

	//If the default assignment is active, we write the modes to it.
	if (currentAssignment == &defaultAssignment)
	{
		currentAssignment->currentOperationMode = currentOperationMode;
		currentAssignment->currentDirectionState = currentDirectionState;
	}
	else //Some states may have to be overwritten based on the assignment and direction changes.
	{
		if (currentDirectionChangeState == directionChanged)
		{
			if (currentAssignment->currentDirectionState == vertical && currentDirectionState == horizontal)
				currentAssignment->currentDirectionState = horizontal;
			else if (currentAssignment->currentDirectionState == horizontal && currentDirectionState == vertical)
				currentAssignment->currentDirectionState = vertical;
			else if (currentAssignment->currentDirectionState == circular && currentDirectionState == diagonal)
				currentAssignment->currentDirectionState = diagonal;
			else if (currentAssignment->currentDirectionState == diagonal && currentDirectionState == circular)
				currentAssignment->currentDirectionState = circular;
		}

		if (currentAssignment->currentDirectionState == circular || currentAssignment->currentDirectionState == diagonal)
			currentCircularOrDiagonalTriggerState = circularOrDiagonalArmed;
	}
		
	//Set drag state
	if (currentDragState == noDrag && knobTouch)
	{
		currentDragState = accumulatingClicks;
	}
	else if ((currentDragState == accumulatingClicks || currentDragState == dragArmed || currentDragState == beingDragged)
		      && !knobTouch)
	{
		if (currentDragState == beingDragged)
		{
			currentAssignment->mouseRelease();

			//Bring the cursor back to its station (if the is no assignment process going on)
			if (currentAssignment->currentOperationMode == stationaryMode && controllerManager->getCurrentAssignmentState()  != assignment_Process_Started)
			{
				moveSmoothlyTo(juce::Point<float>((float)currentAssignment->pointAtDragStart.x, (float)currentAssignment->pointAtDragStart.y));
			}
			else if (currentAssignment->currentOperationMode == dragMode && controllerManager->getCurrentAssignmentState() != assignment_Process_Started && currentAssignment!=&defaultAssignment)
			{
				currentAssignment->updateAssignmentPosition();
			}
			
			if (currentCircularOrDiagonalTriggerState == circularOrDiagonalActive) //circular/diagonal interaction just ended
			{
				currentCircularOrDiagonalTriggerState = noCircularOrDiagonalTrigger;
				sendActionMessage("DefaultIcon");
				stopTimer(circularOrDiagonalArmedTimer);
				currentAssignment->radiusAndPhaseJustSet = false; 
				currentAssignment->radiusAndPhaseNeverSet = false; 
			}
		}

		//Depending on wether there was a drag movement during the last tap, we decide on the number of movements that we have to get undone (between 1 and 3).
		//But not during the assignment process
		if (controllerManager->getCurrentAssignmentState() != assignment_Process_Started)
		{
			if (currentDragState == beingDragged)
				currentAssignment->numberOfMovementsToUndo++;
			else
				currentAssignment->numberOfMovementsToUndo--;

			if (currentAssignment->numberOfMovementsToUndo > 3)
				currentAssignment->numberOfMovementsToUndo = 3;
			if (currentAssignment->numberOfMovementsToUndo < 1)
				currentAssignment->numberOfMovementsToUndo = 1;
		}
		
		currentDragState = noDrag;
		residueClicks = 0;
	}

	//Knob rotation, while knob is being touched
	if (rotation != 0 && knobTouch)
	{
		int numberOfClicks = residueClicks + rotation;
		int movementInPixels = numberOfClicks / currentAssignment->clicksPerPixel;
		residueClicks = numberOfClicks - movementInPixels*currentAssignment->clicksPerPixel;

		if (currentDragState == accumulatingClicks && abs(movementInPixels) > 0)
			currentDragState = dragArmed;

		if (currentDragState == dragArmed) //Dragging has to start
		{
			currentDragState = beingDragged; 

			currentAssignment->incrementCurrentMovementIndex(); //Move to next movement space 
			currentAssignment->movements[currentAssignment->currentMovementIndex].clear(); //Clear the movement space for recording of the movement

			if (controllerManager->getCurrentAssignmentState() != assignment_Process_Started) //Do not click during assignment process
				currentAssignment->mouseClick();

			juce::Point<float> point = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();
			pointAtDragStart.x = (int)point.x;
			pointAtDragStart.y = (int)point.y;

			//Save the point to current assignment:
			currentAssignment->pointAtDragStart = pointAtDragStart;

			//Circular/Diagonal Stuff
			if (currentCircularOrDiagonalTriggerState == circularOrDiagonalArmed)
			{
				if (currentAssignment->currentOperationMode == stationaryMode && currentAssignment->currentDirectionState == circular) //Move to periphery of circle 
				{
					currentAssignment->moveToCircle(true);
				}
				else if (currentAssignment->currentDirectionState == diagonal)
				{
					//only for the default assignment
					if (currentAssignment == &defaultAssignment)
						currentAssignment->radius = 1;
					else if (currentAssignment->radiusHadToGetSetToOneForDirectDiagonalInteraction)
					{
						currentAssignment->radius = 1;
						currentAssignment->radiusHadToGetSetToOneForDirectDiagonalInteraction = false;
					}

					//Inverse phase if the vector is phasing to the left
					if (cos(currentAssignment->phase) < 0)
						currentAssignment->phase += juce::float_Pi;
				}

				if (currentAssignment->radiusAndPhaseNeverSet && currentAssignment->currentDirectionState == diagonal)
				{
					//currentAssignment->radius = 40;
					currentAssignment->phase = -juce::float_Pi / 4;
					currentAssignment->radiusAndPhaseNeverSet = false;
				}

				currentAssignment->radiusAndPhaseJustSet = true; // No need to set the radius and phase again (they are already appropriate, because of last radius/diagonal interaction)

				currentCircularOrDiagonalTriggerState = circularOrDiagonalActive;
			}
		}

		if (currentAssignment->currentDirectionState == vertical)
		{
			currentAssignment->moveCursor(0, -movementInPixels);
			currentAssignment->movements[currentAssignment->currentMovementIndex].addMovement(0, -movementInPixels);

			if (currentAssignment!=&defaultAssignment && currentAssignment->currentOperationMode==dragMode)
				if (controllerManager->getCurrentAssignmentState() != assignment_Process_Started && controllerManager->getCurrentAssignmentState() != nothing_Assigned)
				{
			//		currentAssignment->updateAssignmentPosition();
			//		currentAssignment->updatePosition();
				}
		}
		else if (currentAssignment->currentDirectionState == horizontal)
		{
			currentAssignment->moveCursor(movementInPixels, 0);
			currentAssignment->movements[currentAssignment->currentMovementIndex].addMovement(movementInPixels, 0);
			
			if (currentAssignment != &defaultAssignment && currentAssignment->currentOperationMode == dragMode)
				if (controllerManager->getCurrentAssignmentState() != assignment_Process_Started && controllerManager->getCurrentAssignmentState() != nothing_Assigned)
				{
			//		currentAssignment->updateAssignmentPosition();
			//		currentAssignment->updatePosition();
				}
		}
		else if (currentAssignment->currentDirectionState == circular)
		{
			float phaseMovement = 2 * juce::float_Pi * (float)rotation / 2400.0f;

			int oldY = (int)((float)currentAssignment->radius * sin(currentAssignment->phase));
			int oldX = (int)((float)currentAssignment->radius * cos(currentAssignment->phase));

			currentAssignment->phase = currentAssignment->phase + phaseMovement;

			int newY = (int)((float)currentAssignment->radius * sin(currentAssignment->phase));
			int newX = (int)((float)currentAssignment->radius * cos(currentAssignment->phase));

			int differenceX = newX - oldX;
			int differenceY = newY - oldY;

			currentAssignment->moveCursor(differenceX, differenceY);
			currentAssignment->movements[currentAssignment->currentMovementIndex].addMovement(differenceX, differenceY);

			if (currentAssignment != &defaultAssignment && currentAssignment->currentOperationMode == dragMode)
				if (controllerManager->getCurrentAssignmentState() != assignment_Process_Started && controllerManager->getCurrentAssignmentState() != nothing_Assigned)
				{
			//		currentAssignment->updateAssignmentPosition();
			//		currentAssignment->updatePosition();
				}
		}
		else if (currentAssignment->currentDirectionState == diagonal)
		{
			int oldY = (int)((float)currentAssignment->radius * sin(currentAssignment->phase));
			int oldX = (int)((float)currentAssignment->radius * cos(currentAssignment->phase));

			currentAssignment->radius = currentAssignment->radius + movementInPixels;

			int newY = (int)((float)currentAssignment->radius * sin(currentAssignment->phase));
			int newX = (int)((float)currentAssignment->radius * cos(currentAssignment->phase));

			int differenceX = newX - oldX;
			int differenceY = newY - oldY;

			currentAssignment->moveCursor(differenceX, differenceY);
			currentAssignment->movements[currentAssignment->currentMovementIndex].addMovement(differenceX, differenceY);

			if (currentAssignment != &defaultAssignment && currentAssignment->currentOperationMode == dragMode)
				if (controllerManager->getCurrentAssignmentState() != assignment_Process_Started && controllerManager->getCurrentAssignmentState() != nothing_Assigned)
				{
			//		currentAssignment->updateAssignmentPosition();
			//		currentAssignment->updatePosition();
				}
		}
	}

	//Perform undo if needed
	if (UndoNeeded)
	{
		currentAssignment->performUndo();
	}
}

void CursorController::timerCallback(int timerID)
{
	if (timerID == circularOrDiagonalArmedTimer && currentCircularOrDiagonalTriggerState == circularOrDiagonalArmed) //if no interaction has happened
	{
		currentCircularOrDiagonalTriggerState = noCircularOrDiagonalTrigger;
		sendActionMessage("DefaultIcon");
		stopTimer(circularOrDiagonalArmedTimer);
	}
	else if (timerID == circularOrDiagonalArmedTimer && currentCircularOrDiagonalTriggerState == noCircularOrDiagonalTrigger) //interaction has finished
	{
		stopTimer(circularOrDiagonalArmedTimer);
	}
}

void CursorController::release() //Release the controller 
{
	knobTapChecker.reset();
	rightToggleTapChecker.reset();

	bool click = currentAssignment->mouseButtonIsBeingPressed;
    if (click)
        currentAssignment->mouseRelease();
	
	if (currentAssignment->currentOperationMode == dragMode && click) // && currentDragState == beingDragged 
	{
		currentAssignment->updateAssignmentPosition();
	}

	releaseCurrentAssignment();
	assignToDefault();
}

void CursorController::triggerAssignment(Assignment* assignment)
{
	if (assignment == NULL)
		return;

	if (assignment == currentAssignment) //Already triggered.
    {
        DBG("Assignment already active.");
		return;
    }

	//First release the mouse click, if needed
	bool click = currentAssignment->mouseButtonIsBeingPressed;
	if (click)
	{
        currentAssignment->mouseRelease();
		currentAssignment->updateAssignmentPosition();
	}

	// move the cursor to the right position
	juce::Point<float> point;
	if (assignment->myWindowIsValid())
	{
        DBG("Assignment's window exists (assignment in cursor controller mode).");

		assignment->bringWindowToFront();
		juce::Point<int> globalPoint = assignment->getGlobalPosition();
		point.x = (float)globalPoint.x;
		point.y = (float)globalPoint.y;
	}
	else if (assignment->currentOperationMode == stationaryMode)
	{
		point.x = (float)assignment->pointAtDragStart.x;
		point.y = (float)assignment->pointAtDragStart.y;
	}
	else if (assignment->currentOperationMode == dragMode)
	{
		point.x = (float)assignment->point.x;
		point.y = (float)assignment->point.y;
	}

	moveSmoothlyTo(point);

	//Set the currentAssignment to assignment and configure interaction
	currentAssignment = assignment;

	if (click) 
	{
		currentAssignment->mouseClick();

		//Move to circle under one condition
		if (currentAssignment->currentDirectionState == circular && currentAssignment->currentOperationMode == stationaryMode)
			currentAssignment->moveToCircle(true);

		if (currentAssignment->currentDirectionState == circular || currentAssignment->currentDirectionState == diagonal)
		{
			currentCircularOrDiagonalTriggerState = circularOrDiagonalActive;  //Update the state to make sense.
			if (currentAssignment->currentDirectionState == circular)
				sendActionMessage("CircularIcon");
			else
				sendActionMessage("DiagonalIcon");
		}
	}
	else //there was no dragging when the assignment got triggered
	{
		if (currentAssignment->currentDirectionState == circular || currentAssignment->currentDirectionState == diagonal)
		{
			currentCircularOrDiagonalTriggerState = circularOrDiagonalArmed;  //Update the state to make sense.
			if (currentAssignment->currentDirectionState == circular)
				sendActionMessage("CircularIcon");
			else
				sendActionMessage("DiagonalIcon");
		}
	}

	if (currentAssignment != &defaultAssignment)
	{
        DBG("Assignment (in cursor controller mode) activated.");
        
		currentDirectionChangeState = assignmentJustTriggered;
		savedDirectionStateOfAssignment = currentAssignment->currentDirectionState;
	}
}

void CursorController::moveSmoothlyTo(juce::Point<float> point)
{
	juce::Point<float> currentPos = juce::Desktop::getInstance().getMainMouseSource().getScreenPosition();

	juce::Point<float> movementVector = juce::Point<float>(point.x - currentPos.x, point.y - currentPos.y);
	
	if (movementVector.x == 0 && movementVector.y == 0)
		return;
	
	int stepNumber = 20;

	for (int i = 0; i < stepNumber; i++)
	{
		float factor = 0;

		float d = (float)stepNumber - 1;
		float t = (float)i / d;

		factor = t*t*t;

		juce::Point<int> newPosition = juce::Point<int>((int)(currentPos.x + factor*movementVector.x), (int)(currentPos.y + factor*movementVector.y));
		juce::Desktop::getInstance().setMousePosition(newPosition);
		juce::Thread::sleep(4);
	}
}

void CursorController::releaseCurrentAssignment()
{
	if (currentCircularOrDiagonalTriggerState == circularOrDiagonalArmed)
	{
		currentCircularOrDiagonalTriggerState = noCircularOrDiagonalTrigger;
		sendActionMessage("DefaultIcon");
	}

	if (currentAssignment != &defaultAssignment)
	{
		currentAssignment->currentDirectionState = savedDirectionStateOfAssignment;
	}

	currentDragState = noDrag;
	currentDirectionChangeState = noDirectionChange;
}

void CursorController::assignToDefault()
{
	if (currentCircularOrDiagonalTriggerState == circularOrDiagonalArmed)
	{
		currentCircularOrDiagonalTriggerState = noCircularOrDiagonalTrigger;
		sendActionMessage("DefaultIcon");
	}

	if (currentAssignment != &defaultAssignment)
	{
		currentAssignment->currentDirectionState = savedDirectionStateOfAssignment;
	}

	currentDirectionChangeState = noDirectionChange;
	currentAssignment = &defaultAssignment;
}

void CursorController::setUp(bool isKnobBeingTouched, bool isLeftToggleBeingTouched, bool isLeftToggleUp, bool isRightToggleBeingTouched, bool isRightToggleUp)
{
	//Setting up toggle positions
	if (isLeftToggleUp)
		actionListenerCallback("lefttoggleup 1");
	else
		actionListenerCallback("lefttoggleup 0");

	if (isRightToggleUp)
		actionListenerCallback("righttoggleup 1");
	else
		actionListenerCallback("righttoggleup 0");

	//Setting up touch state for right toggle switch
	if (isRightToggleBeingTouched)
		actionListenerCallback("righttoggletouch 1");
	else
		actionListenerCallback("righttoggletouch 0");
}
