/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WINDOWHELPER_H_INCLUDED
#define WINDOWHELPER_H_INCLUDED

#if __APPLE__
 // #import <AppKit/AppKit.h>
 // #include <CoreFoundation/CoreFoundation.h>
 // #include <ApplicationServices/ApplicationServices.h>
    #include <Carbon/Carbon.h>
#endif


#include "../JuceLibraryCode/JuceHeader.h"

#if JUCE_WIN32
	#include <Windows.h>
	#include <Psapi.h>
#endif

class WindowHelper
{
private:
	static juce::StringArray wildcards;
	static bool matchesWildcard;
	static int wildcardIndex;

public:
	WindowHelper()
	{
	}

	~WindowHelper()
	{
		clearSingletonInstance();
	}

	void setWildcards(juce::StringArray& theWildcards);

    void* getWindowAtPosition(juce::Point<float> position, juce::String &owner, juce::String &title);
	void* findWindow(juce::String owner, juce::String title);
	void* getFocusedWindowInfo(juce::String &owner, juce::String &title);
	void* getFocusedWindow();
	bool windowInfoMatch(juce::String owner, juce::String title, juce::String otherOwner, juce::String otherTitle);
	juce::Point<int> getRelativeCoordinatesToFocusedWindow(juce::Point<int> screenPosition);
    
#if JUCE_MAC
    CGWindowID getWindowIdAtPosition(juce::Point<float> position);
    AXUIElementRef* getAXWindow(CGWindowID windowID);
    bool isAXWindowOnScreen(AXUIElementRef windowRef);
    AXUIElementRef* getFrontMostWindow();
    
    pid_t getPID(juce::String owner);
    AXUIElementRef* getAXWindow(pid_t owner, juce::String title);
#endif

#if JUCE_WIN32
	juce::String getWindowTitle(HWND window);
	juce::String getWindowOwner(HWND window);

	juce::String titleToCompareTo;
	juce::String ownerToCompareTo;
	HWND result;

	static BOOL CALLBACK EnumWindowsProc(_In_ HWND hwnd, _In_ LPARAM lParam);
#endif

	juce_DeclareSingleton_SingleThreaded_Minimal(WindowHelper)
};

#endif 
