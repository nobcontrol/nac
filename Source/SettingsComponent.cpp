/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 4.3.1

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library - "Jules' Utility Class Extensions"
  Copyright (c) 2015 - ROLI Ltd.

  ==============================================================================
*/

//[Headers] You can add your own extra header files here...
/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "LowLevelKeyboardListener.h"
#include "WindowHelper.h"
//[/Headers]

#include "SettingsComponent.h"

//[MiscUserDefs] You can add your own user definitions and misc code here...
//[/MiscUserDefs]

//==============================================================================
SettingsComponent::SettingsComponent ()
{
    //[Constructor_pre] You can add your own custom stuff here..
    //[/Constructor_pre]

    addAndMakeVisible (sensitivitySlider = new Slider ("sensitivity slider"));
    sensitivitySlider->setTooltip (TRANS("Set nOb\'s sensitivity."));
    sensitivitySlider->setRange (1, 9, 1);
    sensitivitySlider->setSliderStyle (Slider::LinearBar);
    sensitivitySlider->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    sensitivitySlider->addListener (this);

    addAndMakeVisible (sensitivityLabel = new Label ("sensitivity label",
                                                     TRANS("Sensitivity")));
    sensitivityLabel->setFont (Font (15.00f, Font::plain));
    sensitivityLabel->setJustificationType (Justification::centredLeft);
    sensitivityLabel->setEditable (false, false, false);
    sensitivityLabel->setColour (TextEditor::textColourId, Colours::black);
    sensitivityLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (controlModeLabel = new Label ("control mode label",
                                                     TRANS("Control mode")));
    controlModeLabel->setFont (Font (15.00f, Font::plain));
    controlModeLabel->setJustificationType (Justification::centredLeft);
    controlModeLabel->setEditable (false, false, false);
    controlModeLabel->setColour (TextEditor::textColourId, Colours::black);
    controlModeLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (controlModeComboBox = new ComboBox ("control mode combo mox"));
    controlModeComboBox->setTooltip (TRANS("Set nOb\'s current control mode."));
    controlModeComboBox->setEditableText (false);
    controlModeComboBox->setJustificationType (Justification::centredRight);
    controlModeComboBox->setTextWhenNothingSelected (String());
    controlModeComboBox->setTextWhenNoChoicesAvailable (TRANS("(no choices)"));
    controlModeComboBox->addItem (TRANS("cursor controller"), 1);
    controlModeComboBox->addItem (TRANS("scroll wheel controller"), 2);
    controlModeComboBox->addItem (TRANS("master volume controller"), 3);
    controlModeComboBox->addListener (this);

    addAndMakeVisible (primaryMouseButtonLabel = new Label ("primary mouse button label",
                                                            TRANS("Primary mouse button")));
    primaryMouseButtonLabel->setFont (Font (15.00f, Font::plain));
    primaryMouseButtonLabel->setJustificationType (Justification::centredLeft);
    primaryMouseButtonLabel->setEditable (false, false, false);
    primaryMouseButtonLabel->setColour (TextEditor::textColourId, Colours::black);
    primaryMouseButtonLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (primaryMouseButtonComboBox = new ComboBox ("primary mouse button combo box"));
    primaryMouseButtonComboBox->setTooltip (TRANS("Set the primary mouse button for dragging elements. Useful for left handed configurations."));
    primaryMouseButtonComboBox->setEditableText (false);
    primaryMouseButtonComboBox->setJustificationType (Justification::centredRight);
    primaryMouseButtonComboBox->setTextWhenNothingSelected (String());
    primaryMouseButtonComboBox->setTextWhenNoChoicesAvailable (TRANS("(no choices)"));
    primaryMouseButtonComboBox->addItem (TRANS("left button"), 1);
    primaryMouseButtonComboBox->addItem (TRANS("right button"), 2);
    primaryMouseButtonComboBox->addListener (this);

    addAndMakeVisible (scrollwheelBehaviourLabel = new Label ("scrollwheel behaviour label",
                                                              TRANS("Scrolling behaviour")));
    scrollwheelBehaviourLabel->setFont (Font (15.00f, Font::plain));
    scrollwheelBehaviourLabel->setJustificationType (Justification::centredLeft);
    scrollwheelBehaviourLabel->setEditable (false, false, false);
    scrollwheelBehaviourLabel->setColour (TextEditor::textColourId, Colours::black);
    scrollwheelBehaviourLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (scrollwheelBehaviourComboBox = new ComboBox ("scrollwheel behaviour combo box"));
    scrollwheelBehaviourComboBox->setTooltip (TRANS("Set the bahaviour of the scroll wheel controlling mode. "));
    scrollwheelBehaviourComboBox->setEditableText (false);
    scrollwheelBehaviourComboBox->setJustificationType (Justification::centredRight);
    scrollwheelBehaviourComboBox->setTextWhenNothingSelected (String());
    scrollwheelBehaviourComboBox->setTextWhenNoChoicesAvailable (TRANS("(no choices)"));
    scrollwheelBehaviourComboBox->addItem (TRANS("classic"), 1);
    scrollwheelBehaviourComboBox->addItem (TRANS("natural"), 2);
    scrollwheelBehaviourComboBox->addListener (this);

    addAndMakeVisible (quickAssignmentsViewport = new Viewport ("quick assignments viewport"));
    quickAssignmentsViewport->setScrollBarsShown (true, false);
    quickAssignmentsViewport->setViewedComponent (new QuickAssignmentsPanel());

    addAndMakeVisible (quickAssignmentsLabel = new Label ("quick assignments label",
                                                          TRANS("Quick assignments")));
    quickAssignmentsLabel->setFont (Font (15.00f, Font::plain));
    quickAssignmentsLabel->setJustificationType (Justification::centredLeft);
    quickAssignmentsLabel->setEditable (false, false, false);
    quickAssignmentsLabel->setColour (TextEditor::textColourId, Colours::black);
    quickAssignmentsLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (advancedButton = new TextButton ("advanced button"));
    advancedButton->setButtonText (TRANS("..."));
    advancedButton->addListener (this);

    cachedImage_nAc_logo_png_1 = ImageCache::getFromMemory (nAc_logo_png, nAc_logo_pngSize);

    //[UserPreSize]
    //[/UserPreSize]

    setSize (600, 400);


    //[Constructor] You can add your own custom stuff here..
    controllerManager = nullptr;
    sensitivitySlider->setValue(5.0);
	controlModeComboBox->setSelectedItemIndex(0);
	primaryMouseButtonComboBox->setSelectedItemIndex(0);
	scrollwheelBehaviourComboBox->setSelectedItemIndex(0);

	QuickAssignmentsPanel* quickAssignmentsPanel = (QuickAssignmentsPanel*)quickAssignmentsViewport->getViewedComponent();
	LowLevelKeyboardListener::getInstance()->setQuickAssignmentsPanel(quickAssignmentsPanel);

	aboutWindow = nullptr;

	sharedAssignmentsWindow = new SharedAssignmentsWindow();
    //[/Constructor]
}

SettingsComponent::~SettingsComponent()
{
    //[Destructor_pre]. You can add your own custom destruction code here..
    //[/Destructor_pre]

    sensitivitySlider = nullptr;
    sensitivityLabel = nullptr;
    controlModeLabel = nullptr;
    controlModeComboBox = nullptr;
    primaryMouseButtonLabel = nullptr;
    primaryMouseButtonComboBox = nullptr;
    scrollwheelBehaviourLabel = nullptr;
    scrollwheelBehaviourComboBox = nullptr;
    quickAssignmentsViewport = nullptr;
    quickAssignmentsLabel = nullptr;
    advancedButton = nullptr;


    //[Destructor]. You can add your own custom destruction code here..
	aboutWindow = nullptr;
	sharedAssignmentsWindow = nullptr;
    //[/Destructor]
}

//==============================================================================
void SettingsComponent::paint (Graphics& g)
{
    //[UserPrePaint] Add your own custom painting code here..
    //[/UserPrePaint]

    g.fillAll (Colour (0xffc4c4c4));

    g.setColour (Colours::black);
    g.drawImage (cachedImage_nAc_logo_png_1,
                 100, -15, 100, 100,
                 0, 0, cachedImage_nAc_logo_png_1.getWidth(), cachedImage_nAc_logo_png_1.getHeight());

    //[UserPaint] Add your own custom painting code here..
//    g.setColour (Colour(0xffbdbdbd));
//    g.fillRect(quickAssignmentsViewport->getX(), quickAssignmentsViewport->getY(), quickAssignmentsViewport->getWidth(), quickAssignmentsViewport->getHeight());
    //[/UserPaint]
}

void SettingsComponent::resized()
{
    //[UserPreResize] Add your own custom resize code here..
    //[/UserPreResize]

    sensitivitySlider->setBounds (83, 65, 205, 24);
    sensitivityLabel->setBounds (9, 65, 80, 24);
    controlModeLabel->setBounds (9, 98, 96, 24);
    controlModeComboBox->setBounds (105, 98, 183, 24);
    primaryMouseButtonLabel->setBounds (9, 131, 152, 24);
    primaryMouseButtonComboBox->setBounds (163, 131, 125, 24);
    scrollwheelBehaviourLabel->setBounds (9, 164, 152, 24);
    scrollwheelBehaviourComboBox->setBounds (163, 164, 125, 24);
    quickAssignmentsViewport->setBounds (16, 228, 272, 36);
    quickAssignmentsLabel->setBounds (9, 197, 127, 24);
    advancedButton->setBounds (144, 201, 23, 16);
    //[UserResized] Add your own custom resize handling here..

    //Readjust quickassignments panel

	int extraSpaceBecauseOfNoneNativeWindow = 0;
#if JUCE_WIN32
	extraSpaceBecauseOfNoneNativeWindow = 28;
#endif

    int y = quickAssignmentsViewport->getY();
    int viewPortWidth = quickAssignmentsViewport->getWidth();
    quickAssignmentsViewport->setSize(viewPortWidth, getParentHeight()-y-14-extraSpaceBecauseOfNoneNativeWindow);

 // Component* viewedComponent = quickAssignmentsViewport->getViewedComponent();
 // viewedComponent->setSize(viewedComponent->getWidth(), viewedComponent->getHeight());
    //[/UserResized]
}

void SettingsComponent::sliderValueChanged (Slider* sliderThatWasMoved)
{
    //[UsersliderValueChanged_Pre]
    //[/UsersliderValueChanged_Pre]

    if (sliderThatWasMoved == sensitivitySlider)
    {
        //[UserSliderCode_sensitivitySlider] -- add your slider handling code here..
        if (controllerManager)
            controllerManager->setSensitivity((int)sensitivitySlider->getValue());
        //[/UserSliderCode_sensitivitySlider]
    }

    //[UsersliderValueChanged_Post]
    //[/UsersliderValueChanged_Post]
}

void SettingsComponent::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    //[UsercomboBoxChanged_Pre]
    //[/UsercomboBoxChanged_Pre]

    if (comboBoxThatHasChanged == controlModeComboBox)
    {
        //[UserComboBoxCode_controlModeComboBox] -- add your combo box handling code here..
		int selectionIndex = controlModeComboBox->getSelectedItemIndex();
		controllerManager->setControlMode(selectionIndex);
        //[/UserComboBoxCode_controlModeComboBox]
    }
    else if (comboBoxThatHasChanged == primaryMouseButtonComboBox)
    {
        //[UserComboBoxCode_primaryMouseButtonComboBox] -- add your combo box handling code here..
		int selectionIndex = primaryMouseButtonComboBox->getSelectedItemIndex();
		controllerManager->setPrimaryMouseButton(selectionIndex);
        //[/UserComboBoxCode_primaryMouseButtonComboBox]
    }
    else if (comboBoxThatHasChanged == scrollwheelBehaviourComboBox)
    {
        //[UserComboBoxCode_scrollwheelBehaviourComboBox] -- add your combo box handling code here..
		int selectionIndex = scrollwheelBehaviourComboBox->getSelectedItemIndex();
		controllerManager->setScrollWheelBehaviour(selectionIndex);
        //[/UserComboBoxCode_scrollwheelBehaviourComboBox]
    }

    //[UsercomboBoxChanged_Post]
    //[/UsercomboBoxChanged_Post]
}

void SettingsComponent::buttonClicked (juce::Button* buttonThatWasClicked)
{
    //[UserbuttonClicked_Pre]
    //[/UserbuttonClicked_Pre]

    if (buttonThatWasClicked == advancedButton)
    {
        //[UserButtonCode_advancedButton] -- add your button handler code here..
		if (sharedAssignmentsWindow)
		{
			sharedAssignmentsWindow->centreAroundComponent(NULL, sharedAssignmentsWindow->getWidth(), sharedAssignmentsWindow->getHeight());
			sharedAssignmentsWindow->setVisible(true);
            sharedAssignmentsWindow->enterModalState();
		}
        //[/UserButtonCode_advancedButton]
    }

    //[UserbuttonClicked_Post]
    //[/UserbuttonClicked_Post]
}

void SettingsComponent::mouseDoubleClick (const MouseEvent& e)
{
    //[UserCode_mouseDoubleClick] -- Add your code here...
	int x = e.getMouseDownX();
	int y = e.getMouseDownY();

	if (x > 100 && x<200 && y>-15 && y < 85)
	{
		if (aboutWindow)
		{
			AboutWindow* currentwindow = aboutWindow;
			aboutWindow.release();
			delete currentwindow;
		}

		aboutWindow = new AboutWindow(this);
		juce::String message = juce::String("\n\nnOb Assignment center a.k.a. nAc\n")
			+ juce::String("Version ") + juce::String(ProjectInfo::versionString) // + juce::String(" Beta05")
			+ juce::String("\n\nCreated by Dio Marinos for NOB CONTROL.\n")
			+ juce::String("Copyright 2016-2019. All rights reserved.");

		aboutWindow->setMessage(message);
		aboutWindow->enterModalState();
	}

    //[/UserCode_mouseDoubleClick]
}



//[MiscUserCode] You can add your own definitions of your custom methods or any other code here...
void SettingsComponent::setControllerManager(ControllerManager* theControllerManager)
{
    controllerManager = theControllerManager;

	((QuickAssignmentsPanel*)(quickAssignmentsViewport->getViewedComponent()))->setControllerManager(controllerManager);

    if (controllerManager)
        controllerManager->setSensitivity((int)sensitivitySlider->getValue());
}

QuickAssignmentsPanel* SettingsComponent::getQuickAssignmentsPanel()
{
    return (QuickAssignmentsPanel*)quickAssignmentsViewport->getViewedComponent();
}

void SettingsComponent::saveSettings()
{
#if JUCE_WIN32
	File settingsFile = juce::File::getCurrentWorkingDirectory().getChildFile("Data/Settings.xml");
#endif

#if JUCE_MAC
	File settingsFile = juce::File::getSpecialLocation(juce::File::currentApplicationFile).getChildFile("Data/Settings.xml");
#endif

	DBG(juce::String("Settings file location: ") + settingsFile.getFullPathName());

	if (!settingsFile.exists())
	{
		DBG("Settings file does not exist. Trying to create it...");
		Result res = settingsFile.create();
		if (res.failed())
		{
			DBG("Settings file creation failed with error message: " + res.getErrorMessage());
		}
		else
		{
			DBG("Settings file sucessfully created.");
		}
	}
	else
	{
		DBG("Settings file already exists.");
	}

	ScopedPointer<XmlElement> settingsElement = new XmlElement("default_settings");

	settingsElement->setAttribute("sensitivity",(int)sensitivitySlider->getValue());
	settingsElement->setAttribute("controlmode", controlModeComboBox->getSelectedItemIndex());
	settingsElement->setAttribute("primaremousebutton", primaryMouseButtonComboBox->getSelectedItemIndex());
	settingsElement->setAttribute("scrollwheelbehaviour", scrollwheelBehaviourComboBox->getSelectedItemIndex());

	QuickAssignmentsPanel* quickAssignmentsPanel = (QuickAssignmentsPanel*)quickAssignmentsViewport->getViewedComponent();
	int numberOfQuickAssignments = quickAssignmentsPanel->getNumberOfQuickAssignments();

	settingsElement->setAttribute("numberofquickassignments", numberOfQuickAssignments);

	for (int i = 0; i < numberOfQuickAssignments; i++)
	{
		XmlElement* qaElement = new XmlElement(String("quickassignment_") + String(i));

		KeypressExtended assignShortcut = quickAssignmentsPanel->getAssignShortcut(i);
		if (assignShortcut.keyPress.isValid())
		{
			qaElement->setAttribute("assignkey", assignShortcut.keyPress.getKeyCode());
			qaElement->setAttribute("assignkeymodifiers", assignShortcut.keyPress.getModifiers().getRawFlags());
			qaElement->setAttribute("assignknobmodifier", assignShortcut.knobModifier?1:0);
		}

		KeypressExtended triggerShortcut = quickAssignmentsPanel->getTriggerShortcut(i);
		if (assignShortcut.keyPress.isValid())
		{
			qaElement->setAttribute("triggerkey", triggerShortcut.keyPress.getKeyCode());
			qaElement->setAttribute("triggerkeymodifiers", triggerShortcut.keyPress.getModifiers().getRawFlags());
			qaElement->setAttribute("triggerknobmodifier", triggerShortcut.knobModifier ? 1 : 0);
		}

        Assignment* assignment = quickAssignmentsPanel->getAssignment(i);

        qaElement->setAttribute("controllertype", assignment->controlMode);
        qaElement->setAttribute("owner", assignment->assignmentPosition.owner);
        qaElement->setAttribute("title", assignment->assignmentPosition.title);
        qaElement->setAttribute("x", assignment->assignmentPosition.relativePosition.x);
        qaElement->setAttribute("y", assignment->assignmentPosition.relativePosition.y);
        qaElement->setAttribute("operationmode", assignment->currentOperationMode);
        qaElement->setAttribute("primarymousebutton", assignment->primaryMouseButton);
        qaElement->setAttribute("scrollbehaviour", assignment->scrollWheelMode);
        qaElement->setAttribute("clicksperpixel", assignment->clicksPerPixel);
        qaElement->setAttribute("direction", assignment->currentDirectionState);
        qaElement->setAttribute("radius", assignment->radius);
        qaElement->setAttribute("phase", assignment->phase);

		settingsElement->addChildElement(qaElement);
	}

	String sharedAssignmentsText = sharedAssignmentsWindow->getEditorsText();
	StringArray lines;
	lines.addLines(sharedAssignmentsText);
	lines.removeEmptyStrings();

	int numberOfWildcards = lines.size();
	settingsElement->setAttribute("numberofwildcards", numberOfWildcards);

	for (int i = 0; i < numberOfWildcards; i++)
	{
		settingsElement->setAttribute(String("wildcard_") + String(i), lines[i]);
	}

	DBG("Trying to write to settings file...");
	bool writeResult = settingsElement->writeToFile(settingsFile, "");
	
	if (writeResult)
	{
		DBG("Settings file successfully written.");
	}
	else
	{
		DBG("Unable to write to settings file");
	}
}

void SettingsComponent::loadSettings()
{
#if JUCE_WIN32
	File settingsFile = juce::File::getCurrentWorkingDirectory().getChildFile("Data/Settings.xml");
#endif

#if JUCE_MAC
	File settingsFile = juce::File::getSpecialLocation(juce::File::currentApplicationFile).getChildFile("Data/Settings.xml");
#endif

	DBG(juce::String("Settings file location: ") + settingsFile.getFullPathName());

	if (!settingsFile.exists())
	{
		DBG("Settings file does not exist. Nothing to load.");
		return;
	}

	QuickAssignmentsPanel* quickAssignmentsPanel = (QuickAssignmentsPanel*)quickAssignmentsViewport->getViewedComponent();

	XmlDocument settingsDocument(settingsFile);

	ScopedPointer<XmlElement> settingsElement = settingsDocument.getDocumentElement();

	if (settingsElement)
	{
		sensitivitySlider->setValue((double)settingsElement->getIntAttribute("sensitivity"));
		controlModeComboBox->setSelectedItemIndex(settingsElement->getIntAttribute("controlmode"));
		primaryMouseButtonComboBox->setSelectedItemIndex(settingsElement->getIntAttribute("primaremousebutton"));
		scrollwheelBehaviourComboBox->setSelectedItemIndex(settingsElement->getIntAttribute("scrollwheelbehaviour"));

		int numberOfQuickAssignments = settingsElement->getIntAttribute("numberofquickassignments");
		for (int i = 0; i < numberOfQuickAssignments; i++)
		{
			XmlElement* e = settingsElement->getChildByName(String("quickassignment_") + String(i));

			quickAssignmentsPanel->addEmptyShortcutEditor();

			if (e->hasAttribute("assignkey"))
			{
				int keycode = e->getIntAttribute("assignkey");
				int modifiers = e->getIntAttribute("assignkeymodifiers");
				bool knobModifier = (e->getIntAttribute("assignknobmodifier") == 1) ? true : false;

				KeypressExtended shortcut;
				shortcut.knobModifier = knobModifier;
				shortcut.keyPress = KeyPress(keycode, ModifierKeys(modifiers), 0);

				quickAssignmentsPanel->setAssignShortcut(i, shortcut);
			}

			if (e->hasAttribute("triggerkey"))
			{
				int keycode = e->getIntAttribute("triggerkey");
				int modifiers = e->getIntAttribute("triggerkeymodifiers");
				bool knobModifier = (e->getIntAttribute("triggerknobmodifier") == 1) ? true : false;

				KeypressExtended shortcut;
				shortcut.knobModifier = knobModifier;
				shortcut.keyPress = KeyPress(keycode, ModifierKeys(modifiers), 0);

				quickAssignmentsPanel->setTriggerShortcut(i, shortcut);
			}

            Assignment* assignment = quickAssignmentsPanel->getAssignment(i);

            assignment->controlMode = e->getIntAttribute("controllertype");
            assignment->assignmentPosition.owner = e->getStringAttribute("owner");
            assignment->assignmentPosition.title = e->getStringAttribute("title");
            assignment->assignmentPosition.relativePosition.x = e->getIntAttribute("x");
            assignment->assignmentPosition.relativePosition.y = e->getIntAttribute("y");
            assignment->currentOperationMode = e->getIntAttribute("operationmode");
            assignment->primaryMouseButton = e->getIntAttribute("primarymousebutton");
            assignment->scrollWheelMode = e->getIntAttribute("scrollbevaviour");
            assignment->clicksPerPixel = e->getIntAttribute("clicksperpixel");
            assignment->currentDirectionState = e->getIntAttribute("direction");
            assignment->radius = e->getIntAttribute("radius");
            assignment->phase = (float)e->getDoubleAttribute("phase");

			quickAssignmentsPanel->updateTooltipsOfShortcutEditor(i);
		}

		String sharedAssignmentsText;
		int numberOfWildcards = settingsElement->getIntAttribute("numberofwildcards");
		StringArray lines;
		for (int i = 0; i < numberOfWildcards; i++)
		{
			String line = settingsElement->getStringAttribute(String("wildcard_") + String(i));
			lines.add(line);
		}
		sharedAssignmentsText = lines.joinIntoString("\n");
		sharedAssignmentsWindow->setEditorsText(sharedAssignmentsText);

		sharedAssignmentsWindow->updateWildcards();
	}
}

void SharedAssignmentsWindow::updateWildcards()
{
    String sharedAssignmentsText = getEditorsText();
    StringArray lines;
    lines.addLines(sharedAssignmentsText);
    lines.removeEmptyStrings();

    String newText = lines.joinIntoString("\n");
    setEditorsText(newText);

    WindowHelper::getInstance()->setWildcards(lines);
}
//[/MiscUserCode]


//==============================================================================
#if 0
/*  -- Projucer information section --

    This is where the Projucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="SettingsComponent" componentName=""
                 parentClasses="public juce::Component" constructorParams="" variableInitialisers=""
                 snapPixels="8" snapActive="1" snapShown="1" overlayOpacity="0.330"
                 fixedSize="0" initialWidth="600" initialHeight="400">
  <METHODS>
    <METHOD name="mouseDoubleClick (const MouseEvent&amp; e)"/>
  </METHODS>
  <BACKGROUND backgroundColour="ffc4c4c4">
    <IMAGE pos="100 -15 100 100" resource="nAc_logo_png" opacity="1" mode="0"/>
  </BACKGROUND>
  <SLIDER name="sensitivity slider" id="3d16ffede0db41f6" memberName="sensitivitySlider"
          virtualName="" explicitFocusOrder="0" pos="83 65 205 24" tooltip="Set nOb's sensitivity."
          min="1" max="9" int="1" style="LinearBar" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="1"
          needsCallback="1"/>
  <LABEL name="sensitivity label" id="b37430527fe70d2c" memberName="sensitivityLabel"
         virtualName="" explicitFocusOrder="0" pos="9 65 80 24" edTextCol="ff000000"
         edBkgCol="0" labelText="Sensitivity" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <LABEL name="control mode label" id="e788df14bed0f4e8" memberName="controlModeLabel"
         virtualName="" explicitFocusOrder="0" pos="9 98 96 24" edTextCol="ff000000"
         edBkgCol="0" labelText="Control mode" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <COMBOBOX name="control mode combo mox" id="3b5e8ccdb667803b" memberName="controlModeComboBox"
            virtualName="" explicitFocusOrder="0" pos="105 98 183 24" tooltip="Set nOb's current control mode."
            editable="0" layout="34" items="cursor controller&#10;scroll wheel controller&#10;master volume controller"
            textWhenNonSelected="" textWhenNoItems="(no choices)"/>
  <LABEL name="primary mouse button label" id="6bd9c0745c7136e2" memberName="primaryMouseButtonLabel"
         virtualName="" explicitFocusOrder="0" pos="9 131 152 24" edTextCol="ff000000"
         edBkgCol="0" labelText="Primary mouse button" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <COMBOBOX name="primary mouse button combo box" id="62bb2d3769e76361" memberName="primaryMouseButtonComboBox"
            virtualName="" explicitFocusOrder="0" pos="163 131 125 24" tooltip="Set the primary mouse button for dragging elements. Useful for left handed configurations."
            editable="0" layout="34" items="left button&#10;right button"
            textWhenNonSelected="" textWhenNoItems="(no choices)"/>
  <LABEL name="scrollwheel behaviour label" id="3917840b0bb586c5" memberName="scrollwheelBehaviourLabel"
         virtualName="" explicitFocusOrder="0" pos="9 164 152 24" edTextCol="ff000000"
         edBkgCol="0" labelText="Scrolling behaviour" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <COMBOBOX name="scrollwheel behaviour combo box" id="7cb8b7dc1d901c36"
            memberName="scrollwheelBehaviourComboBox" virtualName="" explicitFocusOrder="0"
            pos="163 164 125 24" tooltip="Set the bahaviour of the scroll wheel controlling mode. "
            editable="0" layout="34" items="classic&#10;natural" textWhenNonSelected=""
            textWhenNoItems="(no choices)"/>
  <VIEWPORT name="quick assignments viewport" id="d416bee959ca8d68" memberName="quickAssignmentsViewport"
            virtualName="" explicitFocusOrder="0" pos="16 228 272 36" vscroll="1"
            hscroll="0" scrollbarThickness="18" contentType="1" jucerFile="QuickAssignmentsPanel.cpp"
            contentClass="" constructorParams=""/>
  <LABEL name="quick assignments label" id="776bc3fdd7e7c0c9" memberName="quickAssignmentsLabel"
         virtualName="" explicitFocusOrder="0" pos="9 197 127 24" edTextCol="ff000000"
         edBkgCol="0" labelText="Quick assignments" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <TEXTBUTTON name="advanced button" id="529c63e5d5ed876f" memberName="advancedButton"
              virtualName="" explicitFocusOrder="0" pos="144 201 23 16" buttonText="..."
              connectedEdges="0" needsCallback="1" radioGroupId="0"/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif

//==============================================================================
// Binary resources - be careful not to edit any of these sections!

// JUCER_RESOURCE: nAc_logo_png, 8229, "../nAc Icon Projects/nAc_Logo.png"
static const unsigned char resource_SettingsComponent_nAc_logo_png[] = { 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,168,0,0,0,168,8,6,0,0,0,116,75,165,180,0,0,0,4,115,66,73,84,8,8,8,8,124,8,100,
136,0,0,0,9,112,72,89,115,0,0,2,69,0,0,2,69,1,172,229,243,124,0,0,0,25,116,69,88,116,83,111,102,116,119,97,114,101,0,119,119,119,46,105,110,107,115,99,97,112,101,46,111,114,103,155,238,60,26,0,0,31,162,
73,68,65,84,120,156,237,157,123,152,20,213,157,247,127,117,234,214,213,213,205,24,241,2,217,39,138,92,34,27,37,134,64,136,32,232,40,136,12,202,48,192,118,68,12,26,147,119,55,137,9,172,175,171,114,85,58,
196,97,64,7,16,39,97,179,228,89,137,190,217,32,59,106,140,220,94,185,132,9,130,184,236,195,198,133,229,34,134,139,184,201,98,140,23,152,238,174,123,157,247,15,235,204,123,230,76,85,77,247,244,12,51,226,
249,60,79,61,221,64,83,117,234,119,190,245,59,191,115,206,239,156,2,224,112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,
56,28,14,135,195,225,112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,
56,28,14,135,195,225,112,56,28,78,151,35,116,119,1,62,205,100,50,25,241,236,217,179,159,247,60,47,45,138,162,38,8,194,231,0,0,48,198,31,249,190,95,64,8,229,42,42,42,254,212,216,216,232,117,119,89,63,173,
112,129,22,73,101,101,229,69,201,100,114,180,32,8,163,17,66,215,8,130,48,8,33,116,149,32,8,138,32,8,32,8,173,77,137,49,38,135,237,251,254,9,140,241,219,158,231,29,242,60,111,183,231,121,187,183,111,223,
126,182,155,110,229,83,5,23,104,12,99,199,142,253,162,170,170,51,36,73,170,150,36,233,58,81,20,17,66,8,16,66,32,138,34,32,132,64,16,4,64,8,1,0,180,136,20,99,12,0,0,190,239,3,198,24,124,223,7,207,243,192,
247,125,240,125,31,92,215,245,60,207,251,79,215,117,95,113,28,231,87,219,182,109,123,187,219,110,178,135,195,5,202,80,89,89,153,210,117,253,62,81,20,239,145,101,121,184,36,73,64,31,162,40,182,136,147,
28,113,30,148,136,146,136,212,243,60,112,93,183,213,225,56,206,191,59,142,243,156,231,121,107,183,110,221,154,239,166,91,239,145,112,129,6,84,85,85,93,42,138,226,15,100,89,254,161,162,40,189,101,89,6,
89,150,65,146,164,150,207,48,129,22,235,65,195,4,234,56,78,203,103,112,156,181,109,251,89,199,113,150,190,250,234,171,255,211,109,198,232,65,124,230,5,58,114,228,72,173,119,239,222,143,168,170,58,71,81,
20,77,81,20,144,101,25,200,39,45,212,48,239,89,138,7,101,69,74,9,19,28,199,1,219,182,193,182,109,176,44,171,96,89,214,210,124,62,255,100,83,83,147,217,77,166,233,17,124,166,5,122,199,29,119,76,82,20,101,
149,170,170,87,41,138,2,170,170,130,170,170,161,226,36,2,45,70,156,132,56,145,210,94,148,22,168,101,89,96,89,22,249,254,174,109,219,11,55,110,220,248,220,121,54,77,143,225,51,41,208,170,170,170,94,170,
170,254,76,85,213,187,18,137,4,40,138,2,137,68,162,69,156,172,64,17,66,32,73,82,164,48,227,60,40,192,39,77,61,249,164,58,74,45,222,212,182,237,22,145,18,129,146,195,52,77,176,44,235,69,0,248,95,47,191,
252,242,199,231,213,80,61,128,207,156,64,171,171,171,135,43,138,178,78,85,213,129,137,68,2,200,17,230,61,105,175,41,138,98,139,16,195,4,26,6,17,40,237,73,201,119,34,78,34,86,215,117,91,154,120,219,182,
193,52,77,34,78,48,77,19,12,195,120,199,178,172,187,54,110,220,184,247,188,25,171,7,240,153,18,104,117,117,245,223,106,154,246,147,68,34,161,104,154,6,154,166,181,17,39,233,20,145,120,147,237,16,177,162,
140,18,39,129,22,41,249,12,235,56,209,113,41,43,82,211,52,161,80,40,128,105,154,118,161,80,184,127,195,134,13,255,220,133,102,234,81,136,221,93,128,243,197,228,201,147,231,232,186,190,74,215,117,81,215,
117,208,117,29,146,201,36,36,147,201,54,66,141,139,61,137,72,227,98,79,26,246,247,116,207,159,29,83,101,31,138,144,152,87,20,4,161,122,208,160,65,218,209,163,71,183,159,7,179,117,59,159,5,129,162,154,
154,154,159,164,82,169,121,68,152,228,160,189,40,137,61,137,23,37,49,39,221,180,119,22,172,88,195,4,25,37,210,160,28,163,7,12,24,112,217,209,163,71,255,47,0,224,78,43,88,15,228,130,23,232,180,105,211,
158,210,117,253,135,169,84,42,210,107,210,195,74,172,183,12,131,154,198,108,211,67,167,15,182,121,15,59,31,235,93,219,59,168,223,127,109,224,192,129,125,142,28,57,178,177,235,172,215,253,92,208,2,157,
58,117,234,34,93,215,231,208,94,147,21,39,17,38,221,75,15,19,82,152,0,233,248,145,61,88,161,210,7,64,91,177,210,94,154,14,3,104,225,178,161,133,32,8,195,7,14,28,232,29,57,114,100,87,23,155,178,219,184,
96,5,58,101,202,148,191,213,117,125,121,156,56,73,172,73,123,77,26,182,199,77,58,50,108,167,134,157,190,164,255,61,204,171,210,66,5,104,45,86,214,155,178,66,165,69,26,156,231,230,65,131,6,253,247,145,
35,71,126,127,222,140,123,30,57,47,189,248,177,99,199,246,22,69,177,47,0,244,21,4,161,15,0,92,6,0,231,48,198,103,4,65,120,15,33,244,39,195,48,206,52,53,53,185,157,113,189,154,154,154,17,186,174,191,150,
74,165,20,90,156,172,215,164,155,78,154,168,158,54,157,244,225,121,30,96,140,207,120,158,247,150,239,251,39,0,224,207,0,64,230,209,117,65,16,46,21,4,97,128,40,138,131,5,65,184,156,141,41,227,70,8,8,180,
176,233,129,124,210,171,207,231,243,144,207,231,33,151,203,217,249,124,126,212,203,47,191,188,191,51,236,87,89,89,41,105,154,214,199,182,237,190,162,40,246,9,234,172,23,0,252,25,99,124,6,0,254,199,182,
237,51,77,77,77,127,233,140,235,197,209,37,2,29,57,114,164,150,78,167,171,101,89,190,3,33,116,163,40,138,87,132,85,2,61,144,237,251,126,179,231,121,175,187,174,187,195,117,221,245,219,183,111,63,221,145,
107,103,50,153,10,73,146,254,35,149,74,245,167,227,78,77,211,90,122,233,116,207,60,74,20,97,227,148,158,231,57,174,235,110,245,60,239,69,219,182,119,61,250,232,163,199,139,41,83,93,93,221,64,0,184,81,
150,229,191,17,69,241,86,233,19,90,117,196,226,188,56,61,251,68,6,240,13,195,104,17,105,46,151,131,124,62,255,135,143,62,250,104,216,150,45,91,206,117,196,110,227,199,143,255,130,40,138,119,138,162,56,
78,20,197,81,8,161,52,155,99,64,202,67,61,188,167,61,207,219,229,121,222,38,89,150,95,217,176,97,67,161,35,215,142,163,83,5,122,235,173,183,14,82,20,229,33,89,150,239,146,36,41,45,138,98,27,49,208,21,
64,223,44,147,72,129,93,215,253,157,231,121,203,55,111,222,188,9,74,232,169,222,121,231,157,207,167,82,169,59,83,169,20,164,82,169,54,205,122,148,56,195,202,225,121,30,73,232,120,207,113,156,6,195,48,
254,41,155,205,150,229,53,150,44,89,114,169,36,73,223,21,69,113,150,44,203,151,145,16,131,157,235,143,42,27,17,105,48,120,15,133,66,1,114,185,28,57,126,181,126,253,250,187,75,40,142,112,219,109,183,77,
84,85,245,31,36,73,186,73,20,69,20,86,14,186,44,108,10,33,177,147,235,186,57,215,117,215,25,134,81,191,99,199,142,99,229,216,168,85,1,59,227,36,183,221,118,91,95,89,150,151,170,170,250,77,89,150,81,49,
227,136,0,109,111,150,18,4,61,245,119,192,243,188,7,54,109,218,180,179,189,114,76,155,54,173,38,157,78,255,154,136,147,109,218,233,120,147,53,122,196,28,121,222,178,172,90,203,178,86,102,179,217,78,77,
218,88,177,98,133,102,219,246,63,40,138,50,79,150,229,100,148,189,8,180,157,200,148,40,221,212,19,145,54,55,55,87,191,248,226,139,27,218,187,254,132,9,19,42,21,69,121,74,85,213,235,216,250,138,114,42,
97,161,79,72,78,129,111,219,246,175,76,211,156,179,109,219,182,63,149,107,167,178,5,122,251,237,183,127,63,145,72,44,85,20,165,23,61,150,72,223,52,61,158,24,230,65,233,230,148,78,158,32,73,19,65,236,181,
14,33,116,127,212,124,116,38,147,209,18,137,196,161,84,42,117,21,235,61,137,56,73,89,194,196,73,27,57,152,27,223,142,49,190,111,206,156,57,255,93,174,141,226,200,102,179,87,36,147,201,181,138,162,220,
194,38,168,68,137,148,148,149,110,234,41,47,122,252,221,119,223,189,54,42,11,170,166,166,230,34,207,243,126,154,72,36,102,144,186,98,39,39,232,9,131,40,15,26,55,77,27,212,87,179,105,154,243,54,111,222,
188,26,202,24,171,237,112,47,190,166,166,230,162,171,175,190,250,87,201,100,242,161,100,50,169,38,147,201,86,227,140,228,72,36,18,45,77,44,25,16,167,167,22,163,210,219,232,39,58,72,214,24,130,49,190,107,
192,128,1,111,28,59,118,172,141,104,190,250,213,175,46,76,38,147,53,97,157,34,218,240,172,177,217,212,55,203,178,124,211,52,23,154,166,249,189,133,11,23,118,249,178,140,166,166,166,179,55,220,112,195,
47,125,223,119,1,160,82,160,10,24,50,172,212,166,199,79,223,75,240,192,95,156,76,38,173,3,7,14,180,25,122,154,60,121,242,215,37,73,218,174,235,250,24,77,211,90,236,196,142,15,211,245,69,234,138,212,87,
84,189,209,121,179,129,184,85,81,20,39,14,26,52,232,186,193,131,7,191,122,244,232,209,14,181,64,29,242,160,19,39,78,236,163,105,218,150,68,34,241,21,58,190,163,103,100,162,154,119,218,184,236,128,55,29,
255,81,79,34,157,213,3,150,101,89,142,227,204,124,225,133,23,26,201,185,102,206,156,121,89,34,145,56,169,235,122,146,157,37,98,123,236,4,86,156,193,220,183,101,89,214,204,133,11,23,54,66,55,176,120,241,
226,111,104,154,246,92,34,145,80,233,138,39,54,164,203,78,247,236,137,23,37,158,180,80,40,228,115,185,92,191,117,235,214,181,196,203,211,166,77,155,164,40,202,122,85,85,53,82,87,172,163,136,138,63,73,
93,17,162,154,249,176,28,130,32,12,57,156,207,231,39,108,221,186,245,221,82,109,82,178,7,29,63,126,252,23,210,233,244,158,100,50,57,88,215,245,150,88,143,22,69,152,135,36,130,101,99,29,90,200,236,247,
176,79,81,20,37,73,146,166,14,25,50,228,228,129,3,7,14,0,0,140,28,57,114,174,166,105,99,89,15,64,27,61,172,130,233,166,210,52,77,203,48,140,73,143,61,246,88,187,241,91,87,177,115,231,206,67,163,71,143,
254,55,0,248,134,32,8,18,61,38,10,16,158,160,18,54,42,2,0,10,66,200,216,191,127,127,19,0,192,55,191,249,205,187,146,201,228,243,154,166,169,196,83,210,163,27,108,157,69,181,100,180,128,139,169,43,202,
49,93,138,16,154,54,112,224,192,151,142,29,59,86,82,171,84,146,7,157,50,101,74,111,73,146,94,75,38,147,127,205,54,227,172,24,217,57,100,214,152,172,81,163,98,27,54,145,215,178,44,210,145,242,48,198,127,
147,203,229,182,93,114,201,37,239,36,18,137,222,108,243,20,22,119,146,107,208,79,188,97,24,190,97,24,211,23,45,90,212,45,158,147,37,155,205,214,36,147,201,23,52,77,19,89,239,70,223,7,253,144,49,89,79,
96,154,230,135,150,101,93,9,0,163,1,224,21,81,20,101,54,188,162,195,159,168,190,66,123,117,198,122,82,186,190,12,195,104,53,28,86,40,20,246,35,132,190,94,202,50,236,162,61,104,38,147,17,5,65,216,168,235,
250,215,104,207,25,151,112,209,78,102,78,155,217,18,246,55,236,248,32,249,115,240,100,35,85,85,39,85,84,84,40,137,68,226,54,18,235,178,134,143,107,218,73,232,96,154,230,194,69,139,22,253,83,177,182,232,
106,154,154,154,142,142,30,61,26,11,130,112,115,88,22,20,64,184,55,197,24,211,182,212,20,69,241,101,89,94,161,170,170,70,234,136,126,128,73,157,133,197,252,197,214,89,88,61,133,36,183,0,0,128,239,251,
159,55,12,227,212,91,111,189,245,102,177,182,144,138,253,161,231,121,143,166,82,169,74,54,85,141,30,252,102,135,113,138,205,0,98,59,2,97,6,34,198,83,20,165,101,122,17,99,172,35,132,254,55,17,45,251,112,
196,137,147,138,151,154,14,30,60,184,172,88,59,156,47,48,198,181,166,105,222,136,16,26,199,14,209,145,123,34,159,164,89,85,85,181,229,239,37,73,2,207,243,30,244,125,63,73,50,166,232,206,76,152,32,59,90,
103,24,227,208,58,163,135,19,41,111,255,109,0,88,91,172,29,138,242,160,83,166,76,185,78,215,245,127,209,117,29,197,77,29,182,151,112,81,12,81,185,147,196,200,140,23,5,69,81,100,118,120,43,108,48,158,110,
18,169,41,195,188,235,186,99,87,175,94,221,227,54,81,104,106,106,194,35,70,140,248,157,40,138,223,69,8,201,97,25,77,52,172,221,2,59,201,196,38,116,159,128,237,23,116,86,157,1,64,232,185,152,129,253,62,
23,95,124,241,210,83,167,78,249,197,156,187,24,129,10,67,135,14,109,212,117,189,95,84,103,40,170,57,165,11,72,47,121,160,147,38,200,191,199,25,156,13,1,232,0,157,13,232,227,102,99,200,32,55,21,35,101,
127,244,163,31,109,46,198,80,221,193,158,61,123,206,142,26,53,74,148,36,233,230,176,185,123,128,112,59,17,91,209,173,14,59,148,23,101,39,2,91,87,108,157,145,107,211,132,13,139,133,140,208,136,174,235,
54,28,59,118,172,168,105,209,118,155,248,105,211,166,221,145,72,36,70,179,29,144,184,97,16,250,38,233,27,35,139,199,232,148,179,176,236,29,250,198,105,47,234,251,126,203,39,57,23,249,77,156,135,161,189,
39,21,200,191,119,250,244,233,149,197,24,169,59,145,36,105,185,101,89,179,36,73,186,52,42,179,31,224,255,55,247,196,86,146,36,149,100,35,128,182,51,69,81,41,130,108,184,17,54,86,27,246,160,4,229,199,253,
250,245,107,46,250,254,219,251,65,34,145,152,67,7,213,116,179,30,37,78,118,187,151,32,94,108,35,172,176,230,155,237,249,211,55,77,11,149,92,39,204,48,180,49,217,217,42,106,204,243,233,95,252,226,23,61,
126,205,121,54,155,45,204,159,63,191,65,150,229,197,164,162,61,207,107,227,69,1,160,85,15,188,61,97,177,132,229,185,210,158,147,64,11,60,34,219,191,21,33,78,232,112,67,67,131,85,236,253,199,10,116,250,
244,233,67,85,85,189,129,157,77,160,155,8,182,80,97,203,106,131,239,39,125,223,127,217,247,253,131,24,227,247,49,198,21,65,150,211,173,162,40,182,73,84,32,33,3,109,96,242,61,172,2,194,102,91,98,38,1,28,
223,247,127,94,172,145,186,27,140,241,207,108,219,126,76,81,20,137,78,134,102,155,119,210,89,9,254,79,168,125,200,159,201,111,0,160,141,125,130,250,194,174,235,254,14,99,188,213,243,188,211,130,32,156,
21,62,73,33,188,22,33,84,35,138,98,255,168,157,86,232,115,179,201,218,130,32,60,95,202,189,199,10,84,146,164,25,97,226,140,202,8,138,72,32,56,99,219,246,131,133,66,97,125,54,155,13,11,140,235,150,46,93,
122,141,44,203,171,20,69,25,75,98,36,0,104,185,121,2,109,252,168,167,149,134,109,178,168,230,125,107,93,93,221,251,69,91,169,29,50,153,140,214,220,220,124,163,40,138,87,6,118,56,149,74,165,94,107,108,
108,52,58,227,252,117,117,117,239,47,92,184,112,187,227,56,19,92,215,109,101,255,176,86,164,61,251,208,30,145,21,166,109,219,224,186,238,111,11,133,194,223,47,88,176,224,191,194,202,147,205,102,31,214,
117,253,27,146,36,45,151,36,233,243,180,80,233,158,123,72,18,247,25,140,113,67,41,247,30,43,80,69,81,166,177,201,31,81,83,151,33,34,0,219,182,247,155,166,121,251,130,5,11,222,139,187,206,220,185,115,15,
97,140,111,125,242,201,39,31,247,125,127,62,27,136,179,215,42,165,183,201,166,134,5,101,123,169,232,19,196,48,110,220,184,10,77,211,22,250,190,255,253,116,58,173,147,7,40,184,94,174,186,186,122,181,227,
56,181,29,205,209,164,113,28,231,37,34,80,118,205,19,75,49,246,97,237,66,37,122,44,203,231,243,243,35,156,9,0,0,4,255,246,252,146,37,75,118,32,132,94,145,101,249,122,118,136,145,30,49,9,58,165,150,235,
186,211,215,172,89,83,210,136,73,164,64,51,153,204,21,146,36,93,197,138,51,44,35,136,157,213,8,102,103,222,46,20,10,227,179,217,236,135,197,20,68,16,4,12,0,11,150,45,91,150,194,24,207,166,254,30,0,32,
50,198,97,206,209,166,210,216,138,8,142,166,98,202,20,199,237,183,223,222,95,211,180,205,138,162,92,173,40,74,171,88,60,184,94,202,182,237,71,44,203,170,158,52,105,210,196,13,27,54,156,44,231,122,166,
105,238,74,36,18,45,222,40,74,160,97,54,96,137,18,167,97,24,63,157,59,119,238,220,98,203,52,127,254,252,247,179,217,236,88,199,113,126,38,73,210,76,122,182,139,22,168,97,24,167,77,211,188,187,161,161,
97,119,169,247,221,182,235,29,144,72,36,70,133,237,238,22,55,206,69,205,109,227,66,161,112,79,177,226,164,57,126,252,248,67,166,105,30,14,154,154,150,202,96,97,227,175,168,230,61,36,177,225,204,202,149,
43,79,148,90,46,154,76,38,83,145,74,165,54,167,82,169,171,43,42,42,160,87,175,94,112,209,69,23,181,58,122,245,234,5,21,21,21,144,78,167,7,235,186,190,185,170,170,170,87,57,215,92,181,106,213,91,174,235,
190,79,119,96,194,214,55,1,180,111,27,128,182,147,22,134,97,28,53,77,243,193,82,203,149,205,102,11,143,62,250,232,61,185,92,238,134,124,62,255,108,115,115,243,59,185,92,206,202,229,114,31,230,114,185,
223,229,243,249,89,182,109,127,105,213,170,85,37,139,19,32,198,131,138,162,248,133,176,45,7,195,132,192,222,172,101,89,27,178,217,236,27,29,41,208,154,53,107,156,108,54,251,35,81,20,215,211,225,4,221,
41,40,150,136,108,169,183,58,82,46,26,132,208,163,186,174,95,77,198,132,233,100,104,0,104,149,84,28,60,212,131,49,198,11,0,96,78,57,215,117,93,247,45,207,243,46,101,135,129,74,33,34,38,7,203,178,22,103,
179,89,187,163,101,171,173,173,125,29,0,94,239,232,255,143,34,210,131,138,162,216,151,158,185,105,111,140,145,142,241,44,203,42,107,55,182,139,47,190,248,55,182,109,55,147,41,205,142,84,70,76,66,195,31,
202,41,91,38,147,209,52,77,251,30,201,71,72,167,211,45,222,146,62,122,245,234,5,233,116,154,206,236,191,255,91,223,250,86,162,156,107,187,174,251,7,218,30,236,184,114,49,176,15,108,80,95,57,140,241,175,
203,41,91,87,17,41,80,132,80,159,176,197,92,237,9,212,182,109,240,60,175,67,222,147,48,123,246,108,203,182,237,127,103,227,173,142,138,148,246,26,190,239,151,181,166,72,81,148,49,154,166,233,116,218,90,
220,65,101,124,165,92,215,29,83,206,181,125,223,255,128,126,224,232,123,44,134,176,22,37,112,42,111,118,246,146,150,206,34,178,137,71,8,245,97,227,205,176,216,147,109,50,60,207,243,100,89,142,237,181,
23,131,235,186,239,208,2,37,179,72,244,181,219,107,238,217,10,193,24,131,235,186,101,109,177,173,40,202,85,100,216,141,76,92,144,44,42,186,137,103,155,123,85,85,193,48,140,126,229,92,27,99,220,204,78,
59,22,35,208,168,100,99,202,139,118,233,178,150,114,136,19,104,203,247,98,98,63,74,164,130,36,21,157,36,21,137,235,186,2,219,148,1,148,38,76,246,123,88,103,171,84,20,69,193,100,126,155,29,35,166,69,73,
247,100,73,44,42,138,98,89,251,40,177,158,147,189,199,98,236,66,127,167,226,242,200,150,180,187,137,44,152,32,8,103,218,235,9,178,61,201,224,64,31,124,240,65,159,114,11,230,251,254,21,81,21,210,17,168,
89,21,189,156,114,97,140,79,209,57,174,197,28,84,162,203,169,50,175,157,166,239,165,131,231,8,59,62,95,78,185,186,146,72,129,98,140,139,218,196,63,236,73,246,60,239,250,114,10,53,107,214,44,21,99,60,34,
236,26,165,16,54,221,230,121,222,37,229,148,77,215,245,215,0,32,215,129,178,52,59,142,211,161,161,22,138,222,108,211,222,25,118,1,128,161,229,118,224,186,138,56,215,126,166,152,19,176,25,53,65,18,193,
61,229,20,202,113,156,41,8,161,84,216,252,122,41,176,255,63,56,6,150,83,182,149,43,87,26,190,239,175,102,99,185,184,35,248,237,79,202,77,78,17,4,97,32,219,162,149,107,151,160,206,116,77,211,166,150,83,
182,174,34,50,88,244,125,255,116,220,96,48,64,120,186,92,208,148,221,113,223,125,247,141,90,187,118,109,201,227,98,153,76,70,17,69,113,81,92,106,89,71,160,202,57,184,195,39,9,48,77,179,214,113,156,73,
182,109,255,53,61,254,73,199,160,204,58,241,195,8,161,186,114,174,137,49,22,30,120,224,129,193,157,104,7,182,206,30,155,53,107,214,139,165,100,26,209,140,31,63,126,20,66,232,239,16,66,149,0,208,7,0,242,
190,239,31,116,93,247,5,132,80,135,223,255,20,215,196,239,101,189,64,84,115,194,222,172,44,203,130,44,203,207,221,115,207,61,189,75,45,144,174,235,245,146,36,13,110,111,130,32,142,176,236,38,170,98,46,
191,247,222,123,7,148,90,46,154,134,134,134,115,185,92,238,118,211,52,143,82,235,154,218,28,193,34,191,195,249,124,254,142,39,158,120,162,232,28,200,48,190,243,157,239,124,17,33,116,9,45,176,82,91,24,
246,247,100,234,58,72,102,190,218,48,140,146,243,99,39,77,154,148,156,56,113,226,115,154,166,237,209,117,253,94,93,215,175,76,165,82,170,174,235,23,235,186,126,147,174,235,13,178,44,31,30,55,110,220,232,
82,207,13,16,35,208,53,107,214,156,246,125,255,100,156,72,105,67,145,36,89,42,187,125,0,0,252,166,178,178,178,232,216,102,250,244,233,179,85,85,157,21,149,53,85,106,211,22,230,225,17,66,32,203,114,101,
177,101,138,162,190,190,254,164,227,56,95,207,231,243,79,228,243,249,28,181,179,7,57,154,243,249,252,82,81,20,175,175,175,175,47,107,30,30,0,0,33,116,19,189,170,160,148,86,133,77,180,33,226,100,4,10,170,
170,126,255,222,123,239,93,6,241,161,95,11,213,213,213,151,203,178,252,219,116,58,61,51,157,78,183,76,90,208,159,233,116,26,116,93,191,34,153,76,110,175,170,170,186,169,212,251,142,29,15,242,60,175,209,
243,188,71,194,68,74,223,52,189,12,131,94,188,166,40,202,13,151,95,126,249,98,0,120,164,189,130,212,212,212,124,69,85,213,229,113,203,151,75,109,222,194,194,143,160,156,83,1,160,236,23,17,100,179,217,
115,0,48,39,155,205,46,146,101,121,140,40,138,253,0,0,28,199,57,233,121,222,238,206,28,252,150,36,105,106,220,3,91,12,81,246,32,175,225,9,198,157,31,153,57,115,230,8,219,182,31,88,191,126,253,127,70,156,
10,77,153,50,229,46,85,85,159,84,20,165,111,216,210,232,160,67,74,79,249,170,134,97,60,63,110,220,184,193,165,188,72,55,86,160,182,109,175,115,93,247,145,176,1,115,186,99,132,131,68,217,16,129,130,44,
203,127,95,93,93,189,252,149,87,94,137,29,188,215,52,109,177,162,40,82,88,122,31,185,233,142,118,8,232,10,9,142,91,239,187,239,190,75,215,174,93,219,41,57,161,129,16,183,117,198,185,194,152,57,115,230,
101,162,40,142,101,151,112,119,164,163,68,139,148,212,23,189,55,86,240,155,74,211,52,127,127,247,221,119,239,118,28,103,155,235,186,199,48,198,103,17,66,151,139,162,248,85,73,146,38,203,178,124,101,216,
222,78,180,64,201,212,55,21,167,247,193,24,207,2,128,199,139,45,111,172,64,87,175,94,253,230,195,15,63,252,154,235,186,99,232,155,8,203,158,161,159,72,178,138,208,182,109,80,85,85,81,85,117,42,0,252,99,
212,117,170,170,170,122,201,178,60,49,46,107,63,110,44,54,236,239,217,230,157,94,27,35,203,178,44,8,194,247,0,224,199,69,218,169,91,145,101,249,126,89,150,165,114,150,9,19,88,47,42,203,114,171,142,48,
249,55,89,150,5,203,178,198,184,174,59,134,118,76,68,216,180,19,10,243,160,116,186,29,233,60,6,19,3,211,161,179,4,10,0,224,121,222,19,142,227,140,113,28,7,20,69,105,153,198,35,55,66,110,154,21,40,189,
138,80,81,148,161,113,215,72,165,82,95,150,101,89,12,219,106,37,110,129,23,251,157,253,13,91,25,76,140,252,195,76,38,83,223,89,89,239,93,197,204,153,51,117,89,150,127,192,238,62,23,215,204,183,55,54,74,
199,161,228,247,196,123,146,122,36,158,145,36,72,19,162,234,56,42,97,153,22,103,32,216,47,85,85,85,169,91,182,108,41,106,180,160,93,129,46,95,190,124,211,188,121,243,118,57,142,115,35,121,26,162,4,74,
55,163,244,62,61,8,161,116,220,53,20,69,73,135,237,213,212,158,151,96,133,201,118,224,72,232,65,196,73,199,92,138,162,92,150,76,38,31,4,128,218,98,12,213,93,8,130,240,176,162,40,151,132,237,214,66,79,
71,19,138,29,184,39,117,134,49,6,50,53,77,123,85,146,143,75,11,148,118,68,113,185,194,0,159,8,146,245,166,129,152,5,73,146,210,0,80,148,64,219,237,173,9,130,128,93,215,157,101,89,150,75,182,40,164,151,
29,196,221,60,21,243,197,198,122,8,161,247,195,122,168,44,237,13,142,71,85,78,216,40,67,32,210,249,153,76,230,138,246,108,208,93,204,152,49,227,74,85,85,31,97,59,142,81,226,4,104,187,243,28,221,119,160,
71,97,34,134,154,90,18,96,216,116,66,210,35,103,82,8,91,45,67,15,123,207,105,200,198,99,246,176,97,195,138,126,231,104,81,195,9,245,245,245,7,76,211,92,76,54,148,37,34,109,111,249,1,69,236,128,189,105,
154,7,32,102,250,144,78,108,160,23,99,5,233,98,5,246,109,26,116,37,132,140,209,2,21,220,39,53,77,123,46,147,201,244,196,183,157,32,89,150,255,89,85,85,141,174,120,182,5,163,91,15,218,6,129,141,10,244,
54,221,116,38,62,129,126,120,73,179,78,103,105,177,187,225,209,27,198,133,189,153,143,117,52,116,188,28,124,238,203,102,179,69,191,44,163,232,44,150,147,39,79,46,49,77,115,39,61,67,18,246,234,149,16,175,
246,151,124,62,255,155,184,115,55,54,54,218,0,240,75,54,61,142,246,2,244,74,81,106,29,77,206,182,237,149,97,15,11,91,9,116,39,137,206,68,210,52,237,38,89,150,231,21,107,135,243,197,140,25,51,30,75,36,
18,99,217,221,91,162,214,132,177,57,158,193,81,239,56,78,51,93,79,228,97,14,19,41,43,84,90,172,236,158,8,81,155,196,197,133,29,65,39,236,153,82,236,80,180,231,56,124,248,48,30,62,124,248,6,132,208,36,
225,147,245,209,45,113,30,49,14,189,116,149,154,97,153,253,236,179,207,238,107,239,252,215,94,123,237,254,68,34,241,109,73,146,52,250,9,100,207,79,4,235,121,158,227,251,254,84,140,241,47,61,207,251,174,
239,251,73,114,174,184,16,1,160,245,182,46,65,165,222,60,104,208,160,19,135,15,31,62,80,172,61,186,146,76,38,115,167,174,235,79,235,186,46,208,59,31,147,77,218,194,68,74,63,204,193,67,252,193,199,31,127,
156,145,101,249,13,215,117,239,244,125,95,12,27,125,97,7,241,89,207,23,151,181,197,230,11,19,232,250,98,54,181,221,255,222,123,239,205,58,124,248,112,209,25,46,37,53,109,111,188,241,134,49,108,216,176,
13,8,161,201,0,240,185,176,39,151,41,80,195,211,79,63,189,164,152,115,191,249,230,155,185,97,195,134,237,147,36,41,131,16,82,232,166,43,100,23,95,215,182,237,111,207,158,61,251,165,141,27,55,58,227,199,
143,79,97,140,111,34,195,36,113,179,78,97,34,197,24,11,190,239,79,28,48,96,192,158,35,71,142,156,42,197,38,157,205,212,169,83,111,214,117,253,69,93,215,37,86,156,197,108,41,73,237,241,191,236,193,7,31,
220,190,113,227,198,227,99,199,142,61,238,121,222,100,140,49,138,202,169,96,255,28,214,60,183,215,79,32,122,8,219,219,213,48,140,211,142,227,76,120,230,153,103,74,122,231,125,201,177,215,222,189,123,207,
125,249,203,95,94,39,138,98,127,223,247,191,196,110,41,19,204,63,159,43,20,10,15,174,88,177,98,113,41,231,222,183,111,223,169,17,35,70,108,194,24,127,29,0,250,178,134,15,206,127,194,178,172,111,204,157,
59,183,37,108,24,51,102,204,33,65,16,238,7,0,133,54,46,64,219,249,103,128,200,87,99,75,190,239,87,247,235,215,239,95,143,29,59,86,146,17,59,139,73,147,38,93,165,235,250,142,100,50,153,98,223,140,199,118,
146,8,116,204,73,237,71,144,43,20,10,119,239,216,177,195,0,0,120,245,213,87,255,171,178,178,242,205,64,164,114,88,94,69,220,104,73,49,179,86,116,72,22,226,168,14,27,134,113,203,83,79,61,213,245,91,128,
3,0,236,219,183,175,176,107,215,174,127,29,57,114,228,175,29,199,249,139,235,186,103,109,219,126,215,52,205,61,150,101,253,163,235,186,223,121,242,201,39,59,148,251,184,119,239,222,247,198,141,27,247,
115,195,48,94,247,125,255,35,199,113,62,112,28,231,132,101,89,59,44,203,170,251,227,31,255,56,187,190,190,190,213,11,180,118,236,216,81,24,51,102,12,18,4,225,22,214,131,182,103,88,202,139,130,239,251,
26,198,248,202,67,135,14,173,239,72,217,203,229,186,235,174,91,155,76,38,135,146,55,148,176,222,147,157,65,162,155,82,230,77,116,217,5,11,22,108,165,207,189,125,251,246,99,99,198,140,217,134,49,30,231,
251,254,231,232,127,43,198,171,134,65,229,216,70,238,85,111,24,198,75,8,161,201,117,117,117,127,238,136,77,58,158,183,213,195,200,102,179,9,73,146,14,105,154,214,159,158,145,10,243,56,180,199,167,95,62,
112,238,220,57,104,110,110,246,11,133,66,255,151,94,122,233,157,243,89,254,169,83,167,94,153,76,38,79,164,211,105,212,171,87,47,160,69,74,247,224,217,123,161,99,126,203,178,160,80,40,188,93,81,81,49,100,
246,236,217,161,227,140,115,230,204,169,72,165,82,63,81,85,245,110,69,81,4,230,205,28,177,11,36,9,113,157,89,234,65,57,103,24,198,156,31,255,248,199,63,43,199,46,61,113,120,165,67,52,53,53,185,163,70,
141,58,37,8,194,93,36,86,138,138,71,105,15,68,199,208,193,33,184,174,251,230,161,67,135,162,18,37,186,132,33,67,134,212,36,147,201,41,236,235,194,227,154,118,102,169,55,217,163,254,222,249,243,231,31,
137,186,206,158,61,123,172,157,59,119,254,122,196,136,17,59,49,198,95,193,24,247,13,19,92,72,194,117,171,221,10,169,97,62,58,180,0,211,52,61,211,52,255,79,161,80,152,82,91,91,219,84,174,93,202,95,221,
214,131,120,252,241,199,95,89,184,112,225,58,132,208,93,237,37,60,179,83,179,204,235,115,206,251,224,189,36,73,87,208,115,220,113,155,180,133,117,76,3,129,254,114,201,146,37,155,138,185,94,109,109,237,
46,140,241,176,197,139,23,79,112,28,231,65,89,150,111,145,36,9,177,57,184,172,221,194,60,103,32,210,102,215,117,255,197,52,205,250,199,31,127,188,168,119,152,22,101,151,206,58,81,79,225,195,15,63,252,
59,73,146,134,137,162,248,69,214,200,100,94,56,44,145,132,158,162,69,8,117,120,135,141,142,34,8,130,67,55,181,113,189,229,176,206,136,101,89,199,63,254,248,227,31,148,120,77,12,0,91,0,96,203,130,5,11,
254,74,211,180,140,40,138,227,16,66,163,69,81,172,8,187,62,35,206,19,158,231,237,114,28,103,211,217,179,103,55,173,92,185,178,211,243,26,46,56,129,174,94,189,58,55,119,238,220,25,162,40,190,46,138,162,
194,26,153,29,247,99,197,26,124,30,61,223,229,22,4,225,40,93,6,214,123,49,11,255,216,102,213,177,44,235,238,134,134,134,14,239,162,87,91,91,251,71,0,120,10,0,158,202,100,50,226,53,215,92,115,165,32,8,
131,4,65,248,43,132,80,10,99,172,5,41,119,121,0,56,158,207,231,223,238,204,45,44,163,184,224,4,10,0,176,116,233,210,253,243,230,205,187,31,33,244,115,33,168,97,50,232,79,103,240,68,172,183,202,121,158,
215,238,139,107,59,27,140,241,111,225,147,233,222,20,91,62,223,247,91,132,26,210,172,99,203,178,190,91,87,87,247,111,157,85,150,198,198,70,175,177,177,241,4,0,148,181,201,90,103,112,193,116,146,88,118,
239,222,253,251,145,35,71,122,8,161,91,216,89,41,54,134,163,182,5,7,203,178,86,60,243,204,51,231,253,197,10,7,15,30,180,134,15,31,222,75,150,229,209,116,252,73,123,80,118,0,60,232,20,45,172,173,173,45,
105,83,216,79,19,23,172,64,1,0,94,123,237,181,93,215,95,127,253,101,130,32,124,141,30,156,143,234,1,155,166,249,31,166,105,126,107,255,254,253,78,119,148,119,232,208,161,123,37,73,170,18,69,177,111,220,
120,39,245,22,183,159,214,214,214,246,184,60,130,206,164,199,110,121,210,89,200,178,60,43,159,207,175,48,12,3,23,10,133,150,215,243,25,134,193,174,190,220,237,56,206,132,53,107,214,20,245,122,148,174,
96,205,154,53,5,199,113,38,88,150,181,155,94,45,74,151,57,184,7,92,40,20,86,72,146,52,187,253,179,126,186,185,96,6,234,219,99,206,156,57,55,170,170,58,87,150,229,113,8,33,25,160,101,99,175,131,182,109,
55,156,56,113,226,153,82,222,33,217,149,100,50,25,177,127,255,254,223,86,20,101,150,44,203,67,168,172,116,199,113,156,237,150,101,45,93,182,108,89,155,215,109,95,136,124,102,4,74,120,232,161,135,244,32,
143,64,150,36,233,221,243,209,19,45,135,121,243,230,93,234,186,238,23,16,66,142,231,121,39,234,235,235,203,218,157,143,195,225,112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,56,28,14,135,195,225,
112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,56,28,14,135,195,225,
112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,56,28,14,135,195,225,112,56,61,137,255,7,87,27,206,210,109,234,115,206,
0,0,0,0,73,69,78,68,174,66,96,130,0,0};

const char* SettingsComponent::nAc_logo_png = (const char*) resource_SettingsComponent_nAc_logo_png;
const int SettingsComponent::nAc_logo_pngSize = 8229;


//[EndFile] You can add extra defines here...
//[/EndFile]
