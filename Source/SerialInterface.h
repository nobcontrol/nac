/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SERIALINTERFACE_H_INCLUDED
#define SERIALINTERFACE_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

#include <string>
#include <iostream>
#include <cstdio>

// OS Specific sleep
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

//#include "serial/serial.h"

using std::string;
using std::exception;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;

#include "serial/serial.h"

class SerialInterface : public juce::Thread, public juce::ActionBroadcaster
{
public:
	SerialInterface();
	~SerialInterface();

	void run();

	void setTrayIcon(juce::SystemTrayIconComponent* aTrayIcon);

	void setUpController();

	bool updateFirmware();

	bool getKnobIsBeingTouched();
	bool getLeftToggleIsBeingTouched();
	bool getRightToggleIsBeingTouched();
	bool getLeftToggleIsUp();
	bool getRightToggleIsUp();

	bool isConnected();

private:
	bool interfaceReadable;

	serial::PortInfo discoverDevice();
	serial::PortInfo discoverBootloader();

    juce::int64 lastDiscoveryTime;

	bool knobIsBeingTouched;
	bool leftToggleIsBeingTouched;
	bool rightToggleIsBeingTouched;
	bool leftToggleIsUp;
	bool rightToggleIsUp;

	bool HIDFunctionalityEnabled;

	juce::SystemTrayIconComponent* trayIcon;
	bool infoBubbleIsBeingShown;
	juce::int64 timeInfoBubbleWasShown;
};

#endif 
