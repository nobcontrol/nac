/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "UpdateFirmwareWindow.h"

UpdateFirmwareWindow::UpdateFirmwareWindow() : DocumentWindow("Update nOb's firmware",
												Colour(245, 245, 245),
												DocumentWindow::closeButton)
{

#if JUCE_WIN32
	setUsingNativeTitleBar(false);
#endif
#if JUCE_MAC
	setUsingNativeTitleBar(true);
#endif

	lookAndFeel = new LookAndFeel_V3();
	lookAndFeel->setColour(juce::PopupMenu::ColourIds::highlightedBackgroundColourId, juce::Colours::lightblue);
	lookAndFeel->setColour(juce::PopupMenu::ColourIds::highlightedTextColourId, juce::Colours::black);
	lookAndFeel->setColour(juce::Slider::ColourIds::backgroundColourId, juce::Colours::lightgrey);
	lookAndFeel->setColour(juce::Slider::ColourIds::thumbColourId, juce::Colours::lightblue);

	setLookAndFeel(lookAndFeel);

	updateFirmwareComponent = new UpdateFirmwareComponent();

	setContentNonOwned(updateFirmwareComponent, true);
	setSize(248, 112);
	
    int extraSpaceBecauseOfNoneNativeWindow = 0;
    
#if JUCE_WIN32
    extraSpaceBecauseOfNoneNativeWindow = 40;
#endif
    
	centreWithSize(getWidth(), getHeight() + extraSpaceBecauseOfNoneNativeWindow);
}

UpdateFirmwareWindow::~UpdateFirmwareWindow()
{
	updateFirmwareComponent = nullptr;
}

void UpdateFirmwareWindow::closeButtonPressed()
{
	setVisible(false);
}

void UpdateFirmwareWindow::setSerialInterface(SerialInterface * theSerialInterface)
{
	updateFirmwareComponent->setSerialInterface(theSerialInterface);
}
