/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 4.3.1

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library - "Jules' Utility Class Extensions"
  Copyright (c) 2015 - ROLI Ltd.

  ==============================================================================
*/

//[Headers] You can add your own extra header files here...
/*
nAc: An assignment center for nOb.
Copyright 2017 Dio Marinos, NOB CONTROL

This file is part of nAc.

nAc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nAc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nAc.  If not, see <http://www.gnu.org/licenses/>.
*/
//[/Headers]

#include "QuickAssignmentShortcutEditor.h"


//[MiscUserDefs] You can add your own user definitions and misc code here...
#include "QuickAssignmentsPanel.h"
//[/MiscUserDefs]

//==============================================================================
QuickAssignmentShortcutEditor::QuickAssignmentShortcutEditor (QuickAssignmentsPanel* Owner)
{
    //[Constructor_pre] You can add your own custom stuff here..
	owner = Owner;
    //[/Constructor_pre]

    addAndMakeVisible (assignShortcutButton = new SetShortcutButton ("<Assign with...>", false, this));
    assignShortcutButton->setName ("<Assign with...>");

    addAndMakeVisible (triggerShortcutButton = new SetShortcutButton ("<Activate with...>", false, this));
    triggerShortcutButton->setName ("<Activate with...>");

    addAndMakeVisible (removeButton = new SetShortcutButton (juce::String(), true, this));
    removeButton->setName ("remove button");


    //[UserPreSize]
    //[/UserPreSize]

    setSize (272, 25);


    //[Constructor] You can add your own custom stuff here..
    String defaultAssignTooltip = String("Set a shortcut for saving the control assignment.");
    String defaultTriggerTooltip = String("Set a shortcut for activating the corresponding control assignment.");

    assignShortcutButton->setTooltip(defaultAssignTooltip);
    assignShortcutButton->setDefaultTooltip(defaultAssignTooltip);
    triggerShortcutButton->setTooltip(defaultTriggerTooltip);
    triggerShortcutButton->setDefaultTooltip(defaultTriggerTooltip);
    removeButton->setTooltip(String("Press to clear this pair of shortcuts."));

    isDefaultEditor = false;

    assignment = new Assignment();
    //[/Constructor]
}

QuickAssignmentShortcutEditor::~QuickAssignmentShortcutEditor()
{
    //[Destructor_pre]. You can add your own custom destruction code here..
    //[/Destructor_pre]

    assignShortcutButton = nullptr;
    triggerShortcutButton = nullptr;
    removeButton = nullptr;


    //[Destructor]. You can add your own custom destruction code here..
    assignment = nullptr;
    //[/Destructor]
}

//==============================================================================
void QuickAssignmentShortcutEditor::paint (Graphics& g)
{
    //[UserPrePaint] Add your own custom painting code here..
    //[/UserPrePaint]

    g.fillAll (Colour (0xffc4c4c4));

    //[UserPaint] Add your own custom painting code here..
    //[/UserPaint]
}

void QuickAssignmentShortcutEditor::resized()
{
    //[UserPreResize] Add your own custom resize code here..
    //[/UserPreResize]

    assignShortcutButton->setBounds (3, 2, 112, 21);
    triggerShortcutButton->setBounds (119, 2, 112, 21);
    removeButton->setBounds (235, 2, 21, 21);
    //[UserResized] Add your own custom resize handling here..
    //[/UserResized]
}

void QuickAssignmentShortcutEditor::mouseEnter (const MouseEvent& e)
{
    //[UserCode_mouseEnter] -- Add your code here...
    //[/UserCode_mouseEnter]
}

void QuickAssignmentShortcutEditor::mouseExit (const MouseEvent& e)
{
    //[UserCode_mouseExit] -- Add your code here...
    //[/UserCode_mouseExit]
}



//[MiscUserCode] You can add your own definitions of your custom methods or any other code here...
void QuickAssignmentShortcutEditor::removeButtonPressed()
{
	resetShortcuts();
	owner->removeShortcutPair(this);
}

void QuickAssignmentShortcutEditor::shortcutSet(SetShortcutButton *button)
{
	SetShortcutButton *otherButton = NULL;
	if (assignShortcutButton == button)
		otherButton = triggerShortcutButton;
	else if (triggerShortcutButton == button)
		otherButton = assignShortcutButton;

    bool removeButtonShouldBeVisible = false;

	if (otherButton)
	{
		if (button->shortcut == otherButton->shortcut)
		{
			otherButton->resetShortcut();
			otherButton->repaint();

            String newTooltip;
            if (button ==assignShortcutButton)
                newTooltip = String("You can set an assignment by pressing: ")+button->shortcut.getDescription();
            else
                newTooltip = String("You can activate the assignment by pressing: ")+button->shortcut.getDescription();

            if (!isDefaultEditor)
                button->setTooltip(newTooltip);

            removeButtonShouldBeVisible = true;
		}
		else
		{
			owner->shortcutSet(this, button);

            String newTooltip;
            if (button ==assignShortcutButton)
                newTooltip = String("You can set an assignment by pressing: ")+button->shortcut.getDescription();
            else
                newTooltip = String("You can activate the assignment by pressing: ")+button->shortcut.getDescription();

            if (!isDefaultEditor)
                button->setTooltip(newTooltip);

            removeButtonShouldBeVisible = true;
		}

		if (button->shortcut.keyPress.isValid() && otherButton->shortcut.keyPress.isValid())
		{
            String newAssignTooltip;
            String newTriggerTooltip;

            if (button ==assignShortcutButton)
            {
                newAssignTooltip = String("You can set an assignment by pressing: ")+button->shortcut.getDescription();
                newTriggerTooltip = String("You can activate the assignment by pressing: ")+otherButton->shortcut.getDescription();

                button->setTooltip(newAssignTooltip);
                otherButton->setTooltip(newTriggerTooltip);
            }
            else
            {
                newAssignTooltip = String("You can set an assignment by pressing: ")+otherButton->shortcut.getDescription();
                newTriggerTooltip = String("You can activate the assignment by pressing: ")+button->shortcut.getDescription();

                otherButton->setTooltip(newAssignTooltip);
                button->setTooltip(newTriggerTooltip);
            }

			owner->addShortcutPair(this);

            removeButtonShouldBeVisible = false;
		}
	}

    if (isDefaultEditor)
        removeButton->setVisible(removeButtonShouldBeVisible);
}

void QuickAssignmentShortcutEditor::updateTooltips()
{
	String newAssignTooltip;
	String newTriggerTooltip;

	String assignmentInfo;

	if (assignment)
	{
		if (assignment->controlMode == ControlMode::SystemControl)
		{
			assignmentInfo = String("\n\nCurrently assigned to master volume.");
		}
		else if (assignment->assignmentPosition.owner.isNotEmpty() && assignment->assignmentPosition.title.isNotEmpty())
		{
			assignmentInfo = String("\n\nCurrently assigned to " + assignment->assignmentPosition.title + " of " + assignment->assignmentPosition.owner) +
				String("\nin ");

			String extraInfo;
			if (assignment->controlMode == ControlMode::CursorControl)
			{
				if (assignment->currentDirectionState == DirectionStates::vertical && assignment->currentOperationMode == CursorControllerOperationStates::dragMode)
					extraInfo = String("vertical drag mode.");
				else if (assignment->currentDirectionState == DirectionStates::vertical && assignment->currentOperationMode == CursorControllerOperationStates::stationaryMode)
					extraInfo = String("vertical stationary mode.");
				else if (assignment->currentDirectionState == DirectionStates::horizontal && assignment->currentOperationMode == CursorControllerOperationStates::dragMode)
					extraInfo = String("horizontal drag mode.");
				else if (assignment->currentDirectionState == DirectionStates::horizontal && assignment->currentOperationMode == CursorControllerOperationStates::stationaryMode)
					extraInfo = String("horizontal stationary mode.");
			}
			else if (assignment->controlMode == ControlMode::ScrollWheelControl)
			{
				if (assignment->currentDirectionState == DirectionStates::vertical)
					extraInfo = String("vertical scrolling mode.");
				else if (assignment->currentDirectionState == DirectionStates::horizontal)
					extraInfo = String("horizontal scrolling mode.");
			}

			assignmentInfo = assignmentInfo + extraInfo;
		}
	}

	newAssignTooltip = String("You can set an assignment by pressing: ") + assignShortcutButton->shortcut.getDescription() + assignmentInfo;
	newTriggerTooltip = String("You can activate the assignment by pressing: ") + triggerShortcutButton->shortcut.getDescription() + assignmentInfo;

	assignShortcutButton->setTooltip(newAssignTooltip);
	triggerShortcutButton->setTooltip(newTriggerTooltip);
}

void QuickAssignmentShortcutEditor::copyFromOther(const QuickAssignmentShortcutEditor& other)
{
	assignShortcutButton->copyFromOther(*other.assignShortcutButton);
	triggerShortcutButton->copyFromOther(*other.triggerShortcutButton);
}

void QuickAssignmentShortcutEditor::resetShortcuts()
{
	assignShortcutButton->resetShortcut();
	triggerShortcutButton->resetShortcut();

    if (isDefaultEditor)
        removeButton->setVisible(false);

	repaint();
}

void QuickAssignmentShortcutEditor::resetAssignShortcut()
{
	assignShortcutButton->resetShortcut();
	repaint();
}

void QuickAssignmentShortcutEditor::resetTriggerShortcut()
{
	triggerShortcutButton->resetShortcut();
	repaint();
}

QuickAssignmentsPanel* QuickAssignmentShortcutEditor::getQuickAssignmentsPanel()
{
	return owner;
}

void QuickAssignmentShortcutEditor::setAsDefault()
{
    isDefaultEditor = true;
    removeButton->setVisible(false);
}


//[/MiscUserCode]


//==============================================================================
#if 0
/*  -- Projucer information section --

    This is where the Projucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="QuickAssignmentShortcutEditor"
                 componentName="" parentClasses="public Component" constructorParams="QuickAssignmentsPanel* Owner"
                 variableInitialisers="" snapPixels="8" snapActive="1" snapShown="1"
                 overlayOpacity="0.330" fixedSize="1" initialWidth="272" initialHeight="25">
  <METHODS>
    <METHOD name="mouseEnter (const MouseEvent&amp; e)"/>
    <METHOD name="mouseExit (const MouseEvent&amp; e)"/>
  </METHODS>
  <BACKGROUND backgroundColour="ffc4c4c4"/>
  <GENERICCOMPONENT name="&lt;Assign with...&gt;" id="ef38d37781cb5ddf" memberName="assignShortcutButton"
                    virtualName="" explicitFocusOrder="0" pos="3 2 112 21" class="SetShortcutButton"
                    params="&quot;&lt;Assign with...&gt;&quot;, false, this"/>
  <GENERICCOMPONENT name="&lt;Trigger with...&gt;" id="f51f5623f7f49e20" memberName="triggerShortcutButton"
                    virtualName="" explicitFocusOrder="0" pos="119 2 112 21" class="SetShortcutButton"
                    params="&quot;&lt;Trigger with...&gt;&quot;, false, this"/>
  <GENERICCOMPONENT name="remove button" id="43bb253077854b3e" memberName="removeButton"
                    virtualName="" explicitFocusOrder="0" pos="235 2 21 21" class="SetShortcutButton"
                    params="juce::String(), true, this"/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif


//[EndFile] You can add extra defines here...
//[/EndFile]
